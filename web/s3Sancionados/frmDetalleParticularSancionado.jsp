<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri = "http://saemm.gob.mx" prefix="patterns"%>

<%@include file="/encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-sis-min.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="/migas.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>Plataforma Digital Estatal</h1>
            <h2>Sistema III</h2>
            <h4>Particulares sancionados.</h4>
            <div id="cargaDB" class="reveal-overlay" style="display: none;">
                <div id="loader"></div>
                <div id="textDB">
                    Actualizando base de datos...
                </div>
            </div>
            <div class="stacked-for-small expanded large button-group">
                <a class="button hollow" onclick="document.frmListado.submit()"><i class="material-icons md-4t">list</i> Listado de particulares sancionados</a>
                <c:if test="${nivelDeAcceso.id == 1 || nivelDeAcceso.id == 2}">
                    <a class="button hollow" onclick="document.frmNuevo.submit()"><i class="material-icons md-4t">playlist_add</i> Agregar nuevo particular sancionado</a>
                    <form name="frmNuevo" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones">
                        <input type="hidden" name="accion" value="mostrarPaginaRegistroNuevoParticularSancionado"/>
                    </form>
                </c:if>
            </div>
            <h2>Datos del particular sancionado</h2>
            <fieldset class="fieldset">
                <legend>Particular sancionado</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>RFC</b>
                        <label>${patterns:validarRfcPF(particular.particularSancionado.rfc) ? particular.particularSancionado.rfc : ""}</label>
                    </label>
                    <label class="cell medium-4"><b>Raz&oacute;n social</b>
                        <label>${particular.particularSancionado.nombreRazonSocial}</label>
                    </label>
                    <label class="cell medium-4"><b>Objeto social</b>
                        <label>${particular.particularSancionado.objetoSocial}</label>
                    </label>
                    <label class="cell medium-4"><b>Tipo de persona</b>
                        <label>${tipoPersona.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Tel&eacute;fono</b>
                        <label>${particular.particularSancionado.telefono}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Domicilio en M&eacute;xico</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>Pa&iacute;s</b>
                        <label>${particular.particularSancionado.domicilioMexico.pais.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Entidad federativa</b>
                        <label>${particular.particularSancionado.domicilioMexico.entidadFederativa.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Municipio</b>
                        <label>${particular.particularSancionado.domicilioMexico.municipio.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>C&oacute;digo Postal</b>
                        <label>${particular.particularSancionado.domicilioMexico.codigoPostal}</label>
                    </label>
                    <label class="cell medium-4"><b>Localidad</b>
                        <label>${particular.particularSancionado.domicilioMexico.localidad.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Vialidad</b>
                        <label>${particular.particularSancionado.domicilioMexico.vialidad.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>N&uacute;mero exterior</b>
                        <label>${particular.particularSancionado.domicilioMexico.numeroExterior}</label>
                    </label>
                    <label class="cell medium-4"><b>N&uacute;mero interior</b>
                        <label>${particular.particularSancionado.domicilioMexico.numeroInterior}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Domicilio en el extranjero</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>Pa&iacute;s</b>
                        <label>${particular.particularSancionado.domicilioExtranjero.pais.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Estado o Provincia</b>
                        <label>${particular.particularSancionado.domicilioExtranjero.estadoProvincia}</label>
                    </label>
                    <label class="cell medium-4"><b>Ciudad o localidad</b>
                        <label>${particular.particularSancionado.domicilioExtranjero.ciudadLocalidad}</label>
                    </label>
                    <label class="cell medium-4"><b>C&oacute;digo Postal</b>
                        <label>${particular.particularSancionado.domicilioExtranjero.codigoPostal}</label>
                    </label>
                    <label class="cell medium-4"><b>Calle</b>
                        <label>${particular.particularSancionado.domicilioExtranjero.calle}</label>
                    </label>
                    <label class="cell medium-4"><b>N&uacute;mero exterior</b>
                        <label>${particular.particularSancionado.domicilioExtranjero.numeroExterior}</label>
                    </label>
                    <label class="cell medium-4"><b>N&uacute;mero interior</b>
                        <label>${particular.particularSancionado.domicilioExtranjero.numeroInterior}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Director general</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>Nombre</b>
                        <label>${particular.particularSancionado.directorGeneral.nombres} ${particular.particularSancionado.directorGeneral.primerApellido} ${particular.particularSancionado.directorGeneral.segundoApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>CURP</b>
                        <label>${particular.particularSancionado.directorGeneral.curp}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Apoderado legal</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>Nombre</b>
                        <label>${particular.particularSancionado.apoderadoLegal.nombres} ${particular.particularSancionado.apoderadoLegal.primerApellido} ${particular.particularSancionado.apoderadoLegal.segundoApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>CURP</b>
                        <label>${particular.particularSancionado.apoderadoLegal.curp}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Datos de la sanci&oacute;n</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>N&uacute;mero de expediente</b>
                        <label>${particular.expediente}</label>
                    </label>
                    <label class="cell medium-4"><b>Instituci&oacute;n de dependencia</b>
                        <label>${particular.institucionDependencia.nombre}</label>
                    </label>
                    <label class="cell medium-4"><b>Autoridad sancionadora</b>
                        <label>${particular.autoridadSancionadora}</label>
                    </label>
                    <label class="cell medium-4"><b>Tipo de falta</b>
                        <label>${particular.tipoFalta}</label>
                    </label>
                    <label class="cell medium-12">
                        <fieldset class="fieldset">
                            <legend><b>Tipo de sanci&oacute;n</b></legend>
                            <div class="grid-margin-x grid-x">
                                <c:forEach  items="${particular.tipoSancion}" var="tipoSancion">
                                    <label class="cell medium-4">
                                        <label><b>Tipo:</b> ${tipoSancion.valor}</label>
                                        <label><b>Descripci&oacute;n:</b> ${tipoSancion.descripcion}</label>
                                    </label>
                                </c:forEach>
                            </div>
                        </fieldset>
                    </label>
                    <label class="cell medium-12"><b>Causa o motivo</b>
                        <label>${particular.causaMotivoHechos}</label>
                    </label>
                    <label class="cell medium-4"><b>Acto</b>
                        <label>${particular.acto}</label>
                    </label>
                    <label class="cell medium-4"><b>Responsable de la sanci&oacute;n</b>
                        <label>${particular.responsableSancion.nombres} ${particular.responsableSancion.primerApellido} ${particular.responsableSancion.segundoApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>Sentido de la resoluci&oacute;n</b>
                        <label>${particular.resolucion.sentido}</label>
                    </label>
                    <label class="cell medium-4"><b>URL de la resoluc&oacute;n</b>
                        <c:if test="${particular.resolucion.url != null && particular.resolucion.url.isEmpty() == false}">
                            <label><a href="${(fn:contains(particular.resolucion.url, 'http')) ? "" : "//" }${particular.resolucion.url}" target="_blank">${particular.resolucion.url}</a></label>
                            </c:if>
                    </label>
                    <label class="cell medium-4"><b>Fecha de la resoluc&oacute;n</b>
                        <label>${particular.resolucion.fechaNotificacion}</label>
                    </label>
                    <label class="cell medium-4"><b>Sanci&oacute;n econ&oacute;mica</b>
                        <label>
                            <c:choose>
                                <c:when test="${particular.multa.monto == null}">

                                </c:when>
                                <c:otherwise>
                                    ${particular.multa.monto}
                                    ${particular.multa.moneda.valor}
                                </c:otherwise>
                            </c:choose>
                        </label>
                    </label>
                    <label class="cell medium-4"><b>Plazo de inhabilitaci&oacute;n</b>
                        <label>${particular.inhabilitacion.plazo}</label>
                    </label>
                    <label class="cell medium-4"><b>Fecha inicial</b>
                        <label>${particular.inhabilitacion.fechaInicial}</label>
                    </label>
                    <label class="cell medium-4"><b>Fecha final</b>
                        <label>${particular.inhabilitacion.fechaFinal}</label>
                    </label>
                    <label class="cell medium-12"><b>Observaciones</b>
                        <label>${particular.observaciones}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Documentos</legend>
                <c:choose>
                    <c:when test="${fn:length(particular.documentos) > 0}">
                        <div class="grid-margin-x grid-x">
                            <c:forEach items="${particular.documentos}" var="documento">
                                <label class="cell medium-4"><b>Identificador &uacute;nico</b>
                                    <label>${documento.id}</label>
                                </label>
                                <label class="cell medium-4"><b>Tipo</b>
                                    <label>${documento.tipo}</label>
                                </label>
                                <label class="cell medium-4"><b>T&iacute;tulo</b>
                                    <label>${documento.titulo}</label>
                                </label>
                                <label class="cell medium-4"><b>Descripci&oacute;n</b>
                                    <label>${documento.descripcion}</label>
                                </label>
                                <label class="cell medium-4"><b>URL</b>
                                    <c:if test="${documento.url != null && documento.url.isEmpty() == false}">
                                        <label><a href="${(fn:contains(documento.url, 'http')) ? "" : "//" }${documento.url}" target="_blank">${documento.url}</a></label>
                                        </c:if>
                                </label>
                                <label class="cell medium-4"><b>Fecha</b>
                                    <label>${documento.fecha}</label>
                                </label>
                            </c:forEach>
                        </div>
                    </c:when>
                    <c:otherwise>
                        Sin documentos.
                    </c:otherwise>
                </c:choose>
            </fieldset>
            <a class="button expanded" onclick="frmRegresar.submit()">Regresar</a>
        </div>
    </div>
</div>
<form name="frmListado" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones">
    <input type="hidden" name="accion" value="consultarParticularesSancionados"/>
</form>
<form name="frmRegresar" action="/sistemaCaptura${initParam.versionIntranet}/sanciones" method="post">
    <input type="hidden" name="accion" value="consultarParticularesSancionados"/>
    <input type="hidden" name="txtId" value="${filtro.id}"/>
    <input type="hidden" name="txtRazonSocial" value="${filtro.razonSocial}"/>
    <input type="hidden" name="txtInstitucion" value="${filtro.institucion}"/>
    <input type="hidden" name="cmbPaginacion" value="${filtro.registrosMostrar}"/>
    <input type="hidden" name="cmbOrdenacion" value="${filtro.orden}"/>
    <input type="hidden" name="txtNumeroPagina" value="${filtro.numeroPagina}"/>
</form>

<%@include file="/piePagina.jsp" %>
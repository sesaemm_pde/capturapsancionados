<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri = "http://saemm.gob.mx" prefix="patterns"%>

<%@include file="/encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-sis-min.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="/migas.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>Plataforma Digital Estatal</h1>
            <h2>Sistema III</h2>
            <h4>Particulares sancionados.</h4>
            <div id="cargaDB" class="reveal-overlay" style="display: none;">
                <div id="loader"></div>
                <div id="textDB">
                    Actualizando base de datos...
                </div>
            </div>
            <div class="stacked-for-small expanded large button-group">
                <a class="button hollow" onclick="document.frmListado.submit()"><i class="material-icons md-4t">list</i> Listado de particulares sancionados</a>
                <a class="button"><i class="material-icons md-4t">playlist_add</i> Agregar nuevo particular</a>
            </div>
            <form name="frmListado" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones">
                <input type="hidden" name="accion" value="consultarServidoresPublicosSancionados"/>
            </form>
            <h2>Datos del particular sancionado</h2>
            <fieldset class="fieldset">
                <legend>Particular sancionado</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>RFC</b>
                        <label>${patterns:validarRfcPF(particular.particularSancionado.rfc) ? particular.particularSancionado.rfc : ""}</label>
                    </label>
                    <label class="cell medium-4"><b>Raz&oacute;n social</b>
                        <label>${particular.particularSancionado.nombreRazonSocial}</label>
                    </label>
                    <label class="cell medium-4"><b>Objeto social</b>
                        <label>${particular.particularSancionado.objetoSocial}</label>
                    </label>
                    <label class="cell medium-4"><b>Tipo de persona</b>
                        <label>${tipoPersona.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Tel&eacute;fono</b>
                        <label>${particular.particularSancionado.telefono}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Domicilio en M&eacute;xico</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>Pa&iacute;s</b>
                        <label>${particular.particularSancionado.domicilioMexico.pais.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Entidad federativa</b>
                        <label>${particular.particularSancionado.domicilioMexico.entidadFederativa.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Municipio</b>
                        <label>${particular.particularSancionado.domicilioMexico.municipio.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>C&oacute;digo Postal</b>
                        <label>${particular.particularSancionado.domicilioMexico.codigoPostal}</label>
                    </label>
                    <label class="cell medium-4"><b>Localidad</b>
                        <label>${particular.particularSancionado.domicilioMexico.localidad.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Vialidad</b>
                        <label>${particular.particularSancionado.domicilioMexico.vialidad.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>N&uacute;mero exterior</b>
                        <label>${particular.particularSancionado.domicilioMexico.numeroExterior}</label>
                    </label>
                    <label class="cell medium-4"><b>N&uacute;mero interior</b>
                        <label>${particular.particularSancionado.domicilioMexico.numeroInterior}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Domicilio en el extranjero</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>Pa&iacute;s</b>
                        <label>${particular.particularSancionado.domicilioExtranjero.pais.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Estado o Provincia</b>
                        <label>${particular.particularSancionado.domicilioExtranjero.estadoProvincia}</label>
                    </label>
                    <label class="cell medium-4"><b>Ciudad o localidad</b>
                        <label>${particular.particularSancionado.domicilioExtranjero.ciudadLocalidad}</label>
                    </label>
                    <label class="cell medium-4"><b>C&oacute;digo Postal</b>
                        <label>${particular.particularSancionado.domicilioExtranjero.codigoPostal}</label>
                    </label>
                    <label class="cell medium-4"><b>Calle</b>
                        <label>${particular.particularSancionado.domicilioExtranjero.calle}</label>
                    </label>
                    <label class="cell medium-4"><b>N&uacute;mero exterior</b>
                        <label>${particular.particularSancionado.domicilioExtranjero.numeroExterior}</label>
                    </label>
                    <label class="cell medium-4"><b>N&uacute;mero interior</b>
                        <label>${particular.particularSancionado.domicilioExtranjero.numeroInterior}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Director general</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>Nombre</b>
                        <label>${particular.particularSancionado.directorGeneral.nombres} ${particular.particularSancionado.directorGeneral.primerApellido} ${particular.particularSancionado.directorGeneral.segundoApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>CURP</b>
                        <label>${particular.particularSancionado.directorGeneral.curp}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Apoderado legal</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>Nombre</b>
                        <label>${particular.particularSancionado.apoderadoLegal.nombres} ${particular.particularSancionado.apoderadoLegal.primerApellido} ${particular.particularSancionado.apoderadoLegal.segundoApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>CURP</b>
                        <label>${particular.particularSancionado.apoderadoLegal.curp}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Datos de la sanci&oacute;n</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>N&uacute;mero de expediente</b>
                        <label>${particular.expediente}</label>
                    </label>
                    <label class="cell medium-4"><b>Instituci&oacute;n de dependencia</b>
                        <label>${particular.institucionDependencia.nombre}</label>
                    </label>
                    <label class="cell medium-4"><b>Autoridad sancionadora</b>
                        <label>${particular.autoridadSancionadora}</label>
                    </label>
                    <label class="cell medium-4"><b>Tipo de falta</b>
                        <label>${particular.tipoFalta}</label>
                    </label>
                    <label class="cell medium-12">
                        <fieldset class="fieldset">
                            <legend><b>Tipo de sanci&oacute;n</b></legend>
                            <div class="grid-margin-x grid-x">
                                <c:forEach  items="${particular.tipoSancion}" var="tipoSancion">
                                    <label class="cell medium-4">
                                        <label><b>Tipo:</b> ${tipoSancion.valor}</label>
                                        <label><b>Descripci&oacute;n:</b> ${tipoSancion.descripcion}</label>
                                    </label>
                                </c:forEach>
                            </div>
                        </fieldset>
                    </label>
                    <label class="cell medium-12"><b>Causa o motivo</b>
                        <label>${particular.causaMotivoHechos}</label>
                    </label>
                    <label class="cell medium-4"><b>Acto</b>
                        <label>${particular.acto}</label>
                    </label>
                    <label class="cell medium-4"><b>Responsable de la sanci&oacute;n</b>
                        <label>${particular.responsableSancion.nombres} ${particular.responsableSancion.primerApellido} ${particular.responsableSancion.segundoApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>Sentido de la resoluci&oacute;n</b>
                        <label>${particular.resolucion.sentido}</label>
                    </label>
                    <label class="cell medium-4"><b>URL de la resoluci&oacute;n</b>
                        <c:if test="${particular.resolucion.url != null && particular.resolucion.url.isEmpty() == false}">
                            <label><a href="${(fn:contains(particular.resolucion.url, 'http')) ? "" : "//" }${particular.resolucion.url}" target="_blank">${particular.resolucion.url}</a></label>
                            </c:if>
                    </label>
                    <label class="cell medium-4"><b>Fecha de la resoluci&oacute;n</b>
                        <label>${particular.resolucion.fechaNotificacion}</label>
                    </label>
                    <label class="cell medium-4"><b>Sanci&oacute;n econ&oacute;mica</b>
                        <label>${particular.multa.monto} ${particular.multa.moneda.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Plazo de inhabilitaci&oacute;n</b>
                        <label>${particular.inhabilitacion.plazo}</label>
                    </label>
                    <label class="cell medium-4"><b>Fecha inicial</b>
                        <label>${particular.inhabilitacion.fechaInicial}</label>
                    </label>
                    <label class="cell medium-4"><b>Fecha final</b>
                        <label>${particular.inhabilitacion.fechaFinal}</label>
                    </label>
                    <label class="cell medium-12"><b>Observaciones</b>
                        <label>${particular.observaciones}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Documentos</legend>
                <c:choose>
                    <c:when test="${fn:length(particular.documentos) > 0}">
                        <div class="grid-margin-x grid-x">
                            <c:forEach items="${particular.documentos}" var="documento">
                                <label class="cell medium-4"><b>Identificador &uacute;nico</b>
                                    <label>${documento.id}</label>
                                </label>
                                <label class="cell medium-4"><b>Tipo</b>
                                    <label>${documento.tipo}</label>
                                </label>
                                <label class="cell medium-4"><b>T&iacute;tulo</b>
                                    <label>${documento.titulo}</label>
                                </label>
                                <label class="cell medium-4"><b>Descripci&oacute;n</b>
                                    <label>${documento.descripcion}</label>
                                </label>
                                <label class="cell medium-4"><b>URL</b>
                                    <c:if test="${documento.url != null && documento.url.isEmpty() == false}">
                                        <label><a href="${(fn:contains(documento.url, 'http')) ? "" : "//" }${documento.url}" target="_blank">${documento.url}</a></label>
                                        </c:if>
                                </label>
                                <label class="cell medium-4"><b>Fecha</b>
                                    <label>${documento.fecha}</label>
                                </label>
                            </c:forEach>
                        </div>
                    </c:when>
                    <c:otherwise>
                        Sin documentos.
                    </c:otherwise>
                </c:choose>
            </fieldset>
            <div class="grid-x grid-margin-x">
                <fieldset class="cell medium-6">
                    <form name="frmRegistrar" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones" data-abide novalidate>
                        <input type="hidden" name="accion" value="modificarDatosParticularSancionado"/>
                        <input type="hidden" name="txtRFC" id="txtRFC" value="${particular.particularSancionado.rfc}"/>
                        <input type="hidden" name="txtNOMBRE_RAZON_SOCIAL" id="txtNOMBRE_RAZON_SOCIAL" value="${particular.particularSancionado.nombreRazonSocial}"/>
                        <input type="hidden" name="txtOBJETO_SOCIAL" id="txtOBJETO_SOCIAL" value="${particular.particularSancionado.objetoSocial}"/>
                        <input type="hidden" name="cmbTIPO_PERSONA" id="cmbTIPO_PERSONA" value="${particular.particularSancionado.tipoPersona}"/>
                        <input type="hidden" name="txtTELEFONO" id="txtTELEFONO"  value="${particular.particularSancionado.telefono}"/>
                        <input type="hidden" name="cmbPAIS" value="MX"/>
                        <input type="hidden" name="txtCODIGO_POSTAL" id="txtCODIGO_POSTAL" value="${particular.particularSancionado.domicilioMexico.codigoPostal}" />
                        <input type="hidden" name="cmbENTIDAD_FEDERATIVA" id="cmbENTIDAD_FEDERATIVA" value="${particular.particularSancionado.domicilioMexico.entidadFederativa.clave}" />
                        <input type="hidden" name="cmbMUNICIPIO" id="cmbMUNICIPIO" value="${particular.particularSancionado.domicilioMexico.municipio.clave}" />
                        <input type="hidden" name="cmbLOCALIDAD" id="cmbLOCALIDAD" value="${particular.particularSancionado.domicilioMexico.localidad.clave}" />
                        <input type="hidden" name="cmbVIALIDAD_CLAVE" id="cmbVIALIDAD_CLAVE" value="${particular.particularSancionado.domicilioMexico.vialidad.clave}" />
                        <input type="hidden" name="cmbVIALIDAD_VALOR" id="cmbVIALIDAD_VALOR"  value="${particular.particularSancionado.domicilioMexico.vialidad.valor}">
                        <input type="hidden" name="txtNUMERO_EXTERIOR" id="txtNUMERO_EXTERIOR" value="${particular.particularSancionado.domicilioMexico.numeroExterior}"/>
                        <input type="hidden" name="txtNUMERO_INTERIOR" id="txtNUMERO_INTERIOR" value="${particular.particularSancionado.domicilioMexico.numeroInterior}"/>
                        <input type="hidden" name="cmbEXTRANJERO_PAIS" id="cmbEXTRANJERO_PAIS" value="${particular.particularSancionado.domicilioExtranjero.pais.clave}"/>
                        <input type="hidden" name="txtEXTRANJERO_ESTADOPROVINCIA" id="txtEXTRANJERO_ESTADOPROVINCIA" value="${particular.particularSancionado.domicilioExtranjero.estadoProvincia}"/>
                        <input type="hidden" name="txtEXTRANJERO_CIUDADLOCALIDAD" id="txtEXTRANJERO_CIUDADLOCALIDAD" value="${particular.particularSancionado.domicilioExtranjero.ciudadLocalidad}"/>
                        <input type="hidden" name="txtEXTRANJERO_CODIGO_POSTAL" id="txtEXTRANJERO_CODIGO_POSTAL" value="${particular.particularSancionado.domicilioExtranjero.codigoPostal}"/>
                        <input type="hidden" name="txtEXTRANJERO_CALLE" id="txtEXTRANJERO_CALLE" value="${particular.particularSancionado.domicilioExtranjero.calle}"/>
                        <input type="hidden" name="txtEXTRANJERO_NUMERO_EXTERIOR" id="txtEXTRANJERO_NUMERO_EXTERIOR" value="${particular.particularSancionado.domicilioExtranjero.numeroExterior}"/>
                        <input type="hidden" name="txtEXTRANJERO_NUMERO_INTERIOR" id="txtEXTRANJERO_NUMERO_INTERIOR" value="${particular.particularSancionado.domicilioExtranjero.numeroInterior}"/>
                        <input type="hidden" name="txtDIRECTORL_NOMBRES" id="txtDIRECTORL_NOMBRES" value="${particular.particularSancionado.directorGeneral.nombres}"/>
                        <input type="hidden" name="txtDIRECTORL_PRIMERAPELLIDO" id="txtDIRECTORL_PRIMERAPELLIDO" value="${particular.particularSancionado.directorGeneral.primerApellido}"/>
                        <input type="hidden" name="txtDIRECTORL_SEGUNDOAPELLIDO" id="txtDIRECTORL_SEGUNDOAPELLIDO" value="${particular.particularSancionado.directorGeneral.segundoApellido}"/>
                        <input type="hidden" name="txtDIRECTORL_CURP" id="txtDIRECTORL_CURP" class="mayusculas" value="${particular.particularSancionado.directorGeneral.curp}"/>
                        <input type="hidden" name="txtAPODERADO_NOMBRES" id="txtAPODERADO_NOMBRES" value="${particular.particularSancionado.apoderadoLegal.nombres}"/>
                        <input type="hidden" name="txtAPODERADO_PRIMERAPELLIDO" id="txtAPODERADO_PRIMERAPELLIDO" value="${particular.particularSancionado.apoderadoLegal.primerApellido}"/>
                        <input type="hidden" name="txtAPODERADO_SEGUNDOAPELLIDO" id="txtAPODERADO_SEGUNDOAPELLIDO" value="${particular.particularSancionado.apoderadoLegal.segundoApellido}"/>
                        <input type="hidden" name="txtAPODERADO_CURP" id="txtAPODERADO_CURP" class="mayusculas" value="${particular.particularSancionado.apoderadoLegal.curp}"/>
                        <input type="hidden" name="txtOBJETO_CONTRATO" id="txtOBJETO_CONTRATO" value="${particular.objetoContrato}"/>
                        <input type="hidden" name="txtNUMERO_EXPEDIENTE" id="txtNUMERO_EXPEDIENTE" value="${particular.expediente}"/>
                        <input type="hidden" name="cmbDEPENDENCIA" id="cmbDEPENDENCIA" value="${particular.institucionDependencia.clave}"/>
                        <input type="hidden" name="txtAUTORIDAD_SANCIONADORA" id="txtAUTORIDAD_SANCIONADORA" value="${particular.autoridadSancionadora}"/>
                        <input type="hidden" name="cmbTIPO_FALTAS" id="cmbTIPO_FALTAS" value="${particular.tipoFalta}"/>

                        <c:forEach items="${particular.tipoSancion}" var="tipoSancion_p">
                            <input type="hidden" name="chckTIPO_SANCION" value="${tipoSancion_p.clave}"/>
                            <input type="hidden" name="txtTIPO_SANCION_${tipoSancion_p.clave}_DESCRIPCION"  value="${tipoSancion_p.descripcion}"/>
                        </c:forEach>

                        <input type="hidden" name="txtCAUSA_MOTIVO" id="txtCAUSA_MOTIVO" value="${particular.causaMotivoHechos}"/>
                        <input type="hidden" name="txtACTO" id="txtACTO" value="${particular.acto}"/>
                        <input type="hidden" name="txtRESPONSABLESANCION_NOMBRES" id="txtRESPONSABLESANCION_NOMBRES" value="${particular.responsableSancion.nombres}"/>
                        <input type="hidden" name="txtRESPONSABLESANCION_PRIMERAPELLIDO" id="txtRESPONSABLESANCION_PRIMERAPELLIDO" value="${particular.responsableSancion.primerApellido}"/>
                        <input type="hidden" name="txtRESPONSABLESANCION_SEGUNDOAPELLIDO" id="txtRESPONSABLESANCION_SEGUNDOAPELLIDO" value="${particular.responsableSancion.segundoApellido}"/>
                        <input type="hidden" name="txtPLAZO" id="txtPLAZO" value="${arregloDePlazo[0]}"/>
                        <input type="hidden" name="cmbUNIDAD_MEDIDA_PLAZO" id="cmbUNIDAD_MEDIDA_PLAZO" value="${arregloDePlazo[1]}"/>
                        <input type="hidden" name="txtFECHA_INICIAL" id="txtFECHA_INICIAL" value="${particular.inhabilitacion.fechaInicial}" />
                        <input type="hidden" name="txtFECHA_FINAL" id="txtFECHA_FINAL" value="${particular.inhabilitacion.fechaFinal}"/>
                        <input type="hidden" name="txtRESOLUCION_SENTIDO" id="txtRESOLUCION_SENTIDO" value="${particular.resolucion.sentido}"/>
                        <input type="hidden" name="txtRESOLUCION_URL" id="txtRESOLUCION_URL" value="${particular.resolucion.url}" />
                        <input type="hidden" name="txtRESOLUCION_FECHA_NOTIFICACION" id="txtRESOLUCION_FECHA_NOTIFICACION" value="${particular.resolucion.fechaNotificacion}" />
                        <input type="hidden" name="txtMULTA_MONTO" id="txtMULTA_MONTO" value="${particular.multa.monto}"/>
                        <input type="hidden" name="cmbMONEDA" id="cmbMONEDA" value="${particular.multa.moneda.clave}"/>
                        <input type="hidden" name="txtOBSERVACIONES" id="txtOBSERVACIONES" value="${particular.observaciones}"/>

                        <c:set var="contadorDocs" value="0"></c:set>
                        <c:forEach items="${particular.documentos}" var="listaDocumentos">
                            <input type="hidden" name="txtDOCUMENTO_ID_${contadorDocs}" value="${listaDocumentos.id}" />
                            <input type="hidden" name="txtDOCUMENTO_TIPO_${contadorDocs}" value="${listaDocumentos.tipo}"/>
                            <input type="hidden" name="txtDOCUMENTO_TITULO_${contadorDocs}" value="${listaDocumentos.titulo}"/>
                            <input type="hidden" name="txtDOCUMENTO_DESCRIPCION_${contadorDocs}" value="${listaDocumentos.descripcion}"/>
                            <input type="hidden" name="txtDOCUMENTO_URL_${contadorDocs}" value="${listaDocumentos.url}"/>
                            <input type="hidden" name="txtDOCUMENTO_FECHA_${contadorDocs}" value="${listaDocumentos.fecha}"/>
                            <c:set var="contadorDocs" value="${contadorDocs+1}"></c:set>
                        </c:forEach>
                        <input type="hidden" name ="txtDOCUMENTO_NUM" id="txtDOCUMENTO_NUM" value="${fn:length(particular.documentos)}">
                        <input type="submit" class="button expanded secondary" name="btnReset" value="Modificar datos"/>
                    </form>
                </fieldset>
                <fieldset class="cell medium-6">
                    <form name="frmRegistrar" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones" data-abide novalidate>
                        <input type="hidden" name="accion" value="registrarParticularSancionado"/>
                        <input type="hidden" name="txtRFC" id="txtRFC" value="${particular.particularSancionado.rfc}"/>
                        <input type="hidden" name="txtNOMBRE_RAZON_SOCIAL" id="txtNOMBRE_RAZON_SOCIAL" value="${particular.particularSancionado.nombreRazonSocial}"/>
                        <input type="hidden" name="txtOBJETO_SOCIAL" id="txtOBJETO_SOCIAL" value="${particular.particularSancionado.objetoSocial}"/>
                        <input type="hidden" name="cmbTIPO_PERSONA" id="cmbTIPO_PERSONA" value="${particular.particularSancionado.tipoPersona}"/>
                        <input type="hidden" name="txtTELEFONO" id="txtTELEFONO"  value="${particular.particularSancionado.telefono}"/>
                        <input type="hidden" name="cmbPAIS" value="MX"/>
                        <input type="hidden" name="txtCODIGO_POSTAL" id="txtCODIGO_POSTAL" value="${particular.particularSancionado.domicilioMexico.codigoPostal}" />
                        <input type="hidden" name="cmbENTIDAD_FEDERATIVA" id="cmbENTIDAD_FEDERATIVA" value="${particular.particularSancionado.domicilioMexico.entidadFederativa.clave}" />
                        <input type="hidden" name="cmbMUNICIPIO" id="cmbMUNICIPIO" value="${particular.particularSancionado.domicilioMexico.municipio.clave}" />
                        <input type="hidden" name="cmbLOCALIDAD" id="cmbLOCALIDAD" value="${particular.particularSancionado.domicilioMexico.localidad.clave}" />
                        <input type="hidden" name="cmbVIALIDAD_CLAVE" id="cmbVIALIDAD_CLAVE" value="${particular.particularSancionado.domicilioMexico.vialidad.clave}" />
                        <input type="hidden" name="cmbVIALIDAD_VALOR" id="cmbVIALIDAD_VALOR"  value="${particular.particularSancionado.domicilioMexico.vialidad.valor}">
                        <input type="hidden" name="txtNUMERO_EXTERIOR" id="txtNUMERO_EXTERIOR" value="${particular.particularSancionado.domicilioMexico.numeroExterior}"/>
                        <input type="hidden" name="txtNUMERO_INTERIOR" id="txtNUMERO_INTERIOR" value="${particular.particularSancionado.domicilioMexico.numeroInterior}"/>
                        <input type="hidden" name="cmbEXTRANJERO_PAIS" id="cmbEXTRANJERO_PAIS" value="${particular.particularSancionado.domicilioExtranjero.pais.clave}"/>
                        <input type="hidden" name="txtEXTRANJERO_ESTADOPROVINCIA" id="txtEXTRANJERO_ESTADOPROVINCIA" value="${particular.particularSancionado.domicilioExtranjero.estadoProvincia}"/>
                        <input type="hidden" name="txtEXTRANJERO_CIUDADLOCALIDAD" id="txtEXTRANJERO_CIUDADLOCALIDAD" value="${particular.particularSancionado.domicilioExtranjero.ciudadLocalidad}"/>
                        <input type="hidden" name="txtEXTRANJERO_CODIGO_POSTAL" id="txtEXTRANJERO_CODIGO_POSTAL" value="${particular.particularSancionado.domicilioExtranjero.codigoPostal}"/>
                        <input type="hidden" name="txtEXTRANJERO_CALLE" id="txtEXTRANJERO_CALLE" value="${particular.particularSancionado.domicilioExtranjero.calle}"/>
                        <input type="hidden" name="txtEXTRANJERO_NUMERO_EXTERIOR" id="txtEXTRANJERO_NUMERO_EXTERIOR" value="${particular.particularSancionado.domicilioExtranjero.numeroExterior}"/>
                        <input type="hidden" name="txtEXTRANJERO_NUMERO_INTERIOR" id="txtEXTRANJERO_NUMERO_INTERIOR" value="${particular.particularSancionado.domicilioExtranjero.numeroInterior}"/>
                        <input type="hidden" name="txtDIRECTORL_NOMBRES" id="txtDIRECTORL_NOMBRES" value="${particular.particularSancionado.directorGeneral.nombres}"/>
                        <input type="hidden" name="txtDIRECTORL_PRIMERAPELLIDO" id="txtDIRECTORL_PRIMERAPELLIDO" value="${particular.particularSancionado.directorGeneral.primerApellido}"/>
                        <input type="hidden" name="txtDIRECTORL_SEGUNDOAPELLIDO" id="txtDIRECTORL_SEGUNDOAPELLIDO" value="${particular.particularSancionado.directorGeneral.segundoApellido}"/>
                        <input type="hidden" name="txtDIRECTORL_CURP" id="txtDIRECTORL_CURP" class="mayusculas" value="${particular.particularSancionado.directorGeneral.curp}"/>
                        <input type="hidden" name="txtAPODERADO_NOMBRES" id="txtAPODERADO_NOMBRES" value="${particular.particularSancionado.apoderadoLegal.nombres}"/>
                        <input type="hidden" name="txtAPODERADO_PRIMERAPELLIDO" id="txtAPODERADO_PRIMERAPELLIDO" value="${particular.particularSancionado.apoderadoLegal.primerApellido}"/>
                        <input type="hidden" name="txtAPODERADO_SEGUNDOAPELLIDO" id="txtAPODERADO_SEGUNDOAPELLIDO" value="${particular.particularSancionado.apoderadoLegal.segundoApellido}"/>
                        <input type="hidden" name="txtAPODERADO_CURP" id="txtAPODERADO_CURP" class="mayusculas" value="${particular.particularSancionado.apoderadoLegal.curp}"/>
                        <input type="hidden" name="txtOBJETO_CONTRATO" id="txtOBJETO_CONTRATO" value="${particular.objetoContrato}"/>
                        <input type="hidden" name="txtNUMERO_EXPEDIENTE" id="txtNUMERO_EXPEDIENTE" value="${particular.expediente}"/>
                        <input type="hidden" name="cmbDEPENDENCIA" id="cmbDEPENDENCIA" value="${particular.institucionDependencia.clave}"/>
                        <input type="hidden" name="txtAUTORIDAD_SANCIONADORA" id="txtAUTORIDAD_SANCIONADORA" value="${particular.autoridadSancionadora}"/>
                        <input type="hidden" name="cmbTIPO_FALTAS" id="cmbTIPO_FALTAS" value="${particular.tipoFalta}"/>

                        <c:forEach items="${particular.tipoSancion}" var="tipoSancion_p">
                            <input type="hidden" name="chckTIPO_SANCION" value="${tipoSancion_p.clave}"/>
                            <input type="hidden" name="txtTIPO_SANCION_${tipoSancion_p.clave}_DESCRIPCION"  value="${tipoSancion_p.descripcion}"/>
                        </c:forEach>

                        <input type="hidden" name="txtCAUSA_MOTIVO" id="txtCAUSA_MOTIVO" value="${particular.causaMotivoHechos}"/>
                        <input type="hidden" name="txtACTO" id="txtACTO" value="${particular.acto}"/>
                        <input type="hidden" name="txtRESPONSABLESANCION_NOMBRES" id="txtRESPONSABLESANCION_NOMBRES" value="${particular.responsableSancion.nombres}"/>
                        <input type="hidden" name="txtRESPONSABLESANCION_PRIMERAPELLIDO" id="txtRESPONSABLESANCION_PRIMERAPELLIDO" value="${particular.responsableSancion.primerApellido}"/>
                        <input type="hidden" name="txtRESPONSABLESANCION_SEGUNDOAPELLIDO" id="txtRESPONSABLESANCION_SEGUNDOAPELLIDO" value="${particular.responsableSancion.segundoApellido}"/>
                        <input type="hidden" name="txtPLAZO" id="txtPLAZO" value="${arregloDePlazo[0]}"/>
                        <input type="hidden" name="cmbUNIDAD_MEDIDA_PLAZO" id="cmbUNIDAD_MEDIDA_PLAZO" value="${arregloDePlazo[1]}"/>
                        <input type="hidden" name="txtFECHA_INICIAL" id="txtFECHA_INICIAL" value="${particular.inhabilitacion.fechaInicial}" />
                        <input type="hidden" name="txtFECHA_FINAL" id="txtFECHA_FINAL" value="${particular.inhabilitacion.fechaFinal}"/>
                        <input type="hidden" name="txtRESOLUCION_SENTIDO" id="txtRESOLUCION_SENTIDO" value="${particular.resolucion.sentido}"/>
                        <input type="hidden" name="txtRESOLUCION_URL" id="txtRESOLUCION_URL" value="${particular.resolucion.url}" />
                        <input type="hidden" name="txtRESOLUCION_FECHA_NOTIFICACION" id="txtRESOLUCION_FECHA_NOTIFICACION" value="${particular.resolucion.fechaNotificacion}" />
                        <input type="hidden" name="txtMULTA_MONTO" id="txtMULTA_MONTO" value="${particular.multa.monto}"/>
                        <input type="hidden" name="cmbMONEDA" id="cmbMONEDA" value="${particular.multa.moneda.clave}"/>
                        <input type="hidden" name="txtOBSERVACIONES" id="txtOBSERVACIONES" value="${particular.observaciones}"/>

                        <c:set var="contadorDocs" value="0"></c:set>
                        <c:forEach items="${particular.documentos}" var="listaDocumentos">
                            <input type="hidden" name="txtDOCUMENTO_ID_${contadorDocs}" value="${listaDocumentos.id}" />
                            <input type="hidden" name="txtDOCUMENTO_TIPO_${contadorDocs}" value="${listaDocumentos.tipo}"/>
                            <input type="hidden" name="txtDOCUMENTO_TITULO_${contadorDocs}" value="${listaDocumentos.titulo}"/>
                            <input type="hidden" name="txtDOCUMENTO_DESCRIPCION_${contadorDocs}" value="${listaDocumentos.descripcion}"/>
                            <input type="hidden" name="txtDOCUMENTO_URL_${contadorDocs}" value="${listaDocumentos.url}"/>
                            <input type="hidden" name="txtDOCUMENTO_FECHA_${contadorDocs}" value="${listaDocumentos.fecha}"/>
                            <c:set var="contadorDocs" value="${contadorDocs+1}"></c:set>
                        </c:forEach>
                        <input type="hidden" name ="txtDOCUMENTO_NUM" id="txtDOCUMENTO_NUM" value="${fn:length(particular.documentos)}">
                        <input type="submit" class="button expanded" name="btnRegistrar" value="Registrar"/>
                    </form>
                </fieldset>
            </div>
        </div>
    </div>
</div>

<%@include file="/piePagina.jsp" %>
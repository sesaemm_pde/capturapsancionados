Foundation.Abide.defaults.patterns['curp'] = /^[A-Za-z]{1}[AEIOUaeiou]{1}[A-Za-z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HMhm]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE|as|bc|bs|cc|cs|ch|cl|cm|df|dg|gt|gr|hg|jc|mc|mn|ms|nt|nl|oc|pl|qt|qr|sp|sl|sr|tc|ts|tl|vz|yn|zs|ne)[B-DF-HJ-NP-TV-Zb-df-hj-np-tv-z]{3}[0-9A-Za-z]{1}[0-9]{1}$/;

Foundation.Abide.defaults.patterns['rfc_pf'] = /^[A-Za-z]{1}[AEIOUaeiou]{1}[A-Za-z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Za-z0-9]{3}$|^[A-Za-z]{1}[AEIOUaeiou]{1}[A-Za-z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])$/;

Foundation.Abide.defaults.patterns['rfc_pm'] = /^[A-Za-z]{3}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Za-z0-9]{3}$/;

Foundation.Abide.defaults.patterns['year'] = /^(198|199|200|201|202|203|204|205|206|207)\d{1}$/;

Foundation.Abide.defaults.patterns['url'] = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;

Foundation.Abide.defaults.patterns['codigo_postal'] = /^((?!(0))[0-9]{5})$/;

Foundation.Abide.defaults.patterns['telefono'] = /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;

Foundation.Abide.defaults.patterns['solo_enteros'] = /^\d+$/;

Foundation.Abide.defaults.patterns['fecha'] = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;

$(document).foundation();

setTimeout(function ()
{
    $('a[target="_blank"]').each(function ()
        {
            var a = $(this).html();
            0 == a.includes('<i class') && $(this).append('<i class="material-icons md-1t">exit_to_app</i>')
        })
}, 1e3), window.setInterval(function ()
{
    $('a[target="_blank"]').each(function ()
        {
            var a = $(this).html();
            0 == a.includes('<i class') && $(this).append('<i class="material-icons md-1t">exit_to_app</i>')
        })
}, 1e3);

function reiniciarValoresSPS3()
{
    document.getElementById('txtNombre').setAttribute('value', '');
    document.getElementById('txtPrimerApellido').setAttribute('value', '');
    document.getElementById('txtInstitucion').setAttribute('value', '');
}
    
function reiniciarValoresParticularesSPS3()
{
    document.getElementById('txtRazonSocial').setAttribute('value', '');
    document.getElementById('txtInstitucion').setAttribute('value', '');
}
    
function consultarServidoresPublicosS2()
{
    var form = document.createElement("form");
        form.method = "POST";
        form.action = document.getElementById("frmFiltroContrataciones").action;

        var accion = document.createElement("input");
        accion.value = 'consultarServidoresPublicosEnContrataciones';
        accion.name = "accion";
        accion.type = "hidden";
        form.appendChild(accion);

        var cmbPaginacion = document.createElement("input");
        cmbPaginacion.value = document.getElementById("cmbPaginacion").value;
        cmbPaginacion.name = "cmbPaginacion";
        cmbPaginacion.type = "hidden";
        form.appendChild(cmbPaginacion);

        var cmbOrdenacion = document.createElement("input");
        cmbOrdenacion.value = document.getElementById("cmbOrdenacion").value;
        cmbOrdenacion.name = "cmbOrdenacion";
        cmbOrdenacion.type = "hidden";
        form.appendChild(cmbOrdenacion);
       
        var txtNombre = document.createElement("input");
        txtNombre.value = document.getElementById("txtNombre").value;
        txtNombre.name = "txtNombre";
        txtNombre.type = "hidden";
        form.appendChild(txtNombre);

        var txtPrimerApellido = document.createElement("input");
        txtPrimerApellido.value = document.getElementById("txtPrimerApellido").value;
        txtPrimerApellido.name = "txtPrimerApellido";
        txtPrimerApellido.type = "hidden";
        form.appendChild(txtPrimerApellido);

        var txtInstitucion = document.createElement("input");
        txtInstitucion.value = document.getElementById("txtInstitucion").value;
        txtInstitucion.name = "txtInstitucion";
        txtInstitucion.type = "hidden";
        form.appendChild(txtInstitucion);

        document.body.appendChild(form);
        form.submit();
}

function consultarServidoresPublicosS3()
{
    var form = document.createElement("form");
        form.method = "POST";
        form.action = document.getElementById("frmFiltroServidoresSancionados").action;

    var accion = document.createElement("input");
        accion.value = 'consultarServidoresPublicosSancionados';
        accion.name = "accion";
        accion.type = "hidden";
        form.appendChild(accion);

    var cmbPaginacion = document.createElement("input");
        cmbPaginacion.value = document.getElementById("cmbPaginacion").value;
        cmbPaginacion.name = "cmbPaginacion";
        cmbPaginacion.type = "hidden";
        form.appendChild(cmbPaginacion);

    var cmbOrdenacion = document.createElement("input");
        cmbOrdenacion.value = document.getElementById("cmbOrdenacion").value;
        cmbOrdenacion.name = "cmbOrdenacion";
        cmbOrdenacion.type = "hidden";
        form.appendChild(cmbOrdenacion);
       
    var txtNombre = document.createElement("input");
        txtNombre.value = document.getElementById("txtNombre").value;
        txtNombre.name = "txtNombre";
        txtNombre.type = "hidden";
        form.appendChild(txtNombre);

    var txtPrimerApellido = document.createElement("input");
        txtPrimerApellido.value = document.getElementById("txtPrimerApellido").value;
        txtPrimerApellido.name = "txtPrimerApellido";
        txtPrimerApellido.type = "hidden";
        form.appendChild(txtPrimerApellido);

    var txtInstitucion = document.createElement("input");
        txtInstitucion.value = document.getElementById("txtInstitucion").value;
        txtInstitucion.name = "txtInstitucion";
        txtInstitucion.type = "hidden";
        form.appendChild(txtInstitucion);

        document.body.appendChild(form);
        form.submit();
}

function consultarParticularesS3()
{
    var form = document.createElement("form");
    form.method = "POST";
    form.action = document.getElementById("frmFiltroParticularesSancionados").action;

    var accion = document.createElement("input");
    accion.value = 'consultarParticularesSancionados';
    accion.name = "accion";
    accion.type = "hidden";
    form.appendChild(accion);

    var cmbPaginacion = document.createElement("input");
    cmbPaginacion.value = document.getElementById("cmbPaginacion").value;
    cmbPaginacion.name = "cmbPaginacion";
    cmbPaginacion.type = "hidden";
    form.appendChild(cmbPaginacion);

    var cmbOrdenacion = document.createElement("input");
    cmbOrdenacion.value = document.getElementById("cmbOrdenacion").value;
    cmbOrdenacion.name = "cmbOrdenacion";
    cmbOrdenacion.type = "hidden";
    form.appendChild(cmbOrdenacion);
    
    document.body.appendChild(form);
    form.submit();
}

function existeUrl(url)
    {
        $.get(url)
                .done(function ()
                    {
                        return true;
                    })
                .fail(function ()
                    {
                        var form = document.createElement("form");
                        form.method = "POST";
                        form.action = "/pde";

                        var accion = document.createElement("input");
                        accion.value = 'noFound';
                        accion.name = "accion";
                        accion.type = "hidden";
                        form.appendChild(accion);

                        document.body.appendChild(form);
                        form.submit();
                        return false;
                    });

    }

function confirmar(id, mensaje) {

  swal({
      title: "Confirmar",
      text: mensaje,
      icon: "warning",
      buttons: ["Cancelar", "Aceptar"],
      dangerMode: true,
      closeOnClickOutside: false,
      closeOnEsc: false,
    })
    .then((seAcepta) => {
      if (seAcepta) {
          mostrarLoader();
        document.getElementById(id).submit();
      } else {
        swal("Se ha cancelado la operaci\u00f3n", {
          icon: "error",
          buttons: false,
          timer: 2000,
        });
      }
    });
}

function mostrarLoader() {
    document.getElementById("cargaDB").style.display = "block"; 
}

var arregloCodigoDocumentosPorDefecto = new Array(); 
function crearListaDocumentosPorDefecto()
{
   
    var strEstructuraCodigoDocumentoPorDefecto;
   
    var intContadorDocumentos;
    
    if (typeof ($("#divContenedorSinDocumentosPorDefecto").html()) !== 'undefined')
    {
        arregloCodigoDocumentosPorDefecto.push($("#divContenedorSinDocumentosPorDefecto").html()); 
        $("#listaDeIdentificadoresDeDocumentosPorDefecto").val("0,");
    }
    else
    {
        intContadorDocumentos = parseInt($("#txtDOCUMENTO_NUM").val())
        for(i = 0; i <= intContadorDocumentos; i++)
        {
            strEstructuraCodigoDocumentoPorDefecto = "";
            strEstructuraCodigoDocumentoPorDefecto = '<div id="documento' + i + '">';
            strEstructuraCodigoDocumentoPorDefecto = strEstructuraCodigoDocumentoPorDefecto + $('#documento' + i).html();
            strEstructuraCodigoDocumentoPorDefecto = strEstructuraCodigoDocumentoPorDefecto + '</div>';

           
            arregloCodigoDocumentosPorDefecto.push(strEstructuraCodigoDocumentoPorDefecto); 
            
            $("#listaDeIdentificadoresDeDocumentosPorDefecto").val(($("#listaDeIdentificadoresDeDocumentosPorDefecto").val() + i + ","));
        }
    }
   
    $("#listaDeIdentificadoresDeDocumentosARegistrar").val($("#listaDeIdentificadoresDeDocumentosPorDefecto").val());
}

function borrarDocumentoSeleccionado(intNumeroDocumento, strIdentificadorDocumento)
{
   
    $('#documento' + intNumeroDocumento).remove();
   
    $('#txtDOCUMENTO_NUM').val(($('#txtDOCUMENTO_NUM').val() - 1));
   
   
    $("#listaDeIdentificadoresDeDocumentosARegistrar").val($("#listaDeIdentificadoresDeDocumentosARegistrar").val().replace((intNumeroDocumento + ","), "")); 
}
    
function desbloquearCamposDocumento(idGeneradoDocumento)
{

    var id = $("#txtDOCUMENTO_ID_" + idGeneradoDocumento);                     
    var valorActual = id.val();                                                
    id.blur(function()
        {

            var tipo;                      
            var titulo;                    
            var descripcion;               
            var url;                       
            var fecha;                     

            var spanTitulo;                
            var spanDescripcion;           
            var spanUrl;                   
            var spanFecha;                 

            var labelTitulo;               
            var labelDescripcion;          
            var labelUrl;                  
            var labelFecha;                

            tipo = $('#txtDOCUMENTO_TIPO_'+idGeneradoDocumento);
            titulo = $('#txtDOCUMENTO_TITULO_'+idGeneradoDocumento);
            descripcion = $('#txtDOCUMENTO_DESCRIPCION_'+idGeneradoDocumento);
            url = $('#txtDOCUMENTO_URL_'+idGeneradoDocumento);
            fecha = $('#txtDOCUMENTO_FECHA_'+idGeneradoDocumento);
            
            spanTitulo = $('#spanDOCUMENTO_TITULO_'+idGeneradoDocumento);
            spanDescripcion = $('#spanDOCUMENTO_DESCRIPCION_'+idGeneradoDocumento);
            spanUrl = $('#spanDOCUMENTO_URL_'+idGeneradoDocumento);
            spanFecha = $('#spanDOCUMENTO_FECHA_'+idGeneradoDocumento);

            labelTitulo = titulo.parent();
            labelDescripcion = descripcion.parent();
            labelUrl = url.parent();
            labelFecha = fecha.parent();

            if((id.val()).length > 0)
            {

                tipo.removeAttr('disabled');               
                titulo.removeAttr('disabled');
                titulo.attr('required',true);

                descripcion.removeAttr('disabled');
                descripcion.attr('required',true);

                url.removeAttr('disabled');
                url.attr('required',true);

                fecha.removeAttr('disabled'); 
                fecha.attr('required',true);
                
                Foundation.reInit("abide");                
            }
            else
            { 

                swal
                (
                    {
                        title: "Documento sin identificador",
                        text: "Si borra el identificador de documento, se borraran todos los datos asociados. \u00bfDeseas continuar\u003F",
                        icon: "warning",
                        buttons: ["Cancelar", "Aceptar"],
                        dangerMode: true,
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    }
                )
                .then((seAcepta) => 
                    {
                        if (seAcepta)
                        {
                            swal
                            (
                                {
                                    title: "Confirmar",
                                    text: "\u00bfEst\u00E1 completamente seguro de realizar la operaci\u00f3n\u003F",
                                    icon: "warning",
                                    buttons: ["Cancelar", "Aceptar"],
                                    dangerMode: true,
                                    closeOnClickOutside: false,
                                    closeOnEsc: false,
                                }
                            )
                            .then((seAcepta) => 
                                {
                                    if(seAcepta)
                                    {
                                        tipo.attr('disabled',true);                        

                                        titulo.attr('disabled',true);                      
                                        titulo.removeAttr('required');                     
                                        if(spanTitulo.hasClass("is-visible"))
                                        {
                                            titulo.removeClass("is-invalid-input");        
                                            spanTitulo.removeClass("is-visible");          
                                            labelTitulo.removeClass("is-invalid-label");   
                                        }

                                        descripcion.attr('disabled',true);                 
                                        descripcion.removeAttr('required');                
                                        if(spanDescripcion.hasClass("is-visible"))
                                        {
                                            descripcion.removeClass("is-invalid-input");   
                                            spanDescripcion.removeClass("is-visible");     
                                            labelDescripcion.removeClass("is-invalid-label");
                                        }

                                        url.attr('disabled',true);                         
                                        url.removeAttr('required');                        
                                        if(spanUrl.hasClass("is-visible"))
                                        {
                                            url.removeClass("is-invalid-input");           
                                            spanUrl.removeClass("is-visible");             
                                            labelUrl.removeClass("is-invalid-label");      
                                        }

                                        fecha.attr('disabled',true);                       
                                        fecha.removeAttr('required');                      
                                        if(spanFecha.hasClass("is-visible"))
                                        {
                                            fecha.removeClass("is-invalid-input");         
                                            spanFecha.removeClass("is-visible");           
                                            labelFecha.removeClass("is-invalid-label");    
                                        }

                                        tipo.val("");                                      
                                        titulo.val("");                                    
                                        descripcion.val("");                               
                                        url.val("");                                       
                                        fecha.val("");                                     
                                    }
                                    else
                                    {
                                        swal("Se ha cancelado la operaci\u00f3n", 
                                            {
                                                icon: "error",
                                                buttons: false,
                                                timer: 2000,
                                            }
                                        );
                                        $("#txtDOCUMENTO_ID_" + idGeneradoDocumento).val(valorActual); 
                                    }
                                }
                            )
                        }
                        else
                        {
                            swal("Se ha cancelado la operaci\u00f3n", 
                                {
                                    icon: "error",
                                    buttons: false,
                                    timer: 2000,
                                }
                            );
                            $("#txtDOCUMENTO_ID_" + idGeneradoDocumento).val(valorActual); 
                        }
                    }
                );

                Foundation.reInit("abide");        
            }
        }
    );
}
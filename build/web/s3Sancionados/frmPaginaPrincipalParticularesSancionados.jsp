<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@include file="/encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-sis-min.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="/migas.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>Plataforma Digital Estatal</h1>
            <h2>Sistema III</h2>
            <h4>Particulares sancionados.</h4>
            <div id="cargaDB" class="reveal-overlay" style="display: none;">
              <div id="loader"></div>
              <div id="textDB">
                Actualizando base de datos...
              </div>
            </div>
            <div class="stacked-for-small expanded large button-group">
                <a class="button"><i class="material-icons md-4t">list</i> Listado de particulares sancionados</a>
                <c:if test="${nivelDeAcceso.id == 1 || nivelDeAcceso.id == 2}">
                    <a class="button hollow" onclick="document.frmNuevo.submit()"><i class="material-icons md-4t">playlist_add</i> Agregar nuevo particular</a>
                    <form name="frmNuevo" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones">
                        <input type="hidden" name="accion" value="mostrarPaginaRegistroNuevoParticularSancionado"/>
                    </form>
                </c:if>
            </div>
            <form name="frmPrincipalFiltro" id="frmFiltroParticularesSancionados" action="/sistemaCaptura${initParam.versionIntranet}/sanciones" method="post">
                <input type="hidden" name="accion" value="consultarParticularesSancionados"/>
                <fieldset class="fieldset">
                    <legend>Busca un particular</legend>
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">
                            Raz&oacute;n social
                            <input type="text" name="txtRazonSocial" id="txtRazonSocial" placeholder="p. ej. Empresa S.A. de C.V." value="${filtro.razonSocial}"/>
                        </label>
                        <label class="cell medium-6">
                            Dependencia
                            <input type="text" name="txtInstitucion" id="txtInstitucion" placeholder="p. ej. Gubernatura" value="${filtro.institucion}"/>
                        </label>
                    </div>
                    <div class="grid-margin-x grid-x">
                        <input type="submit" value="Filtrar" class="button cell expanded medium-6"/>
                        <input type="reset" id="btnReiniciar" value="Reiniciar" onclick="reiniciarValoresParticularesSPS3();" class="button cell expanded medium-6 secondary"/>
                    </div>
                </fieldset>
                <div class="grid-x grid-margin-x align-middle">
                    <div class="cell medium-4">
                        <label>Mostrando <code>${(particularesSancionados.size() > 0 ? 1 : 0) + (paginacion.registrosMostrar * (paginacion.numeroPagina - 1))}</code> a <code>${(paginacion.registrosMostrar * (paginacion.numeroPagina - 1)) + particularesSancionados.size()}</code> de <code>${paginacion.totalRegistros}</code> registros</label>
                    </div>
                    <div class="cell medium-4">
                        <label>Registros por p&aacute;gina
                            <select name="cmbPaginacion" id="cmbPaginacion" onchange="consultarParticularesS3();">
                                <option value="10" ${filtro.registrosMostrar == 10 ? 'selected': ''}>10</option>
                                <option value="25" ${filtro.registrosMostrar == 25 ? 'selected': ''}>25</option>
                                <option value="50" ${filtro.registrosMostrar == 50 ? 'selected': ''}>50</option>
                                <option value="100" ${filtro.registrosMostrar == 100 ? 'selected': ''}>100</option>
                            </select>
                        </label>
                    </div>
                    <div class="cell medium-4">
                        <label id ="resultadosServidores">Ordenado por
                            <select name="cmbOrdenacion" id="cmbOrdenacion" onchange="consultarParticularesS3();">
                                <option value="nombreRazonSocial" ${filtro.orden == 'nombreRazonSocial' ? 'selected' : '' }>Raz&oacute;n social</option>
                                <option value="dependencia" ${filtro.orden == 'dependencia' ? 'selected' : '' }>Dependencia</option>
                            </select>
                        </label>
                    </div>
                </div>
            </form>
                            
            <div class="callout primary">
                <h5>Art&iacute;culo 53.</h5>
                <p class="text-justify">Las sanciones impuestas por faltas administrativas graves ser&aacute;n del conocimiento p&uacute;blico cuando &eacute;stas contengan impedimentos o inhabilitaciones para ser contratados como servidores p&uacute;blicos o como prestadores de servicios o contratistas del sector p&uacute;blico, en t&eacute;rminos de la Ley de Responsabilidades Administrativas del Estado de M&eacute;xico y Municipios.</p>
                <p class="text-justify">Los registros de las sanciones relativas a responsabilidades administrativas no graves, quedar&aacute;n registradas para efectos de eventual reincidencia, pero no ser&aacute;n p&uacute;blicas.</p>
                <a href="https://legislacion.edomex.gob.mx/sites/legislacion.edomex.gob.mx/files/files/pdf/ley/vig/leyvig240.pdf" target="_blank">Ley del Sistema Anticorrupci&oacute;n del Estado de M&eacute;xico y Municipios</a>
            </div>
                            
            <h2>Particulares</h2>
            <table class="hover unstriped stack" id="listado">
                <thead>
                    <tr>
                        <th>Proveedor o contratista</th>
                        <th>Dependencia</th>
                        <th>Expediente</th>
                        <c:if test="${nivelDeAcceso.id == 5 || nivelDeAcceso.id == 1}">
                            <th>Usuario de captura</th>
                        </c:if>
                        <th>Detalles</th>
                        <c:if test="${nivelDeAcceso.id == 1 || nivelDeAcceso.id == 2}">
                            <th>Editar</th>
                            <th>Publicar</th>
                            <th>Ocultar</th>
                        </c:if>
                        <c:if test="${nivelDeAcceso.id == 1}">
                            <th>Borrar</th>
                        </c:if>
                    </tr>
                </thead>
                <tbody id="resultados">
                    <c:forEach items="${particularesSancionados}" var="particular">
                        <tr>
                            <td><strong class="hide-for-large">Proveedor o contratista:</strong> ${particular.nombreRazonSocial}</td>
                            <td><strong class="hide-for-large">Dependencia:</strong> ${particular.institucionDependenciaNombre}</td>
                            <td><strong class="hide-for-large">Expediente:</strong> ${particular.expediente}</td>
                            <c:if test="${nivelDeAcceso.id == 5 || nivelDeAcceso.id == 1}">
                                <td><strong class="hide-for-large">Usuario de captura</strong>${particular.usuarioRegistro}</td>
                            </c:if>
                            <td>
                                <form name="frmDetalle${particular.id}" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones">
                                    <input type="hidden" name="accion" value="detalleParticularSancionado"/>
                                    <input type="hidden" name="txtId" value="${particular.id}"/>
                                    <input type="hidden" name="txtRazonSocial" value="${filtro.razonSocial}"/>
                                    <input type="hidden" name="txtInstitucion" value="${filtro.institucion}"/>
                                    <input type="hidden" name="cmbPaginacion" value="${filtro.registrosMostrar}"/>
                                    <input type="hidden" name="cmbOrdenacion" value="${filtro.orden}"/>
                                    <input type="hidden" name="txtNumeroPagina" value="${filtro.numeroPagina}"/>
                                    <a onclick="document.frmDetalle${particular.id}.submit()"><i class="material-icons">info</i> <span class="hide-for-large">Detalle</span></a>
                                </form>
                            </td>
                            <c:if test="${nivelDeAcceso.id == 1 || nivelDeAcceso.id == 2}">
                                <td>
                                    <form name="frmEditar${particular.id}" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones">
                                        <c:choose>
                                            <c:when test="${particular.publicar == 0}">
                                                <input type="hidden" name="accion" value="mostrarPaginaEditarParticularSancionado"/>
                                                <input type="hidden" name="txtId" value="${particular.id}"/>
                                                <input type="hidden" name="txtRazonSocial" value="${filtro.razonSocial}"/>
                                                <input type="hidden" name="txtInstitucion" value="${filtro.institucion}"/>
                                                <input type="hidden" name="cmbPaginacion" value="${filtro.registrosMostrar}"/>
                                                <input type="hidden" name="cmbOrdenacion" value="${filtro.orden}"/>
                                                <input type="hidden" name="txtNumeroPagina" value="${filtro.numeroPagina}"/>
                                                <a onclick="document.frmEditar${particular.id}.submit()"><i class="material-icons">edit</i> <span class="hide-for-large">Editar</span></a>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="deshabilitado"><i class="material-icons">edit</i> <span class="hide-for-large">Editar</span></a>
                                            </c:otherwise>
                                        </c:choose>
                                    </form>
                                </td>
                                <td>
                                    <form name="frmPublicar${particular.id}" id="frmPublicar${particular.id}" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones">
                                        <c:choose>
                                            <c:when test="${particular.publicar == 0}">
                                                <input type="hidden" name="accion" value="publicarParticularSancionado"/>
                                                <input type="hidden" name="txtId" value="${particular.id}"/>
                                                <input type="hidden" name="txtRazonSocial" value="${filtro.razonSocial}"/>
                                                <input type="hidden" name="txtInstitucion" value="${filtro.institucion}"/>
                                                <input type="hidden" name="cmbPaginacion" value="${filtro.registrosMostrar}"/>
                                                <input type="hidden" name="cmbOrdenacion" value="${filtro.orden}"/>
                                                <input type="hidden" name="txtNumeroPagina" value="${filtro.numeroPagina}"/>
                                                <a onclick="confirmar('frmPublicar${particular.id}', '\u00bfEst\u00e1 seguro de publicar el registro?')"><i class="material-icons">public</i> <span class="hide-for-large">Publicar</span></a>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="deshabilitado"><i class="material-icons">public</i> <span class="hide-for-large ">Publicar</span></a>
                                            </c:otherwise>
                                        </c:choose>
                                    </form>
                                </td>
                                <td>
                                    <form name="frmDespublicar${particular.id}" id="frmDespublicar${particular.id}" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones">
                                        <c:choose>
                                            <c:when test="${particular.publicar != 0}">
                                                <input type="hidden" name="accion" value="despublicarParticularSancionado"/>
                                                <input type="hidden" name="txtId" value="${particular.id}"/>
                                                <input type="hidden" name="txtRazonSocial" value="${filtro.razonSocial}"/>
                                                <input type="hidden" name="txtInstitucion" value="${filtro.institucion}"/>
                                                <input type="hidden" name="cmbPaginacion" value="${filtro.registrosMostrar}"/>
                                                <input type="hidden" name="cmbOrdenacion" value="${filtro.orden}"/>
                                                <input type="hidden" name="txtNumeroPagina" value="${filtro.numeroPagina}"/>
                                                <a onclick="confirmar('frmDespublicar${particular.id}', '\u00bfEst\u00e1 seguro de ocultar el registro?')"><i class="material-icons">visibility_off</i> <span class="hide-for-large">Desactivar</span></a>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="deshabilitado"><i class="material-icons">visibility_off</i> <span class="hide-for-large ">Ocultar</span></a>
                                            </c:otherwise>
                                        </c:choose>
                                    </form>
                                </td>
                            </c:if>
                            <c:if test="${nivelDeAcceso.id == 1}">
                                <td>
                                    <form name="frmEliminar${particular.id}" id="frmEliminar${particular.id}" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones">
                                        <c:choose>
                                            <c:when test="${particular.publicar == 0}">
                                                <input type="hidden" name="accion" value="eliminarParticularSancionado"/>
                                                <input type="hidden" name="txtId" value="${particular.id}"/>
                                                <input type="hidden" name="txtRazonSocial" value="${filtro.razonSocial}"/>
                                                <input type="hidden" name="txtInstitucion" value="${filtro.institucion}"/>
                                                <input type="hidden" name="cmbPaginacion" value="${filtro.registrosMostrar}"/>
                                                <input type="hidden" name="cmbOrdenacion" value="${filtro.orden}"/>
                                                <input type="hidden" name="txtNumeroPagina" value="${filtro.numeroPagina}"/>
                                                <a onclick="confirmar('frmEliminar${particular.id}', '\u00bfEst\u00e1 seguro de eliminar el registro?')"><i class="material-icons">delete</i> <span class="hide-for-large">Borrar</span></a>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="deshabilitado"><i class="material-icons">delete</i> <span class="hide-for-large">Borrar</span></a>
                                            </c:otherwise>
                                        </c:choose>
                                    </form>
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    <c:if test="${particularesSancionados.size() == 0}">
                        <tr>
                            <td colspan="6">Sin resultados</td>
                        </tr>
                    </c:if>
                </tbody>
            </table>
            <c:if test="${particularesSancionados.size() > 0}">
                <p class="text-center">Mostrando <strong>${(particularesSancionados.size() > 0 ? 1 : 0) + (paginacion.registrosMostrar * (paginacion.numeroPagina - 1))}</strong> a <strong>${(paginacion.registrosMostrar * (paginacion.numeroPagina - 1)) + particularesSancionados.size()}</strong> de <strong>${paginacion.totalRegistros}</strong> registros</p>
                <c:if test="${particularesSancionados != null}">
                    <nav aria-label="Pagination">
                        <ul class="pagination text-center">
                            <c:set var="totalPaginas" value="${paginacion.totalPaginas}"></c:set>
                            <c:set var="numeroPaginaActual" value="${paginacion.numeroPagina}"></c:set>
                            <c:set var="numeroPaginas" value="${paginacion.numeroPagina}"></c:set>
                            <c:set var="totalRegistros" value="${paginacion.totalRegistros}"></c:set>
                            <c:if test="${numeroPaginas > 1 }">
                                <li class="pagination-previous">
                                    <a onclick="frmAnterior.submit()" aria-label="P&aacute;gina anterior">Anterior</a>
                                </li>admin
                                <c:choose>
                                    <c:when test="${numeroPaginaActual == 1}">
                                        <li class="current">
                                            1
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li>
                                            <a onclick="frm1.submit()" aria-label="P&aacute;gina 1">1</a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                                <c:if test="${numeroPaginas != 2}">
                                    <li class="ellipsis">
                                    </li>
                                </c:if>
                            </c:if>
                            <c:set var="limiteSuperior" value="${(totalPaginas - numeroPaginas < 3 ? totalPaginas : numeroPaginas + 3)}">
                            </c:set>
                            <c:forEach begin="${numeroPaginas}" end="${limiteSuperior -1}" >
                                <c:choose>
                                    <c:when test="${numeroPaginaActual == numeroPaginas}">
                                        <li class="current">
                                            ${numeroPaginas}
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li>
                                            <a onclick="frm${numeroPaginas}.submit()" aria-label="P&aacute;gina ${numeroPaginas}">
                                                ${numeroPaginas}
                                            </a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                                <c:set var="numeroPaginas" value="${numeroPaginas + 1 }"></c:set>
                            </c:forEach>
                            <c:if test="${numeroPaginaActual <= totalPaginas }">
                                <c:if test="${numeroPaginas < totalPaginas}">
                                    <li class="ellipsis">
                                    </li>
                                </c:if>
                                <c:choose>
                                    <c:when test="${numeroPaginaActual == numeroPaginas}">
                                        <li class="current">
                                            ${totalPaginas}
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li>
                                            <a onclick="frm${numeroPaginas}.submit()" aria-label="P&aacute;gina ${totalPaginas}">${totalPaginas}</a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                            <c:if test="${numeroPaginaActual < totalPaginas }">
                                <li class="pagination-next">
                                    <a onclick="frmSiguiente.submit();" aria-label="P&aacute;gina siguiente">Siguiente</a>
                                </li>
                            </c:if>
                        </ul>
                    </nav>

                    <c:set var="totalPaginas" value="${paginacion.totalPaginas}"></c:set>
                    <c:set var="numeroPaginaActual" value="${paginacion.numeroPagina}"></c:set>
                    <c:set var="numeroPaginas" value="${paginacion.numeroPagina}"></c:set>
                    <c:set var="totalRegistros" value="${paginacion.totalRegistros}"></c:set>

                    <c:if test="${numeroPaginas > 1 }">
                    <form name="frmAnterior" action="/sistemaCaptura${initParam.versionIntranet}/sanciones" method="post">
                        <input type="hidden" name="accion" value="consultarParticularesSancionados"/>
                        <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                        <input type="hidden" name="txtNumeroPagina" value="${numeroPaginas - 1}"/>
                        <input type="hidden" name="cmbOrdenacion" value="${filtro.orden}"/>
                       
                        <input type="hidden" name="txtId" value="${filtro.id}"/>
                        <input type="hidden" name="txtRazonSocial" value="${filtro.razonSocial}"/>
                        <input type="hidden" name="txtInstitucion" value="${filtro.institucion}"/>
                       
                    </form>
                    <form name="frm1" action="/sistemaCaptura${initParam.versionIntranet}/sanciones" method="post">
                            <input type="hidden" name="accion" value="consultarParticularesSancionados"/>
                            <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                            <input type="hidden" name="txtNumeroPagina" value="1"/>
                            <input type="hidden" name="cmbOrdenacion" value="${filtro.orden}"/>
                           
                            <input type="hidden" name="txtId" value="${filtro.id}"/>
                            <input type="hidden" name="txtRazonSocial" value="${filtro.razonSocial}"/>
                            <input type="hidden" name="txtInstitucion" value="${filtro.institucion}"/>
                       
                        </form>
                    </c:if>
                    <c:set var="limiteSuperior" value="${(totalPaginas - numeroPaginas < 3 ? totalPaginas : numeroPaginas + 3)}"></c:set>
                    <c:forEach begin="${numeroPaginas}" end="${limiteSuperior -1}" >
                        <form name="frm${numeroPaginas}" action="/sistemaCaptura${initParam.versionIntranet}/sanciones" method="post">
                            <input type="hidden" name="accion" value="consultarParticularesSancionados"/>
                            <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                            <input type="hidden" name="txtNumeroPagina" value="${numeroPaginas}"/>
                            <input type="hidden" name="cmbOrdenacion" value="${filtro.orden}"/>
                           
                            <input type="hidden" name="txtId" value="${filtro.id}"/>
                            <input type="hidden" name="txtRazonSocial" value="${filtro.razonSocial}"/>
                            <input type="hidden" name="txtInstitucion" value="${filtro.institucion}"/>
                           
                        </form>
                        <c:set var="numeroPaginas" value="${numeroPaginas + 1 }"></c:set>
                    </c:forEach>
                    <c:if test="${numeroPaginaActual <= totalPaginas }">
                        <form name="frm${numeroPaginas}" action="/sistemaCaptura${initParam.versionIntranet}/sanciones" method="post">
                            <input type="hidden" name="accion" value="consultarParticularesSancionados"/>
                            <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                            <input type="hidden" name="txtNumeroPagina" value="${totalPaginas}"/>
                            <input type="hidden" name="cmbOrdenacion" value="${filtro.orden}"/>
                           
                            <input type="hidden" name="txtId" value="${filtro.id}"/>
                            <input type="hidden" name="txtRazonSocial" value="${filtro.razonSocial}"/>
                            <input type="hidden" name="txtInstitucion" value="${filtro.institucion}"/>
                           
                        </form>
                    </c:if>
                    <c:if test="${numeroPaginaActual < totalPaginas }">
                    <form name="frmSiguiente" action="/sistemaCaptura${initParam.versionIntranet}/sanciones" method="post">
                        <input type="hidden" name="accion" value="consultarParticularesSancionados"/>
                        <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                        <input type="hidden" name="txtNumeroPagina" value="${numeroPaginaActual + 1}"/>
                        <input type="hidden" name="cmbOrdenacion" value="${filtro.orden}"/>
                       
                        <input type="hidden" name="txtId" value="${filtro.id}"/>
                        <input type="hidden" name="txtRazonSocial" value="${filtro.razonSocial}"/>
                        <input type="hidden" name="txtInstitucion" value="${filtro.institucion}"/>
                       
                    </form>
                    </c:if>
                </c:if>
            </c:if>
        </div>
    </div>
</div>

<c:if test="${irASeccion == true}">
    <script>
        var loc = window.location.href;
        window.location.href = loc + "#resultadosServidores";
    </script>
</c:if>

<%@include file="/piePagina.jsp" %>

<c:if test="${correcto}">
    <script>
        var json = ${mensaje};
        swal(json.mensaje, {
            icon: json.tipo,
            buttons: {
                confirm: {
                text: "OK",
                value: true,
                visible: true,
                className: "",
                closeModal: true
                }
            }
        });
    </script>
</c:if>
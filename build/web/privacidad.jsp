<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@include file="encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-privacidad.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="migas.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>Aviso de privacidad</h1>

            <div class="WordSection1">
                <p>
                    <strong>Secretar&iacute;a Ejecutiva del Sistema Estatal Anticorrupci&oacute;n</strong>
                </p>
                <p>
                    <strong>Direcci&oacute;n General de Servicios Tecnol&oacute;gicos y Plataforma Digital</strong>
                </p>
                <p>
                    <strong>Aviso de Privacidad Integral para la Plataforma Digital </strong>
                </p>
                <p>
                    <strong>(1) Revisi&oacute;n n&uacute;mero: 00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fecha de aprobaci&oacute;n: /01/2019</strong>
                </p>
                <p>
                    La Direcci&oacute;n General de Servicios Tecnol&oacute;gicos y Plataforma Digital (en adelante, &quot;La Direcci&oacute;n&quot;) de la Secretar&iacute;a Ejecutiva del Sistema Estatal Anticorrupci&oacute;n, es la responsable de llevar a cabo el tratamiento de los datos personales recabados, tratados y protegidos en la Plataforma Digital de la Secretar&iacute;a Ejecutiva relativos a informaci&oacute;n patrimonial, declaraci&oacute;n de intereses, declaraci&oacute;n fiscal, procedimientos de contrataciones p&uacute;blicas, servidores p&uacute;blicos y particulares sancionados, informaci&oacute;n y comunicaci&oacute;n del Sistema Estatal Anticorrupci&oacute;n y Sistema Municipales Anticorrupci&oacute;n, denuncias p&uacute;blicas de faltas administrativa, hechos de corrupci&oacute;n e informaci&oacute;n p&uacute;blica de contrataciones, asesor&iacute;a y apoyo t&eacute;cnico. Con el objeto de que conozca la manera en que protegemos sus datos y los derechos con que cuenta en esta materia, se le informa:
                </p>
                <p>
                    <strong>(2) &iquest;A qui&eacute;n va dirigido el presente aviso de privacidad?</strong>
                </p>
                <p>
                    A los particulares y servidores p&uacute;blicos a nivel estatal y municipal cuya finalidad sea crear cuentas de acceso y tr&aacute;mite para procedimientos, obligaciones y disposiciones a cargo de los sujetos obligados que integran el Sistema Estatal Anticorrupci&oacute;n y Sistemas Municipales Anticorrupci&oacute;n, previstos por la Ley del Sistema Estatal Anticorrupci&oacute;n del Estado de M&eacute;xico y Municipios, Ley General del Sistema Nacional Anticorrupci&oacute;n y Ley de Responsabilidades Administrativas del Estado de M&eacute;xico y Municipios o, en su caso, registrar el seguimiento de los procedimientos de referencia, cuando el tr&aacute;mite se efect&uacute;e de manera f&iacute;sica, as&iacute; como las finalidades que se derivan de la funci&oacute;n de los dem&aacute;s perfiles.
                </p>
                <p>
                    <strong>(3) &iquest;Qu&eacute; es un aviso de privacidad y cu&aacute;l es su utilidad?</strong>
                </p>
                <p>
                    El aviso de privacidad es el documento f&iacute;sico, electr&oacute;nico o en cualquier formato generado por el responsable que es puesto a disposici&oacute;n del Titular con el objeto de informarle los prop&oacute;sitos del tratamiento al que ser&aacute;n sometidos sus datos personales.
                </p>
                <p>
                    (4) A trav&eacute;s de dicho aviso, el responsable tiene la obligaci&oacute;n de informar de modo expreso, preciso e inequ&iacute;voco a las y los titulares, la informaci&oacute;n que se recaba de ellos y con qu&eacute; fines, la existencia y caracter&iacute;sticas principales del tratamiento al que ser&aacute;n sometidos sus datos personales, a fin de que puedan tomar decisiones informadas al respecto.
                </p>
                <p>
                    <strong>&nbsp;(5)&iquest;Qu&eacute; es un dato personal?</strong>
                </p>
                <p>
                    Se considera dato personal a cualquier informaci&oacute;n concerniente a una persona f&iacute;sica o jur&iacute;dica colectiva identificada o identificable, establecida en cualquier formato o modalidad y que est&eacute; almacenada en sistemas y/o bases de datos.
                </p>
                <p>
                    <strong>(6)&iquest;Qu&eacute; es un dato personal sensible?</strong>
                </p>
                <p>
                    Son datos personales referentes a la esfera m&aacute;s &iacute;ntima de su titular, cuya utilizaci&oacute;n indebida puede dar origen a discriminaci&oacute;n o conlleva un riesgo grave para &eacute;ste.
                </p>
                <p>
                    (7) De manera enunciativa, se consideran sensibles los datos personales que puedan revelar aspectos como origen racial o &eacute;tnico, estado de salud f&iacute;sica o mental, presente o futura, informaci&oacute;n gen&eacute;tica, creencias religiosas, filos&oacute;ficas y morales, opiniones pol&iacute;ticas y preferencia sexual.
                </p>
                <p>
                    <strong>(8)&iquest;Qu&eacute; es tratamiento de datos personales?</strong>
                </p>
                <p>
                    La Ley de Protecci&oacute;n de Datos Personales en Posesi&oacute;n de Sujetos Obligados del Estado de M&eacute;xico y Municipios (en adelante, &quot;La Ley&quot;) define al tratamiento como: las operaciones efectuadas por los procedimientos manuales o automatizados aplicados a los datos personales, relacionadas con la obtenci&oacute;n, uso, registro, organizaci&oacute;n, conservaci&oacute;n, elaboraci&oacute;n, utilizaci&oacute;n, comunicaci&oacute;n, difusi&oacute;n, almacenamiento, posesi&oacute;n, acceso, manejo, aprovechamiento, divulgaci&oacute;n, transferencia o disposici&oacute;n de datos personales.
                </p>
                <p>
                    <strong>(9)&iquest;De qu&eacute; manera se protegen mis datos personales en posesi&oacute;n de sujetos obligados (autoridades) del Estado de M&eacute;xico y sus Municipios?</strong>
                </p>
                <p>
                    La Ley tiene por objeto garantizar la protecci&oacute;n de los datos personales que se encuentran en posesi&oacute;n de los sujetos obligados, as&iacute; como establecer los principios, derechos, excepciones, obligaciones, sanciones y responsabilidades que rigen en la materia.
                </p>
                <p>
                    (10) As&iacute;, la Ley establece diversas obligaciones a cargo de los responsables del tratamiento de datos personales, tales como el aviso de privacidad, con el objeto de que el titular de los datos est&eacute; informado sobre qu&eacute; datos personales se recaban de &eacute;l y con qu&eacute; finalidad. De igual manera, regula la tramitaci&oacute;n de los derechos de Acceso, Rectificaci&oacute;n, Cancelaci&oacute;n y Oposici&oacute;n (ARCO), las transmisiones, la posibilidad de interponer denuncias por posibles violaciones a la Ley, as&iacute; como los medios por los cuales el Instituto de Transparencia, Acceso a la Informaci&oacute;n P&uacute;blica y Protecci&oacute;n de Datos Personales del Estado de M&eacute;xico y Municipios (Infoem) verificar&aacute; el cumplimiento de las disposiciones en la materia.
                </p>
                <p>
                    (11)Ahora bien, con el objeto de dar cumplimiento a lo establecido en el art&iacute;culo 31 de la Ley, se hace de su conocimiento lo siguiente:
                </p>
                <p>
                    <strong>(12) I. La denominaci&oacute;n del responsable.</strong>
                </p>
                <p>
                    Direcci&oacute;n General de Servicios Tecnol&oacute;gicos y Plataforma Digital de la Secretar&iacute;a Ejecutiva del Sistema Estatal Anticorrupci&oacute;n.
                </p>
                <p>
                    <strong>(13) II. El nombre y cargo del administrador, as&iacute; como el &aacute;rea o unidad administrativa a la que se encuentra adscrito.</strong>
                </p>
                <p>
                    <strong>A) Nombre del Administrador:</strong>
                    Ingeniero Jorge G&eacute;niz Pe&ntilde;a
                </p>
                <p>
                    <strong>B) Cargo:</strong>
                    Director General de Servicios Tecnol&oacute;gicos y Plataforma Digital
                </p>
                <p>
                    <strong>C) Área o Unidad Administrativa:</strong>
                    Direcci&oacute;n General de Servicios Tecnol&oacute;gicos y Plataforma Digital
                </p>
                <p>
                    -Correo electr&oacute;nico: jorge.geniz@sesaemm.org.mx
                </p>
                <p>
                    -Tel&eacute;fonos: 01 (722) 9 14 60 34
                </p>
                <p>
                    <strong>(14) III. El nombre del sistema de datos personales o base de datos al que ser&aacute;n incorporados los datos personales.</strong>
                </p>
                <p>
                    <strong>Nombre del sistema y/o base de datos personales:</strong>
                    Plataforma Digital
                </p>
                <p>
                    A)&nbsp;&nbsp;&nbsp; <strong>N&uacute;mero de Registro:</strong>
                    CBDP35718BECE006
                </p>
                <p>
                    <strong>(15) IV. Los datos personales que ser&aacute;n sometidos a tratamiento, identificando los que son sensibles. </strong>
                </p>
                <p>
                    Con el objeto de cumplir la finalidad establecida en el sistema, se podr&aacute; llevar a cabo el uso de los siguientes datos personales:
                </p>
                <p>
                    (16) <strong>Datos obligatorios:</strong>
                    Nombre, primer apellido y segundo apellido, nombre de usuario, contrase&ntilde;a de acceso, pregunta secreta y respuesta para la pregunta secreta.
                </p>
                <p>
                    (17) <strong>Datos facultativos:</strong>
                    Personalidad jur&iacute;dica, fecha de nacimiento, domicilio (calle, n&uacute;mero exterior, n&uacute;mero interior, colonia, estado, municipio, pa&iacute;s, c&oacute;digo postal) y datos de contacto (tel&eacute;fono con c&oacute;digo de &aacute;rea, tel&eacute;fono m&oacute;vil, fax con c&oacute;digo de &aacute;rea, correo electr&oacute;nico, autorizaci&oacute;n para recibir avisos por correo electr&oacute;nico).
                </p>
                <p>
                    (18) <strong>Los datos estad&iacute;sticos solicitados</strong>, como sexo, edad y ocupaci&oacute;n, son de car&aacute;cter facultativo.
                </p>
                <p>
                    (19) Los datos referidos se obtienen a trav&eacute;s de un formulario electr&oacute;nico, en el cual se obtiene su consentimiento para el tratamiento de los datos personales en las finalidades previstas en el presente aviso de privacidad, en el momento en el cual usted pulsa el bot&oacute;n &quot;registrar&quot;. Resulta importante mencionar que la veracidad y exactitud de los datos que se proporcionan son responsabilidad de las personas que se registran y utilizan la Plataforma Digital Estatal de la Secretar&iacute;a Ejecutiva.
                </p>
                <p>
                    (20) Los nombres de usuario y contrase&ntilde;as de todos los perfiles, de manera independiente a que se consideren datos personales, constituyen medidas de seguridad de identificaci&oacute;n y autenticaci&oacute;n. La informaci&oacute;n de referencia, en t&eacute;rminos de la Ley, podr&aacute; interoperar con la Plataforma Digital Nacional. Los servidores p&uacute;blicos que cuenten con clave de acceso al sistema de datos personales estar&aacute;n sujetos a la normatividad y pol&iacute;ticas que emita el responsable del sistema, la cual constituir&aacute; fuente obligacional para efectos de responsabilidades, con independencia de la entrega de responsivas o declaratorias.&nbsp;&nbsp;
                </p>
                <p>
                    (21) Dadas las caracter&iacute;sticas del uso de los datos personales de este sistema, no se recopilan datos personales sensibles y, por ende, el titular de dichos datos deber&aacute; abstenerse de remitirlos a la Direcci&oacute;n.
                </p>
                <p>
                    (22) No obstante, en caso de que personal de la Direcci&oacute;n identifique que, con motivo de una consulta, ha recibido datos personales sensibles, deber&aacute; registrarlo, a fin de establecer medidas especiales de protecci&oacute;n durante su tratamiento.
                </p>
                <p>
                    (23) Dichos datos personales sensibles deber&aacute;n ser eliminados a la brevedad del sistema, a trav&eacute;s de su devoluci&oacute;n al titular o su destrucci&oacute;n, seg&uacute;n proceda, una vez que se hubiera cumplido con la finalidad principal del sistema. Sin embargo, cuando se conserven documentos con datos personales sensibles por causas no imputables a la Direcci&oacute;n, dicha informaci&oacute;n solamente podr&aacute; ser conservada hasta en tanto dichos documentos no causen abandono a favor de la Secretar&iacute;a Ejecutiva y puedan ser suprimidos, de conformidad con los instrumentos de control archiv&iacute;stico correspondientes.
                </p>
                <p>
                    (24) <strong>V. El car&aacute;cter obligatorio o facultativo de la entrega de los datos personales.</strong>
                </p>
                <p>
                    Con el objeto de cumplir la finalidad establecida en el sistema, los datos que se enumeran en el apartado anterior deber&aacute;n ser de car&aacute;cter obligatorio para el uso y aprovechamiento de aqu&eacute;l, con excepci&oacute;n de los datos estad&iacute;sticos solicitados, como sexo, edad y ocupaci&oacute;n, que tienen car&aacute;cter facultativo.
                </p>
                <p>
                    (25) <strong>VI. Las consecuencias de la negativa a suministrarlos.</strong>
                </p>
                <p>
                    La negativa a suministrar los datos personales se&ntilde;alados provoca que el sistema no le permita crear la cuenta para el acceso y utilizaci&oacute;n de sistema y, en dado caso, que el propio sistema no le facilite de manera autom&aacute;tica la informaci&oacute;n para el ejercicio del derecho de acceso a la informaci&oacute;n p&uacute;blica.
                </p>
                <p>
                    (26) Asimismo, resulta importante mencionar que el resguardo y uso del nombre de usuario y contrase&ntilde;a son responsabilidad exclusiva del titular de la cuenta y, en consecuencia, son intransferibles, debido a que constituyen elementos de identificaci&oacute;n y autenticaci&oacute;n, por lo que cualquier tr&aacute;mite o acto realizados al amparo de su uso representa una extensi&oacute;n de la voluntad f&iacute;sica y, por lo tanto, es susceptible de producir efectos jur&iacute;dicos, as&iacute; como de exigir su eventual responsabilidad, en t&eacute;rminos de las disposiciones constitucionales, administrativas, civiles, penales o pol&iacute;ticas que procedan. Cualquier vulnerabilidad o afectaci&oacute;n en la confidencialidad de dichos datos deber&aacute; ser reportada ante el responsable del sistema de datos personales, a fin de que se establezcan alternativas de soluci&oacute;n, ya sean de car&aacute;cter preventivo o correctivo.
                </p>
                <p>
                    (27) Considerando que el personal de la Direcci&oacute;n se encuentra legitimado para el tratamiento de los datos personales en el ejercicio de una funci&oacute;n de derecho p&uacute;blico, en t&eacute;rminos del fundamento legal indicado en el art&iacute;culo 22 fracciones III, IV, X, XIII, XIV, XX de la Estatutos de referencia, no se podr&aacute; condicionar el servicio proporcionado a la entrega del aviso de privacidad.
                </p>
                <p>
                    Finalmente, se hace de su conocimiento que el personal de la Direcci&oacute;n, en concordancia con el principio de buena fe que rige el actuar de las autoridades, no califica la veracidad, integridad o autenticidad de la informaci&oacute;n proporcionada, a reserva de que exista disposici&oacute;n legal que as&iacute; lo exija o que puedan verse afectados derechos de terceros o del propio titular de los datos personales.
                </p>
                <p>
                    En el caso de los particulares que identifiquen informaci&oacute;n que, a su consideraci&oacute;n, sea confidencial, deber&aacute;n ejercer su derecho de oposici&oacute;n ante el sujeto obligado y/o responsable de sistema de datos personales que corresponda. No obstante, el responsable podr&aacute; establecer las medidas que resulten pertinentes conforme a sus pol&iacute;ticas, previa solicitud por parte del titular.
                </p>
                <p>
                    (28) <strong>VII. Las finalidades del tratamiento para las cuales se obtienen los datos personales, distinguiendo aqu&eacute;llas que requieran el consentimiento del titular.</strong>
                </p>
                <p>
                    <strong>A) Finalidad principal de tratamiento: </strong>Proporcionar una clave de usuario y contrase&ntilde;a, registr&aacute;ndose para tal efecto en el sistema, a efecto de cargar informaci&oacute;n relativa a las obligaciones de transparencia, as&iacute; como ser un repositorio a trav&eacute;s del cual la ciudadan&iacute;a puede acceder a la informaci&oacute;n considerada como obligaci&oacute;n de transparencia.
                </p>
                <p>
                    <strong>B) Finalidades secundarias:</strong>
                    Generar estad&iacute;stica en torno a los usuarios y las contrase&ntilde;as otorgados a los sujetos obligados y verificar el cumplimiento de las obligaciones de transparencia por parte del personal autorizado.
                </p>
                <p>
                    (29) <strong>VIII. Cuando se realicen transferencias de datos personales se informar&aacute;</strong>:
                </p>
                <p>
                    Los datos personales tratados se consideran como informaci&oacute;n confidencial, de conformidad con lo dispuesto por el art&iacute;culo 143, fracci&oacute;n I, de la Ley de Transparencia y Acceso a la Informaci&oacute;n P&uacute;blica, con excepci&oacute;n del nombre de los servidores p&uacute;blicos, las relativas a las funciones de servicio p&uacute;blico y la erogaci&oacute;n de recursos p&uacute;blicos, as&iacute; como cualquier otra informaci&oacute;n que permita transparentar las acciones y garantizar el derecho de acceso a la informaci&oacute;n p&uacute;blica, en virtud de que constituye informaci&oacute;n susceptible de ser publicada y difundida, de conformidad con lo previsto por los art&iacute;culos 23, p&aacute;rrafos pen&uacute;ltimo y &uacute;ltimo, y 92, fracci&oacute;n VII, de la Ley de Transparencia y Acceso a la Informaci&oacute;n P&uacute;blica del Estado de M&eacute;xico y Municipios. Del mismo modo, usted podr&aacute; autorizar, en cualquier momento, la publicidad y difusi&oacute;n de los datos personales que se consideran confidenciales, incluyendo los sensibles, lo que deber&aacute; constar de manera escrita, expresa e inequ&iacute;voca.
                </p>
                <p>
                    (30) Fuera de los supuestos establecidos en el art&iacute;culo 66 de la Ley, sus datos personales no podr&aacute;n ser transferidos. Es importante considerar que, en t&eacute;rminos del art&iacute;culo antes citado, eventualmente se podr&iacute;an llevar a cabo transferencias, a fin de hacer exigibles las responsabilidades y/o cr&eacute;ditos fiscales a su cargo. Asimismo, la informaci&oacute;n personal que usted proporcione ser&aacute; susceptible de ser utilizada para fines estad&iacute;sticos y de control, para lo cual, de manera previa, se disociar&aacute; la mayor cantidad de datos que pudieran hacer identificable a su titular, a fin de evitar una afectaci&oacute;n con la publicaci&oacute;n y/o difusi&oacute;n de los datos.
                </p>
                <p>
                    (31) En todos los casos, resulta importante mencionar que la publicidad de sus datos personales depender&aacute; proporcionalmente de lo establecido en las diversas leyes sobre el caso concreto y la expectativa de privacidad a la cual tenga derecho.
                </p>
                <p>
                    (32) <strong>IX. Los mecanismos y medios estar&aacute;n disponibles para el uso previo al tratamiento de los datos personales, para que el titular pueda manifestar su negativa para la finalidad y transferencia que requieran el consentimiento del titular.</strong>
                </p>
                <p>
                    En congruencia con las finalidades para el tratamiento de sus datos personales, no se cuenta con medios para la negativa de la finalidad y transferencia.
                </p>
                <p>
                    <strong>(33) X. Los mecanismos, medios y procedimientos disponibles para ejercer los derechos ARCO, indicando la direcci&oacute;n electr&oacute;nica del sistema para presentar sus solicitudes.</strong>
                    <strong></strong>
                </p>
                <p>
                    Los derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n y oposici&oacute;n com&uacute;nmente se conocen como derechos ARCO.
                </p>
                <p>
                    (34) Los derechos ARCO son independientes. El ejercicio de cualquiera de ellos no es requisito previo ni impide el ejercicio de otro.
                </p>
                <p>
                    (35) La procedencia de estos derechos, en su caso, se har&aacute; efectiva una vez que el titular o su representante legal acrediten su identidad o representaci&oacute;n, respectivamente.
                </p>
                <p>
                    (36) En ning&uacute;n caso el acceso a los datos personales de un titular podr&aacute; afectar los derechos y libertades de otros.
                </p>
                <p>
                    (37) El ejercicio de cualquiera de los derechos ARCO forma parte de las garant&iacute;as primarias del derecho a la protecci&oacute;n de datos personales.
                </p>
                <p>
                    (38) <strong>Derecho de acceso.</strong>
                    El titular tiene derecho a acceder, solicitar y ser informado sobre sus datos personales en posesi&oacute;n de los sujetos obligados, as&iacute; como la informaci&oacute;n relacionada con las condiciones y generalidades de su tratamiento, como el origen de los datos, las condiciones del tratamiento del cual sean objeto, las cesiones realizadas o que se pretendan realizar, as&iacute; como tener acceso al aviso de privacidad al que est&aacute; sujeto el tratamiento y a cualquier otra generalidad del tratamiento, en los t&eacute;rminos previstos en la Ley.
                </p>
                <p>
                    (39) <strong>Derecho de rectificaci&oacute;n.</strong>
                    El titular tendr&aacute; derecho a solicitar la rectificaci&oacute;n de sus datos personales cuando sean inexactos, incompletos, desactualizados, inadecuados o excesivos.
                </p>
                <p>
                    (40) <strong>Derecho de cancelaci&oacute;n.</strong>
                    El titular tendr&aacute; derecho a solicitar la cancelaci&oacute;n de sus datos personales de los archivos, registros, expedientes y sistemas del responsable, a fin de que ya no est&eacute;n en su posesi&oacute;n y dejen de ser tratados por este &uacute;ltimo.
                </p>
                <p>
                    (41) El responsable no estar&aacute; obligado a cancelar los datos personales cuando:
                </p>
                <p>
                    I. Deban ser tratados por disposici&oacute;n legal.
                </p>
                <p>
                    II. Se refieran a las partes de un contrato y sean necesarios para su desarrollo y cumplimiento.
                </p>
                <p>
                    III. Obstaculicen actuaciones judiciales o administrativas, la investigaci&oacute;n y persecuci&oacute;n de delitos o la actualizaci&oacute;n de sanciones administrativas, afecten la seguridad o salud p&uacute;blica, disposiciones de orden p&uacute;blico, o derechos de terceros.
                </p>
                <p>
                    IV. Sean necesarios para proteger los intereses jur&iacute;dicamente tutelados del titular o de un tercero.
                </p>
                <p>
                    V. Sean necesarios para realizar una acci&oacute;n en funci&oacute;n del inter&eacute;s p&uacute;blico.
                </p>
                <p>
                    VI. Se requieran para cumplir con una obligaci&oacute;n legalmente adquirida por el titular.
                </p>
                <p>
                    (42) <strong>Derecho de oposici&oacute;n.</strong>
                    El titular tendr&aacute; derecho, en todo momento y por razones leg&iacute;timas, a oponerse al tratamiento de sus datos personales para una o varias finalidades o a exigir su cese, en los supuestos siguientes:
                </p>
                <p>
                    I. Cuando los datos se hubiesen recabado sin su consentimiento y &eacute;ste resultara exigible en t&eacute;rminos de esta Ley y disposiciones aplicables.
                </p>
                <p>
                    II. Aun siendo l&iacute;cito el tratamiento, el mismo debe cesar para evitar que su persistencia cause un da&ntilde;o o perjuicio al titular.
                </p>
                <p>
                    III. Sus datos personales sean objeto de un tratamiento automatizado, el cual le produzca efectos jur&iacute;dicos no deseados o afecte de manera significativa sus intereses, derechos o libertades y est&eacute;n destinados a evaluar, sin intervenci&oacute;n humana, determinados aspectos personales del mismo o analizar o predecir, en particular, su rendimiento profesional, situaci&oacute;n econ&oacute;mica, estado de salud, preferencias sexuales, fiabilidad o comportamiento.
                </p>
                <p>
                    IV. Cuando la o el titular identifique que se han asociado datos personales o se le ha identificado con un registro del cu&aacute;l no sea titular o se le incluya dentro de un sistema de datos personales en el cual no tenga correspondencia.
                </p>
                <p>
                    V. Cuando existan motivos fundados para ello y la Ley no disponga lo contrario.
                </p>
                <p>
                    (43) Dichos derechos se podr&aacute;n ejercer indistintamente a trav&eacute;s del Sistema de Acceso, Rectificaci&oacute;n, Cancelaci&oacute;n y Oposici&oacute;n de Datos Personales del Estado de M&eacute;xico (Sarcoem, www.sarcoem.org.mx y/o www.plataformadetransparencia.org.mx) o en la forma que m&aacute;s le convenga al titular, ya sea de manera escrita o verbal, directamente o a trav&eacute;s de apoderado o representante legal, por correo certificado o servicio de mensajer&iacute;a, ante la Unidad de Planeaci&oacute;n y Transparencia de la Secretar&iacute;a Ejecutiva del Sistema Estatal Anticorrupci&oacute;n.
                </p>
                <p>
                    (44) XI. La indicaci&oacute;n por la cual el titular podr&aacute; revocar el consentimiento para el tratamiento de sus datos, detallando el procedimiento a seguir para tal efecto. De manera general, solamente proceder&aacute; la revocaci&oacute;n y, por ende, el cese en el tratamiento de los datos personales cuando &eacute;stos hubieran sido obtenidos con su consentimiento y, a su vez, no exista impedimento legal o contractual para ello. Lo anterior, en el entendido de que el ingreso de cualquier documento o la anotaci&oacute;n en cualquier registro tiene el car&aacute;cter de p&uacute;blico, por lo que la legislaci&oacute;n en materia de archivos establece disposiciones para su protecci&oacute;n, preservaci&oacute;n y conservaci&oacute;n.
                </p>
                <p>
                    (45) En caso de que resulte procedente, solamente en cada caso concreto se podr&aacute; determinar el alcance de la revocaci&oacute;n del consentimiento, ya sea cancelando la informaci&oacute;n, cesando y bloqueando su uso, o estableciendo medidas especiales de protecci&oacute;n, por lo que se le solicita atentamente establecer comunicaci&oacute;n con la Direcci&oacute;n, para poderle proponer una soluci&oacute;n a su requerimiento que sea congruente con las disposiciones de la materia.
                </p>
                <p>
                    (46) Para solicitar la revocaci&oacute;n del consentimiento para el tratamiento de sus datos personales, deber&aacute; ingresar un escrito libre, dirigido ante el Director, en el cual indique lo siguiente:
                </p>
                <p>
                    - Nombre completo.
                </p>
                <p>
                    - Sistema de datos personales en el cual obra la informaci&oacute;n respectiva (Plataforma Digital).
                </p>
                <p>
                    - Datos personales sobre los cuales se revoca el consentimiento para el tratamiento.
                </p>
                <p>
                    - Manifestaci&oacute;n de que conoce las consecuencias que conlleva la revocaci&oacute;n y que las asume a su entero perjuicio.
                </p>
                <p>
                    - Firma aut&oacute;grafa o huella digital.
                </p>
                <p>
                    (47) Una vez ingresado el escrito, deber&aacute; comparecer personalmente ante la Unidad de Planeaci&oacute;n y Transparencia correspondiente dentro del plazo de tres d&iacute;as h&aacute;biles siguientes a su presentaci&oacute;n (pudiendo presentarse y ratificarse el mismo d&iacute;a), con el objeto de que le sean explicadas las consecuencias de la revocaci&oacute;n y se ratifique su voluntad para llevar a cabo dicho acto, momento en el cual, en caso de ser procedente, se tomar&aacute;n las providencias necesarias para que surta sus efectos.
                </p>
                <p>
                    (48) Es importante precisar que, en caso de que la revocaci&oacute;n del consentimiento d&eacute; lugar a la cancelaci&oacute;n, de manera previa deber&aacute; establecerse un periodo de bloqueo de tres meses, con el objeto de confirmar su procedencia, as&iacute; como la falta de impedimento legal o inter&eacute;s p&uacute;blico involucrado.
                </p>
                <p>
                    (49) XII. Opciones y medios que el responsable ofrezca a los titulares para limitar el uso o divulgaci&oacute;n, o la portabilidad de datos. La Plataforma Digital puede establecer opciones para facilitar su uso, para lo cual se requerir&aacute;n permisos o autorizaciones previstos por el propio sistema. En caso de que considere que el tratamiento no resulta adecuado conforme a las autorizaciones, podr&aacute;n establecerse opciones para habilitarlas o deshabilitarlas, con las cuales podr&aacute; limitar el uso o divulgaci&oacute;n de sus datos.
                </p>
                <p>
                    (50) La Plataforma Digital es un sistema en un continuo proceso de mejora. En caso de que identifique alg&uacute;n m&oacute;dulo en el sistema en el que usted, como titular de los datos, advierta alg&uacute;n supuesto en el que resulte pertinente limitar el uso o divulgaci&oacute;n de sus datos y que el propio sistema no le brinde la opci&oacute;n respectiva, se le sugiere establecer comunicaci&oacute;n con el responsable del sistema de datos personales, para que valore la pertinencia y procedencia de su solicitud.
                </p>
                <p>
                    (51)<strong>
                        XIII. Los medios a trav&eacute;s de los cuales el responsable comunicar&aacute; a los titulares los cambios al aviso de privacidad.</strong>
                </p>
                <p>
                    Este aviso de privacidad es un documento controlado, se identifica con el n&uacute;mero 00, aprobado el 00/00/0000. Es importante que lo sepa, ya que estos son los t&eacute;rminos que rigen el tratamiento al momento de la obtenci&oacute;n de sus datos. Cualquier modificaci&oacute;n que sufra el presente documento deber&aacute; ser reportada en el apartado identificado como &quot;control de cambios&quot;.
                </p>
                <p>
                    (52) Sin embargo, el presente documento puede sufrir cambios, modificaciones y/o actualizaci&oacute;n en funci&oacute;n de requerimientos legales, administrativos o de mejora, que eventualmente pueden cambiar los t&eacute;rminos y/o condiciones en el tratamiento de sus datos personales, por lo cual, en caso de que sea su inter&eacute;s identificar el tratamiento vigente de sus datos personales o ejercer su derecho, se le invita consultar el aviso de privacidad vigente en la p&aacute;gina de internet de la Secretar&iacute;a Ejecutiva, en la direcci&oacute;n www.sesaemm.org.mx, en el apartado de Avisos de Privacidad, donde podr&aacute; consultar e imprimir el presente aviso de privacidad.
                </p>
                <p>
                    (53) Asimismo, en caso de que as&iacute; lo prefiera, podr&aacute; acudir directamente ante la Direcci&oacute;n o la Unidad de Planeaci&oacute;n y Transparencia de la Secretar&iacute;a Ejecutiva.
                </p>
                <p>
                    (54) <strong>XIV. El cargo y domicilio del encargado, indicando su nombre o el medio por el cual se pueda conocer su identidad. </strong>
                </p>
                <p>
                    Se precisa que, para efecto del presente sistema de datos personales, no existe encargado.
                </p>
                <p>
                    (55) <strong>XV. El domicilio de la Secretar&iacute;a.</strong>
                </p>
                <p>
                    Avenida Venustiano Carranza 607, colonia Cuauht&eacute;moc, c&oacute;digo postal 50130, Toluca, Estado de M&eacute;xico.
                </p>
                <p>
                    (56) <strong>XVI. El fundamento legal que faculta al responsable para llevar a cabo el tratamiento.</strong>
                </p>
                <p>
                    Art&iacute;culos 27 fracci&oacute;n V, 35 fracci&oacute;n XIV, 48, 49 y 50, Ley del Sistema Anticorrupci&oacute;n del Estado de M&eacute;xico y Municipios; 7, 18 fracci&oacute;n XIV, 22 fracciones III, IV, XIII, XIV, XVIII, XX del Estatuto Org&aacute;nico de la Secretar&iacute;a Ejecutiva del Sistema Estatal Anticorrupci&oacute;n.&nbsp;&nbsp;&nbsp;&nbsp;
                </p>
                <p>
                    (57) <strong>XVII. El procedimiento para que se ejerza el derecho a la portabilidad.</strong>
                </p>
                <p>
                    (58) En raz&oacute;n del procedimiento del que se trata, para el cumplimiento de las atribuciones establecidas en la Ley, no es aplicable el presente apartado.
                </p>
                <p>
                    (58) <strong>XVIII. El domicilio de la Unidad de Planeaci&oacute;n y Transparencia.</strong>
                </p>
                <p>
                    Avenida Venustiano Carranza No. 607, Colonia Cuauht&eacute;moc, Toluca, Estado de M&eacute;xico, C&oacute;digo Postal 50130.
                </p>
                <p>
                    (59) <strong>XIX. Datos de contacto del instituto, incluidos domicilio, direcci&oacute;n del portal informativo, correo electr&oacute;nico y tel&eacute;fono del Centro de Atenci&oacute;n Telef&oacute;nica, para que el titular pueda recibir asesor&iacute;a o presentar denuncias por violaciones a las disposiciones de la Ley.</strong>
                </p>
                <p>
                    <strong>Datos de Contacto del Infoem: </strong>
                </p>
                <p>
                    a. <strong>Tel&eacute;fonos:</strong>
                    01 (722) 226 19 80 (conmutador).
                </p>
                <p>
                    b. <strong>Direcci&oacute;n del Portal Informativo:</strong>
                    <a href="http://www.infoem.org.mx/">http://www.infoem.org.mx/</a>
                </p>
                <p>
                    c. <strong>Correo electr&oacute;nico del Centro de Atenci&oacute;n Telef&oacute;nica (CAT):</strong>
                    <a href="mailto:cat@infoem.org.mx">cat@infoem.org.mx</a>
                </p>
                <p>
                    d. <strong>Tel&eacute;fono del CAT:</strong>
                    01 800 821 04 41
                </p>
                <p>
                    e. <strong>Direcci&oacute;n:</strong>
                    Calle de Pino Su&aacute;rez S/N, actualmente Carretera Toluca-Ixtapan No. 111, Col. La Michoacana, Metepec, Estado de M&eacute;xico, C.P. 52166.
                </p>
                <p>
                    (60) <strong>Asesor&iacute;a y orientaci&oacute;n en materia de protecci&oacute;n de datos personales. </strong>
                </p>
                <p>
                    En caso de que requiera asesor&iacute;a u orientaci&oacute;n en materia de protecci&oacute;n de datos personales&nbsp; o sobre los derechos previstos por la Ley de Protecci&oacute;n de Datos Personales en Posesi&oacute;n de Sujetos Obligados del Estado de M&eacute;xico y Municipios, se le sugiere dirigirse a la Direcci&oacute;n de Protecci&oacute;n de Datos Personales del Infoem, a trav&eacute;s del tel&eacute;fono 01 (722) 226 19 80 (conmutador) o, en su caso enviar correo electr&oacute;nico a la direcci&oacute;n <a href="mailto:datospersonales@infoem.org.mx">datospersonales@infoem.org.mx</a>.
                </p>
                <p>
                    (61) <strong>Control de cambios. </strong>
                </p>
            </div>

        </div>
    </div>
</div>

<%@include file="piePagina.jsp" %>
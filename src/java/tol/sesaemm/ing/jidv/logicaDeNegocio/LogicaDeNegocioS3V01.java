/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.logicaDeNegocio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.bson.Document;
import tol.sesaemm.funciones.ExpresionesRegulares;
import tol.sesaemm.funciones.ManejoDeFechas;
import tol.sesaemm.funciones.ObtenerRfc;
import tol.sesaemm.ing.jidv.integracion.DAOSanciones;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocioS3;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.FILTRO_BUSQUEDA_PSANCIONADOS;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.PsancionadosConPaginacion;
import tol.sesaemm.javabeans.ASENTAMIENTO;
import tol.sesaemm.javabeans.DOCUMENTO;
import tol.sesaemm.javabeans.ENTE_PUBLICO;
import tol.sesaemm.javabeans.ENTIDAD_FEDERATIVA_PDE;
import tol.sesaemm.javabeans.GENERO;
import tol.sesaemm.javabeans.INHABILITACION;
import tol.sesaemm.javabeans.INSTITUCION_DEPENDENCIA;
import tol.sesaemm.javabeans.METADATA;
import tol.sesaemm.javabeans.METADATOS;
import tol.sesaemm.javabeans.MONEDA;
import tol.sesaemm.javabeans.MUNICIPIO_PDE;
import tol.sesaemm.javabeans.ORGANO_INTERNO_CONTROL;
import tol.sesaemm.javabeans.PAIS;
import tol.sesaemm.javabeans.PUESTO;
import tol.sesaemm.javabeans.TIPO_DOCUMENTO;
import tol.sesaemm.javabeans.TIPO_PERSONA;
import tol.sesaemm.javabeans.UNIDAD_MEDIDA_PLAZO;
import tol.sesaemm.javabeans.s3psancionados.DOMICILIO_EXTRANJERO;
import tol.sesaemm.javabeans.s3psancionados.DOMICILIO_MEXICO;
import tol.sesaemm.javabeans.s3psancionados.ENTIDAD_FEDERATIVA;
import tol.sesaemm.javabeans.s3psancionados.LOCALIDAD;
import tol.sesaemm.javabeans.s3psancionados.MULTA_PSANCIONADOS;
import tol.sesaemm.javabeans.s3psancionados.MUNICIPIO;
import tol.sesaemm.javabeans.s3psancionados.PARTICULAR_SANCIONADO;
import tol.sesaemm.javabeans.s3psancionados.PERSONA;
import tol.sesaemm.javabeans.s3psancionados.PSANCIONADOS;
import tol.sesaemm.javabeans.s3psancionados.RESOLUCION_PAR;
import tol.sesaemm.javabeans.s3psancionados.RESPONSABLE_SANCION;
import tol.sesaemm.javabeans.s3psancionados.TIPO_FALTA;
import tol.sesaemm.javabeans.s3psancionados.TIPO_SANCION_PAR;
import tol.sesaemm.javabeans.s3psancionados.VIALIDAD;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */
public class LogicaDeNegocioS3V01 implements LogicaDeNegocioS3
{

    /**
     * Obtener listado de las dependencias que el usuario puede modificar o
     * consultar
     *
     * @param usuario Usuario en sesión
     *
     * @return ArrayList Listado de dependencias
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ENTE_PUBLICO> obtenerDependenciasDelUsuario(USUARIO usuario) throws Exception
    {
        ArrayList<ENTE_PUBLICO> dependencias = usuario.getDependencias();
        return dependencias;
    }

    /**
     * Devuelve la lista de puestos registrado en la base de datos
     *
     * @return ArrayList PUESTO
     *
     * @throws Exception
     */
    @Override
    public ArrayList<PUESTO> obtenerListaDePuestos() throws Exception
    {
        return DAOSanciones.obtenerListaDePuestos();
    }

    /**
     * Devuelve la lista de g&eacute;neros almacenados en la base de datos
     *
     * @return ArrayList GENERO
     */
    @Override
    public ArrayList<GENERO> obtenerListaDeGeneros() throws Exception
    {
        return DAOSanciones.obtenerListaDeGeneros();
    }

    /**
     * Devuelve el listado de las diferentes dependencias que est&aacute;n
     * registradas en la base de datos
     *
     * @param usuario
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ENTE_PUBLICO> obtenerEntesPublicos(USUARIO usuario) throws Exception
    {
        return DAOSanciones.obtenerEntesPublicos(usuario);
    }

    /**
     * Devuelve la lista de los tipos de monedas registrados en la base de datos
     *
     * @return ArrayList MONEDA
     */
    @Override
    public ArrayList<MONEDA> obtenerListaDeTiposDeMoneda() throws Exception
    {
        return DAOSanciones.obtenerListaDeTiposDeMoneda();
    }

    /**
     * Devuelve un arreglo que contiene los atributos de los Documentos
     *
     * @param request HttpServletRequest
     *
     * @return ArrayList DOCUMENTO
     *
     * @throws Exception
     */
    private ArrayList<DOCUMENTO> obtenerLaCantidadDeValoresCadaDocumento(HttpServletRequest request) throws Exception
    {
        ArrayList<DOCUMENTO> listaDocumentos;                                                   
        DOCUMENTO documento;                                                                    
        ManejoDeFechas formatoFechas;                                                           
        listaDocumentos = new ArrayList<>();
        formatoFechas = new ManejoDeFechas("yyyy-MM-dd");
        String[] listaDeIdentificadoresDeDocumentosARegistrar;          

        try
        {
            if (request.getParameter("listaDeIdentificadoresDeDocumentosARegistrar") != null)
            { 
                listaDeIdentificadoresDeDocumentosARegistrar = request.getParameter("listaDeIdentificadoresDeDocumentosARegistrar").split(",");

                for (String identificadorDocumento : listaDeIdentificadoresDeDocumentosARegistrar)
                { 
                    documento = new DOCUMENTO();
                    documento.setId((request.getParameter("txtDOCUMENTO_ID_" + identificadorDocumento) == null) ? "" : request.getParameter("txtDOCUMENTO_ID_" + identificadorDocumento));
                    documento.setTipo((request.getParameter("txtDOCUMENTO_TIPO_" + identificadorDocumento) == null || request.getParameter("txtDOCUMENTO_TIPO_" + identificadorDocumento).equals("")) ? "" : DAOSanciones.obtenerDocumento(request.getParameter("txtDOCUMENTO_TIPO_" + identificadorDocumento)).getValor());
                    documento.setTitulo((request.getParameter("txtDOCUMENTO_TITULO_" + identificadorDocumento) == null) ? "" : request.getParameter("txtDOCUMENTO_TITULO_" + identificadorDocumento));
                    documento.setDescripcion((request.getParameter("txtDOCUMENTO_DESCRIPCION_" + identificadorDocumento) == null) ? "" : request.getParameter("txtDOCUMENTO_DESCRIPCION_" + identificadorDocumento));
                    documento.setUrl((request.getParameter("txtDOCUMENTO_URL_" + identificadorDocumento) == null) ? "" : request.getParameter("txtDOCUMENTO_URL_" + identificadorDocumento));
                    documento.setFecha((request.getParameter("txtDOCUMENTO_FECHA_" + identificadorDocumento) == null) ? "" : formatoFechas.getParseAString(request.getParameter("txtDOCUMENTO_FECHA_" + identificadorDocumento)));
                    if (documento.getId().isEmpty() == false)
                    { 
                        listaDocumentos.add(documento);
                    } 
                } 
            } 
            else
            { 
                for (int i = 0; i <= (Integer.parseInt(request.getParameter("txtDOCUMENTO_NUM"))); i++)
                { 
                    documento = new DOCUMENTO();
                    documento.setId((request.getParameter("txtDOCUMENTO_ID_" + i) == null) ? "" : request.getParameter("txtDOCUMENTO_ID_" + i));
                    documento.setTipo((request.getParameter("txtDOCUMENTO_TIPO_" + i) == null || request.getParameter("txtDOCUMENTO_TIPO_" + i).equals("")) ? "" : DAOSanciones.obtenerDocumento(request.getParameter("txtDOCUMENTO_TIPO_" + i)).getValor());
                    documento.setTitulo((request.getParameter("txtDOCUMENTO_TITULO_" + i) == null) ? "" : request.getParameter("txtDOCUMENTO_TITULO_" + i));
                    documento.setDescripcion((request.getParameter("txtDOCUMENTO_DESCRIPCION_" + i) == null) ? "" : request.getParameter("txtDOCUMENTO_DESCRIPCION_" + i));
                    documento.setUrl((request.getParameter("txtDOCUMENTO_URL_" + i) == null) ? "" : request.getParameter("txtDOCUMENTO_URL_" + i));
                    documento.setFecha((request.getParameter("txtDOCUMENTO_FECHA_" + i) == null) ? "" : formatoFechas.getParseAString(request.getParameter("txtDOCUMENTO_FECHA_" + i)));
                    if (documento.getId().isEmpty() == false)
                    { 
                        listaDocumentos.add(documento);
                    } 
                } 
            } 
        }
        catch (Exception e)
        {
            throw new Exception("Error al intentar leer los datos de los documentos del servidor publico sancionado: " + e.toString());
        }
        return listaDocumentos;
    }

    /**
     * Devuelve el listado de las unidades de medida para el plazo de
     * inhabilitaci&oacute;n
     *
     * @return ArrayList UNIDAD_MEDIDA_PLAZO
     *
     * @throws Exception
     */
    @Override
    public ArrayList<UNIDAD_MEDIDA_PLAZO> obtenerListaDeUnidadMedidaPlazo() throws Exception
    {
        return DAOSanciones.obtenerListaDeUnidadMedidaPlazo();
    }

    /**
     * Devuelve el listado de los &oacute;rgano internos de control de la base
     * de datos
     *
     * @return ArrayList ORGANO_INTERNO_CONTROL
     *
     * @throws java.lang.Exception
     */
    @Override
    public ArrayList<ORGANO_INTERNO_CONTROL> obtenerListaDeOrganoInternoDeControl() throws Exception
    {
        return DAOSanciones.obtenerListaDeOrganoInternoDeControl();
    }

    /**
     * Devuelve el valor correspondiente al &oacute;rgano interno de control por
     * el filtro de id
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public ORGANO_INTERNO_CONTROL obtenerOrganoInternoDeControl(String parameter) throws Exception
    {
        return DAOSanciones.obtenerOrganoInternoDeControl(parameter);
    }

    /**
     * Modelado para el filtro de particulares sancionados
     *
     * @param request HttpServletRequest
     *
     * @return FILTRO_BUSQUEDA_PSANCIONADOS
     */
    @Override
    public FILTRO_BUSQUEDA_PSANCIONADOS modelarFiltroParticularSancionado(HttpServletRequest request)
    {
        FILTRO_BUSQUEDA_PSANCIONADOS filtro;                            
        int numeroPagina;                                               
        int registrosMostrar;                                           
        String orden;                                                   
        String id;                                                      
        String razonSocial;                                             
        String institucion;                                             

        filtro = new FILTRO_BUSQUEDA_PSANCIONADOS();

        registrosMostrar = (request.getParameter("cmbPaginacion") == null) ? 10 : Integer.parseInt(request.getParameter("cmbPaginacion"));
        numeroPagina = (request.getParameter("txtNumeroPagina") == null ? 1 : Integer.parseInt(request.getParameter("txtNumeroPagina")));
        orden = (request.getParameter("cmbOrdenacion") == null) ? "nombreRazonSocial" : request.getParameter("cmbOrdenacion");
        id = (request.getParameter("txtId") == null) ? "" : request.getParameter("txtId");

        razonSocial = (request.getParameter("txtRazonSocial") == null) ? "" : request.getParameter("txtRazonSocial");
        institucion = (request.getParameter("txtInstitucion") == null) ? "" : request.getParameter("txtInstitucion");

        filtro.setId(id);
        filtro.setRazonSocial(razonSocial.trim());
        filtro.setInstitucion(institucion.trim());
        filtro.setNumeroPagina(numeroPagina);
        filtro.setRegistrosMostrar(registrosMostrar);
        filtro.setOrden(orden);
        return filtro;
    }

    /**
     * Devuelve el tipo de sanci&oacute;n y su descripci&oacute;n de
     * particulares sancionados
     *
     * @param request HttpServletRequest
     *
     * @return ArrayList TIPO_SANCION_PAR
     *
     * @throws Exception
     */
    private ArrayList<TIPO_SANCION_PAR> obtenerTiposDeSancionParticulares(HttpServletRequest request) throws Exception
    {
        ArrayList<TIPO_SANCION_PAR> tipoSancion;                                
        List<String> codSancion;                                                

        codSancion = Arrays.asList(request.getParameterValues("chckTIPO_SANCION"));
        tipoSancion = DAOSanciones.obtenerListaTiposDeSancionPsancionados(codSancion);

        for (int i = 0; i < tipoSancion.size(); i++)
        { 
            if (request.getParameter("txtTIPO_SANCION_" + tipoSancion.get(i).getClave() + "_DESCRIPCION") == null || request.getParameter("txtTIPO_SANCION_" + tipoSancion.get(i).getClave() + "_DESCRIPCION").isEmpty())
            { 
                tipoSancion.get(i).setDescripcion(null);
            } 
            else
            { 
                tipoSancion.get(i).setDescripcion(request.getParameter("txtTIPO_SANCION_" + tipoSancion.get(i).getClave() + "_DESCRIPCION"));
            } 
        } 

        return tipoSancion;
    }

    /**
     * Modela el objeto final de particular sancionado
     *
     * @param request HttpServletRequest
     *
     * @return PSANCIONADOS
     */
    @Override
    public PSANCIONADOS modelarRequestParticularSancionado(HttpServletRequest request) throws Exception
    {
        PSANCIONADOS particular;                                                
        INSTITUCION_DEPENDENCIA institucionDependencia;                         
        PARTICULAR_SANCIONADO particularSancionado;                             
        DOMICILIO_MEXICO domicilioMexico;                                       
        DOMICILIO_EXTRANJERO domicilioExtranjero;                               
        PERSONA directorGeneral;                                                
        PERSONA apoderadoLegal;                                                 
        RESPONSABLE_SANCION responsableSancion;                                 
        RESOLUCION_PAR resolucion;                                              
        MULTA_PSANCIONADOS multa;                                               
        INHABILITACION inhabilitacion;                                          
        Date dateInicial;                                                       
        Date dateFinal;                                                         
        ManejoDeFechas formatoFechas;                                           
        String fechaInicial;                                                    
        String fechaFinal;                                                      
        UNIDAD_MEDIDA_PLAZO medidaPlazo;                                        
        VIALIDAD vialidad;                                                      

        particular = new PSANCIONADOS();
        particularSancionado = new PARTICULAR_SANCIONADO();
        domicilioMexico = new DOMICILIO_MEXICO();
        domicilioExtranjero = new DOMICILIO_EXTRANJERO();
        directorGeneral = new PERSONA();
        apoderadoLegal = new PERSONA();
        responsableSancion = new RESPONSABLE_SANCION();
        resolucion = new RESOLUCION_PAR();
        multa = new MULTA_PSANCIONADOS();
        inhabilitacion = new INHABILITACION();
        vialidad = new VIALIDAD();
        formatoFechas = new ManejoDeFechas("yyyy-MM-dd");

        particular.setExpediente((request.getParameter("txtNUMERO_EXPEDIENTE") == null || request.getParameter("txtNUMERO_EXPEDIENTE").isEmpty()) ? null : request.getParameter("txtNUMERO_EXPEDIENTE"));

        institucionDependencia = DAOSanciones.obtenerEntePublico(request.getParameter("cmbDEPENDENCIA"));
        particular.setInstitucionDependencia(institucionDependencia);

        particularSancionado.setNombreRazonSocial(request.getParameter("txtNOMBRE_RAZON_SOCIAL"));
        particularSancionado.setObjetoSocial((request.getParameter("txtOBJETO_SOCIAL") == null || request.getParameter("txtOBJETO_SOCIAL").isEmpty()) ? null : request.getParameter("txtOBJETO_SOCIAL"));

        if (particularSancionado.getRfc() == null || particularSancionado.getRfc().isEmpty())
        { 
            particularSancionado.setRfc(ObtenerRfc.obntenerRfcSPDN(particularSancionado.getNombreRazonSocial()).toLowerCase());
        } 

        particularSancionado.setTipoPersona(request.getParameter("cmbTIPO_PERSONA"));

        if (particularSancionado.getTipoPersona().equalsIgnoreCase("F"))
        { 
            particularSancionado.setRfc(
                    (request.getParameter("txtRFC") != null && !request.getParameter("txtRFC").isEmpty())
                    ? (ExpresionesRegulares.validarRfcPF(request.getParameter("txtRFC")))
                    ? request.getParameter("txtRFC").toUpperCase()
                    : ""
                    : ""
            );
        } 
        else
        { 
            particularSancionado.setRfc(
                    (request.getParameter("txtRFC") != null && !request.getParameter("txtRFC").isEmpty())
                    ? (ExpresionesRegulares.validarRfcPM(request.getParameter("txtRFC")))
                    ? request.getParameter("txtRFC").toUpperCase()
                    : ""
                    : ""
            );
        } 

        if (particularSancionado.getRfc() == null || particularSancionado.getRfc().isEmpty())
        { 
            particularSancionado.setRfc(ObtenerRfc.obntenerRfcSPDN(particularSancionado.getNombreRazonSocial()).toLowerCase());
        } 

        particularSancionado.setTelefono((request.getParameter("txtTELEFONO") == null || request.getParameter("txtTELEFONO").isEmpty()) ? null : request.getParameter("txtTELEFONO"));

        domicilioMexico.setPais((request.getParameter("cmbPAIS") == null || request.getParameter("cmbPAIS").isEmpty()) ? new PAIS() : DAOSanciones.obtenerPais(request.getParameter("cmbPAIS")));

        if (request.getParameter("cmbENTIDAD_FEDERATIVA") == null || request.getParameter("cmbENTIDAD_FEDERATIVA").isEmpty())
        { 
            domicilioMexico.setEntidadFederativa(new ENTIDAD_FEDERATIVA());
        } 
        else
        { 
            domicilioMexico.setEntidadFederativa(DAOSanciones.obtenerEntidadFederativa(request.getParameter("cmbENTIDAD_FEDERATIVA")));
        } 

        if (request.getParameter("cmbMUNICIPIO") == null || request.getParameter("cmbMUNICIPIO").isEmpty())
        { 
            domicilioMexico.setMunicipio(new MUNICIPIO());
        } 
        else
        { 
            domicilioMexico.setMunicipio(DAOSanciones.obtenerMunicipio(domicilioMexico.getEntidadFederativa().getClave(), request.getParameter("cmbMUNICIPIO")));
        } 

        domicilioMexico.setCodigoPostal((request.getParameter("txtCODIGO_POSTAL") == null || request.getParameter("txtCODIGO_POSTAL").isEmpty()) ? null : request.getParameter("txtCODIGO_POSTAL"));
        domicilioMexico.setLocalidad((request.getParameter("cmbLOCALIDAD") == null || request.getParameter("cmbLOCALIDAD").isEmpty()) ? new LOCALIDAD() : DAOSanciones.obtenerLocalidad(request.getParameter("cmbLOCALIDAD")));

        vialidad.setClave((request.getParameter("cmbVIALIDAD_CLAVE") == null || request.getParameter("cmbVIALIDAD_CLAVE").isEmpty()) ? null : DAOSanciones.obtenerVialidad(request.getParameter("cmbVIALIDAD_CLAVE")).getValor());
        vialidad.setValor((request.getParameter("cmbVIALIDAD_VALOR") == null || request.getParameter("cmbVIALIDAD_VALOR").isEmpty()) ? null : request.getParameter("cmbVIALIDAD_VALOR"));
        domicilioMexico.setVialidad(vialidad);

        domicilioMexico.setNumeroExterior((request.getParameter("txtNUMERO_EXTERIOR") == null || request.getParameter("txtNUMERO_EXTERIOR").isEmpty()) ? null : request.getParameter("txtNUMERO_EXTERIOR"));
        domicilioMexico.setNumeroInterior((request.getParameter("txtNUMERO_INTERIOR") == null || request.getParameter("txtNUMERO_INTERIOR").isEmpty()) ? null : request.getParameter("txtNUMERO_INTERIOR"));
        particularSancionado.setDomicilioMexico(domicilioMexico);

        domicilioExtranjero.setCalle((request.getParameter("txtEXTRANJERO_CALLE") == null || request.getParameter("txtEXTRANJERO_CALLE").isEmpty()) ? null : request.getParameter("txtEXTRANJERO_CALLE"));
        domicilioExtranjero.setNumeroExterior((request.getParameter("txtEXTRANJERO_NUMERO_EXTERIOR") == null || request.getParameter("txtEXTRANJERO_NUMERO_EXTERIOR").isEmpty()) ? null : request.getParameter("txtEXTRANJERO_NUMERO_EXTERIOR"));
        domicilioExtranjero.setNumeroInterior((request.getParameter("txtEXTRANJERO_NUMERO_INTERIOR") == null || request.getParameter("txtEXTRANJERO_NUMERO_INTERIOR").isEmpty()) ? null : request.getParameter("txtEXTRANJERO_NUMERO_INTERIOR"));
        domicilioExtranjero.setCiudadLocalidad((request.getParameter("txtEXTRANJERO_CIUDADLOCALIDAD") == null || request.getParameter("txtEXTRANJERO_CIUDADLOCALIDAD").isEmpty()) ? null : request.getParameter("txtEXTRANJERO_CIUDADLOCALIDAD"));
        domicilioExtranjero.setEstadoProvincia((request.getParameter("txtEXTRANJERO_ESTADOPROVINCIA") == null || request.getParameter("txtEXTRANJERO_ESTADOPROVINCIA").isEmpty()) ? null : request.getParameter("txtEXTRANJERO_ESTADOPROVINCIA"));
        domicilioExtranjero.setPais((request.getParameter("cmbEXTRANJERO_PAIS") == null || request.getParameter("cmbEXTRANJERO_PAIS").isEmpty()) ? new PAIS() : DAOSanciones.obtenerPais(request.getParameter("cmbEXTRANJERO_PAIS")));
        domicilioExtranjero.setCodigoPostal((request.getParameter("txtEXTRANJERO_CODIGO_POSTAL") == null || request.getParameter("txtEXTRANJERO_CODIGO_POSTAL").isEmpty()) ? null : request.getParameter("txtEXTRANJERO_CODIGO_POSTAL"));
        particularSancionado.setDomicilioExtranjero(domicilioExtranjero);

        directorGeneral.setNombres((request.getParameter("txtDIRECTORL_NOMBRES") == null || request.getParameter("txtDIRECTORL_NOMBRES").isEmpty()) ? null : request.getParameter("txtDIRECTORL_NOMBRES"));
        directorGeneral.setPrimerApellido((request.getParameter("txtDIRECTORL_PRIMERAPELLIDO") == null || request.getParameter("txtDIRECTORL_PRIMERAPELLIDO").isEmpty()) ? null : request.getParameter("txtDIRECTORL_PRIMERAPELLIDO"));
        directorGeneral.setSegundoApellido((request.getParameter("txtDIRECTORL_SEGUNDOAPELLIDO") == null || request.getParameter("txtDIRECTORL_SEGUNDOAPELLIDO").isEmpty()) ? null : request.getParameter("txtDIRECTORL_SEGUNDOAPELLIDO"));
        directorGeneral.setCurp((request.getParameter("txtDIRECTORL_CURP") == null || request.getParameter("txtDIRECTORL_CURP").isEmpty()) ? null : request.getParameter("txtDIRECTORL_CURP"));
        particularSancionado.setDirectorGeneral(directorGeneral);

        apoderadoLegal.setNombres((request.getParameter("txtAPODERADO_NOMBRES") == null || request.getParameter("txtAPODERADO_NOMBRES").isEmpty()) ? null : request.getParameter("txtAPODERADO_NOMBRES"));
        apoderadoLegal.setPrimerApellido((request.getParameter("txtAPODERADO_PRIMERAPELLIDO") == null || request.getParameter("txtAPODERADO_PRIMERAPELLIDO").isEmpty()) ? null : request.getParameter("txtAPODERADO_PRIMERAPELLIDO"));
        apoderadoLegal.setSegundoApellido((request.getParameter("txtAPODERADO_SEGUNDOAPELLIDO") == null || request.getParameter("txtAPODERADO_SEGUNDOAPELLIDO").isEmpty()) ? null : request.getParameter("txtAPODERADO_SEGUNDOAPELLIDO"));
        apoderadoLegal.setCurp((request.getParameter("txtAPODERADO_CURP") == null || request.getParameter("txtAPODERADO_CURP").isEmpty()) ? null : request.getParameter("txtAPODERADO_CURP"));
        particularSancionado.setApoderadoLegal(apoderadoLegal);

        particular.setParticularSancionado(particularSancionado);

        particular.setObjetoContrato((request.getParameter("txtOBJETO_CONTRATO") == null || request.getParameter("txtOBJETO_CONTRATO").isEmpty()) ? null : request.getParameter("txtOBJETO_CONTRATO"));

        particular.setAutoridadSancionadora(DAOSanciones.obtenerOrganoInternoDeControl(request.getParameter("txtAUTORIDAD_SANCIONADORA")).getValor());

        particular.setTipoFalta((request.getParameter("cmbTIPO_FALTAS") == null || request.getParameter("cmbTIPO_FALTAS").isEmpty()) ? null : request.getParameter("cmbTIPO_FALTAS"));

        particular.setTipoSancion(this.obtenerTiposDeSancionParticulares(request));

        particular.setCausaMotivoHechos(request.getParameter("txtCAUSA_MOTIVO"));
        particular.setActo((request.getParameter("txtACTO") == null || request.getParameter("txtACTO").isEmpty()) ? null : request.getParameter("txtACTO"));

        responsableSancion.setNombres((request.getParameter("txtRESPONSABLESANCION_NOMBRES") == null || request.getParameter("txtRESPONSABLESANCION_NOMBRES").isEmpty()) ? null : request.getParameter("txtRESPONSABLESANCION_NOMBRES"));
        responsableSancion.setPrimerApellido((request.getParameter("txtRESPONSABLESANCION_PRIMERAPELLIDO") == null || request.getParameter("txtRESPONSABLESANCION_PRIMERAPELLIDO").isEmpty()) ? null : request.getParameter("txtRESPONSABLESANCION_PRIMERAPELLIDO"));
        responsableSancion.setSegundoApellido((request.getParameter("txtRESPONSABLESANCION_SEGUNDOAPELLIDO") == null || request.getParameter("txtRESPONSABLESANCION_SEGUNDOAPELLIDO").isEmpty()) ? null : request.getParameter("txtRESPONSABLESANCION_SEGUNDOAPELLIDO"));
        particular.setResponsableSancion(responsableSancion);

        resolucion.setSentido((request.getParameter("txtRESOLUCION_SENTIDO") == null || request.getParameter("txtRESOLUCION_SENTIDO").isEmpty()) ? null : request.getParameter("txtRESOLUCION_SENTIDO"));
        resolucion.setUrl((request.getParameter("txtRESOLUCION_URL") == null || request.getParameter("txtRESOLUCION_URL").isEmpty()) ? null : request.getParameter("txtRESOLUCION_URL"));
        resolucion.setFechaNotificacion((request.getParameter("txtRESOLUCION_FECHA_NOTIFICACION") == null || request.getParameter("txtRESOLUCION_FECHA_NOTIFICACION").isEmpty()) ? null : request.getParameter("txtRESOLUCION_FECHA_NOTIFICACION"));
        particular.setResolucion(resolucion);

        multa.setMonto((request.getParameter("txtMULTA_MONTO") == null || request.getParameter("txtMULTA_MONTO").isEmpty()) ? null : Integer.parseInt(request.getParameter("txtMULTA_MONTO")));
        multa.setMoneda((request.getParameter("cmbMONEDA") == null || request.getParameter("cmbMONEDA").isEmpty()) ? new MONEDA() : DAOSanciones.obtenerMoneda(request.getParameter("cmbMONEDA")));
        particular.setMulta(multa);

        if ((request.getParameter("txtPLAZO") == null || request.getParameter("txtPLAZO").isEmpty()) || (request.getParameter("cmbUNIDAD_MEDIDA_PLAZO") == null || request.getParameter("cmbUNIDAD_MEDIDA_PLAZO").isEmpty()))
        { 
            inhabilitacion.setPlazo(null);
        } 
        else
        { 
            medidaPlazo = DAOSanciones.obtenerUnidadMedidaPlazo(request.getParameter("cmbUNIDAD_MEDIDA_PLAZO"));
            String plazo = (request.getParameter("txtPLAZO") != null)
                    ? request.getParameter("txtPLAZO").concat(" " + medidaPlazo.getValor())
                    : null;
            inhabilitacion.setPlazo(plazo);
        } 

        if ((request.getParameter("txtFECHA_INICIAL") == null || request.getParameter("txtFECHA_INICIAL").isEmpty()) || (request.getParameter("txtFECHA_FINAL") == null || request.getParameter("txtFECHA_FINAL").isEmpty()))
        { 
            inhabilitacion.setFechaInicial(null);
            inhabilitacion.setFechaFinal(null);
        } 
        else
        { 
            dateInicial = formatoFechas.getParse((request.getParameter("txtFECHA_INICIAL") != null) ? request.getParameter("txtFECHA_INICIAL") : null);
            dateFinal = formatoFechas.getParse((request.getParameter("txtFECHA_FINAL") != null) ? request.getParameter("txtFECHA_FINAL") : null);

            if (dateInicial.compareTo(dateFinal) == 0 || dateInicial.compareTo(dateFinal) == -1)
            { 
                fechaInicial = formatoFechas.getFormat(dateInicial);
                fechaFinal = formatoFechas.getFormat(dateFinal);
                inhabilitacion.setFechaInicial(fechaInicial);
                inhabilitacion.setFechaFinal(fechaFinal);
            } 
            else
            { 
                throw new Exception("Error: La fecha inicial de inhabilitaron debe ser mayor que la fecha final!");
            } 
        } 

        particular.setInhabilitacion(inhabilitacion);

        particular.setObservaciones((request.getParameter("txtOBSERVACIONES") == null || request.getParameter("txtOBSERVACIONES").isEmpty()) ? null : request.getParameter("txtOBSERVACIONES"));

        if (request.getParameter("listaDeIdentificadoresDeDocumentosARegistrar") == null)
        {
            particular.setDocumentos((request.getParameter("txtDOCUMENTO_NUM") == null || request.getParameter("txtDOCUMENTO_NUM").isEmpty()) ? new ArrayList<>() : this.obtenerLaCantidadDeValoresCadaDocumento(request));
        }
        else
        {
            particular.setDocumentos((request.getParameter("listaDeIdentificadoresDeDocumentosARegistrar").isEmpty()) ? new ArrayList<>() : this.obtenerLaCantidadDeValoresCadaDocumento(request));
        }

        return particular;
    }

    /**
     * A&ntilde;ade un nuevo registro a la base de datos, con los datos del
     * particular sancionado
     *
     * @param particular Datos del particular sancionado
     * @param usuario Usuario en sesi&oacute;n
     * @param nivelAcceso Nivel de acceso del usario
     *
     * @return boolean
     *
     * @throws Exception
     */
    @Override
    public String registrarParticularSancionado(PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        Document mensaje;                                                       
        METADATA metadata;                                                      
        METADATOS metadatos;                                                    

        metadata = new METADATA();
        metadatos = new METADATOS();
        mensaje = new Document();

        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        { 
            metadata.setActualizacion(new ManejoDeFechas().getDateNowAsString());
            metadata.setInstitucion(usuario.getDependencia().getValor());
            metadata.setContacto(usuario.getCorreo_electronico());
            metadata.setPersonaContacto(usuario.getNombreCompleto());
            particular.setMetadata(metadata);

            particular.setId(UUID.randomUUID().toString());
            particular.setFechaCaptura(new ManejoDeFechas().getDateNowAsString());

            particular.setDependencia("SAEMM");
            metadatos.setUsuario_registro(usuario.getUsuario());
            metadatos.setFecha_registro(new ManejoDeFechas().getDateNowAsString());
            particular.setMetadatos(metadatos);
            particular.setPublicar("0");

            if (DAOSanciones.registrarParticularSancionado(particular, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha registrado correctamente al particular: " + particular.getParticularSancionado().getNombreRazonSocial());
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible registrar los datos del particular sancionado, intente de nuevo!");
            }
        } 
        else
        { 
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        } 
        return mensaje.toJson();
    }

    /**
     * Devuelve el listado de los particulares sancionados junto con los valores
     * de paginaci&oacute;n
     *
     * @param usuario Usuario en sesi&oacute;n
     * @param dependenciasDelUsuario Dependencias asignadas al usuario
     * @param filtroBusqueda filtro de b&uacute;squeda
     *
     * @return ArrayList Object
     *
     * @throws Exception
     */
    @Override
    public PsancionadosConPaginacion obtenerParticularesSancionados(USUARIO usuario, NIVEL_ACCESO nivelAcceso, ArrayList<ENTE_PUBLICO> dependenciasDelUsuario, FILTRO_BUSQUEDA_PSANCIONADOS filtroBusqueda) throws Exception
    {
        return DAOSanciones.obtenerParticularesSancionados(usuario, nivelAcceso, dependenciasDelUsuario, filtroBusqueda);
    }

    /**
     * Devuelve los valores de un particular sancionado usando como filtro el id
     * de la base de datos
     *
     * @param idParticular id del particular sancionado
     *
     * @return PSANCIONADOS
     *
     * @throws Exception
     */
    @Override
    public PSANCIONADOS obtenerDetalleParticularSancionado(String idParticular) throws Exception
    {
        return DAOSanciones.obtenerDetalleParticularSancionado(idParticular);
    }

    /**
     * Devuelve el valor de los campos p&uacute;blicos y privados
     *
     * @return PSANCIONADOS
     *
     * @throws java.lang.Exception
     */
    @Override
    public PSANCIONADOS obtenerEstatusDePrivacidadCamposPSANCIONADOS() throws Exception
    {
        return DAOSanciones.obtenerEstatusDePrivacidadCamposPSANCIONADOS("psancionados");
    }

    @Override
    public PSANCIONADOS modelarRequestParticularSancionado(HttpServletRequest request, PSANCIONADOS particular) throws Exception
    {
        INSTITUCION_DEPENDENCIA institucionDependencia;                         
        PARTICULAR_SANCIONADO particularSancionado;                             
        DOMICILIO_MEXICO domicilioMexico;                                       
        DOMICILIO_EXTRANJERO domicilioExtranjero;                               
        PERSONA directorGeneral;                                                
        PERSONA apoderadoLegal;                                                 
        RESPONSABLE_SANCION responsableSancion;                                 
        RESOLUCION_PAR resolucion;                                              
        MULTA_PSANCIONADOS multa;                                               
        INHABILITACION inhabilitacion;                                          
        Date dateInicial;                                                       
        Date dateFinal;                                                         
        ManejoDeFechas formatoFechas;                                           
        String fechaInicial;                                                    
        String fechaFinal;                                                      
        UNIDAD_MEDIDA_PLAZO medidaPlazo;                                        
        VIALIDAD vialidad;                                                      
        VIALIDAD objVialidad;                                                   

        particularSancionado = new PARTICULAR_SANCIONADO();
        domicilioMexico = new DOMICILIO_MEXICO();
        domicilioExtranjero = new DOMICILIO_EXTRANJERO();
        directorGeneral = new PERSONA();
        apoderadoLegal = new PERSONA();
        responsableSancion = new RESPONSABLE_SANCION();
        resolucion = new RESOLUCION_PAR();
        multa = new MULTA_PSANCIONADOS();
        inhabilitacion = new INHABILITACION();
        vialidad = new VIALIDAD();
        formatoFechas = new ManejoDeFechas("yyyy-MM-dd");

        particular.setExpediente((request.getParameter("txtNUMERO_EXPEDIENTE") == null || request.getParameter("txtNUMERO_EXPEDIENTE").isEmpty()) ? null : request.getParameter("txtNUMERO_EXPEDIENTE"));

        institucionDependencia = DAOSanciones.obtenerEntePublico(request.getParameter("cmbDEPENDENCIA"));
        particular.setInstitucionDependencia(institucionDependencia);

        particularSancionado.setNombreRazonSocial(request.getParameter("txtNOMBRE_RAZON_SOCIAL"));
        particularSancionado.setObjetoSocial((request.getParameter("txtOBJETO_SOCIAL") == null || request.getParameter("txtOBJETO_SOCIAL").isEmpty()) ? null : request.getParameter("txtOBJETO_SOCIAL"));

        if (particularSancionado.getRfc() == null || particularSancionado.getRfc().isEmpty())
        { 
            particularSancionado.setRfc(ObtenerRfc.obntenerRfcSPDN(particularSancionado.getNombreRazonSocial()).toLowerCase());
        } 

        particularSancionado.setTipoPersona(request.getParameter("cmbTIPO_PERSONA"));

        if (particularSancionado.getTipoPersona().equalsIgnoreCase("F"))
        { 
            particularSancionado.setRfc(
                    (request.getParameter("txtRFC") != null && !request.getParameter("txtRFC").isEmpty())
                    ? (ExpresionesRegulares.validarRfcPF(request.getParameter("txtRFC")))
                    ? request.getParameter("txtRFC").toUpperCase()
                    : ""
                    : ""
            );
        } 
        else
        { 
            particularSancionado.setRfc(
                    (request.getParameter("txtRFC") != null && !request.getParameter("txtRFC").isEmpty())
                    ? (ExpresionesRegulares.validarRfcPM(request.getParameter("txtRFC")))
                    ? request.getParameter("txtRFC").toUpperCase()
                    : ""
                    : ""
            );
        } 

        if (particularSancionado.getRfc() == null || particularSancionado.getRfc().isEmpty())
        { 
            particularSancionado.setRfc(ObtenerRfc.obntenerRfcSPDN(particularSancionado.getNombreRazonSocial()).toLowerCase());
        } 

        particularSancionado.setTelefono((request.getParameter("txtTELEFONO") == null || request.getParameter("txtTELEFONO").isEmpty()) ? null : request.getParameter("txtTELEFONO"));

        domicilioMexico.setPais((request.getParameter("cmbPAIS") == null || request.getParameter("cmbPAIS").isEmpty()) ? new PAIS() : DAOSanciones.obtenerPais(request.getParameter("cmbPAIS")));

        if (request.getParameter("cmbENTIDAD_FEDERATIVA") == null || request.getParameter("cmbENTIDAD_FEDERATIVA").isEmpty())
        { 
            domicilioMexico.setEntidadFederativa(new ENTIDAD_FEDERATIVA());
        } 
        else
        { 
            domicilioMexico.setEntidadFederativa(DAOSanciones.obtenerEntidadFederativa(request.getParameter("cmbENTIDAD_FEDERATIVA")));
        } 

        if (request.getParameter("cmbMUNICIPIO") == null || request.getParameter("cmbMUNICIPIO").isEmpty())
        { 
            domicilioMexico.setMunicipio(new MUNICIPIO());
        } 
        else
        { 
            domicilioMexico.setMunicipio(DAOSanciones.obtenerMunicipio(domicilioMexico.getEntidadFederativa().getClave(), request.getParameter("cmbMUNICIPIO")));
        } 

        domicilioMexico.setCodigoPostal((request.getParameter("txtCODIGO_POSTAL") == null || request.getParameter("txtCODIGO_POSTAL").isEmpty()) ? null : request.getParameter("txtCODIGO_POSTAL"));
        domicilioMexico.setLocalidad((request.getParameter("cmbLOCALIDAD") == null || request.getParameter("cmbLOCALIDAD").isEmpty()) ? new LOCALIDAD() : DAOSanciones.obtenerLocalidad(request.getParameter("cmbLOCALIDAD")));

        vialidad.setClave((request.getParameter("cmbVIALIDAD_CLAVE") == null || request.getParameter("cmbVIALIDAD_CLAVE").isEmpty()) ? null : DAOSanciones.obtenerVialidad(request.getParameter("cmbVIALIDAD_CLAVE")).getValor());
        vialidad.setValor((request.getParameter("cmbVIALIDAD_VALOR") == null || request.getParameter("cmbVIALIDAD_VALOR").isEmpty()) ? null : request.getParameter("cmbVIALIDAD_VALOR"));
        domicilioMexico.setVialidad(vialidad);

        domicilioMexico.setNumeroExterior((request.getParameter("txtNUMERO_EXTERIOR") == null || request.getParameter("txtNUMERO_EXTERIOR").isEmpty()) ? null : request.getParameter("txtNUMERO_EXTERIOR"));
        domicilioMexico.setNumeroInterior((request.getParameter("txtNUMERO_INTERIOR") == null || request.getParameter("txtNUMERO_INTERIOR").isEmpty()) ? null : request.getParameter("txtNUMERO_INTERIOR"));
        particularSancionado.setDomicilioMexico(domicilioMexico);

        domicilioExtranjero.setCalle((request.getParameter("txtEXTRANJERO_CALLE") == null || request.getParameter("txtEXTRANJERO_CALLE").isEmpty()) ? null : request.getParameter("txtEXTRANJERO_CALLE"));
        domicilioExtranjero.setNumeroExterior((request.getParameter("txtEXTRANJERO_NUMERO_EXTERIOR") == null || request.getParameter("txtEXTRANJERO_NUMERO_EXTERIOR").isEmpty()) ? null : request.getParameter("txtEXTRANJERO_NUMERO_EXTERIOR"));
        domicilioExtranjero.setNumeroInterior((request.getParameter("txtEXTRANJERO_NUMERO_INTERIOR") == null || request.getParameter("txtEXTRANJERO_NUMERO_INTERIOR").isEmpty()) ? null : request.getParameter("txtEXTRANJERO_NUMERO_INTERIOR"));
        domicilioExtranjero.setCiudadLocalidad((request.getParameter("txtEXTRANJERO_CIUDADLOCALIDAD") == null || request.getParameter("txtEXTRANJERO_CIUDADLOCALIDAD").isEmpty()) ? null : request.getParameter("txtEXTRANJERO_CIUDADLOCALIDAD"));
        domicilioExtranjero.setEstadoProvincia((request.getParameter("txtEXTRANJERO_ESTADOPROVINCIA") == null || request.getParameter("txtEXTRANJERO_ESTADOPROVINCIA").isEmpty()) ? null : request.getParameter("txtEXTRANJERO_ESTADOPROVINCIA"));
        domicilioExtranjero.setPais((request.getParameter("cmbEXTRANJERO_PAIS") == null || request.getParameter("cmbEXTRANJERO_PAIS").isEmpty()) ? new PAIS() : DAOSanciones.obtenerPais(request.getParameter("cmbEXTRANJERO_PAIS")));
        domicilioExtranjero.setCodigoPostal((request.getParameter("txtEXTRANJERO_CODIGO_POSTAL") == null || request.getParameter("txtEXTRANJERO_CODIGO_POSTAL").isEmpty()) ? null : request.getParameter("txtEXTRANJERO_CODIGO_POSTAL"));
        particularSancionado.setDomicilioExtranjero(domicilioExtranjero);

        directorGeneral.setNombres((request.getParameter("txtDIRECTORL_NOMBRES") == null || request.getParameter("txtDIRECTORL_NOMBRES").isEmpty()) ? null : request.getParameter("txtDIRECTORL_NOMBRES"));
        directorGeneral.setPrimerApellido((request.getParameter("txtDIRECTORL_PRIMERAPELLIDO") == null || request.getParameter("txtDIRECTORL_PRIMERAPELLIDO").isEmpty()) ? null : request.getParameter("txtDIRECTORL_PRIMERAPELLIDO"));
        directorGeneral.setSegundoApellido((request.getParameter("txtDIRECTORL_SEGUNDOAPELLIDO") == null || request.getParameter("txtDIRECTORL_SEGUNDOAPELLIDO").isEmpty()) ? null : request.getParameter("txtDIRECTORL_SEGUNDOAPELLIDO"));
        directorGeneral.setCurp((request.getParameter("txtDIRECTORL_CURP") == null || request.getParameter("txtDIRECTORL_CURP").isEmpty()) ? null : request.getParameter("txtDIRECTORL_CURP"));
        particularSancionado.setDirectorGeneral(directorGeneral);

        apoderadoLegal.setNombres((request.getParameter("txtAPODERADO_NOMBRES") == null || request.getParameter("txtAPODERADO_NOMBRES").isEmpty()) ? null : request.getParameter("txtAPODERADO_NOMBRES"));
        apoderadoLegal.setPrimerApellido((request.getParameter("txtAPODERADO_PRIMERAPELLIDO") == null || request.getParameter("txtAPODERADO_PRIMERAPELLIDO").isEmpty()) ? null : request.getParameter("txtAPODERADO_PRIMERAPELLIDO"));
        apoderadoLegal.setSegundoApellido((request.getParameter("txtAPODERADO_SEGUNDOAPELLIDO") == null || request.getParameter("txtAPODERADO_SEGUNDOAPELLIDO").isEmpty()) ? null : request.getParameter("txtAPODERADO_SEGUNDOAPELLIDO"));
        apoderadoLegal.setCurp((request.getParameter("txtAPODERADO_CURP") == null || request.getParameter("txtAPODERADO_CURP").isEmpty()) ? null : request.getParameter("txtAPODERADO_CURP"));
        particularSancionado.setApoderadoLegal(apoderadoLegal);

        particular.setParticularSancionado(particularSancionado);

        particular.setObjetoContrato((request.getParameter("txtOBJETO_CONTRATO") == null || request.getParameter("txtOBJETO_CONTRATO").isEmpty()) ? null : request.getParameter("txtOBJETO_CONTRATO"));

        particular.setAutoridadSancionadora(DAOSanciones.obtenerOrganoInternoDeControl(request.getParameter("txtAUTORIDAD_SANCIONADORA")).getValor());

        particular.setTipoFalta((request.getParameter("cmbTIPO_FALTAS") == null || request.getParameter("cmbTIPO_FALTAS").isEmpty()) ? null : request.getParameter("cmbTIPO_FALTAS"));

        particular.setTipoSancion(this.obtenerTiposDeSancionParticulares(request));

        particular.setCausaMotivoHechos(request.getParameter("txtCAUSA_MOTIVO"));
        particular.setActo((request.getParameter("txtACTO") == null || request.getParameter("txtACTO").isEmpty()) ? null : request.getParameter("txtACTO"));

        responsableSancion.setNombres((request.getParameter("txtRESPONSABLESANCION_NOMBRES") == null || request.getParameter("txtRESPONSABLESANCION_NOMBRES").isEmpty()) ? null : request.getParameter("txtRESPONSABLESANCION_NOMBRES"));
        responsableSancion.setPrimerApellido((request.getParameter("txtRESPONSABLESANCION_PRIMERAPELLIDO") == null || request.getParameter("txtRESPONSABLESANCION_PRIMERAPELLIDO").isEmpty()) ? null : request.getParameter("txtRESPONSABLESANCION_PRIMERAPELLIDO"));
        responsableSancion.setSegundoApellido((request.getParameter("txtRESPONSABLESANCION_SEGUNDOAPELLIDO") == null || request.getParameter("txtRESPONSABLESANCION_SEGUNDOAPELLIDO").isEmpty()) ? null : request.getParameter("txtRESPONSABLESANCION_SEGUNDOAPELLIDO"));
        particular.setResponsableSancion(responsableSancion);

        resolucion.setSentido((request.getParameter("txtRESOLUCION_SENTIDO") == null || request.getParameter("txtRESOLUCION_SENTIDO").isEmpty()) ? null : request.getParameter("txtRESOLUCION_SENTIDO"));
        resolucion.setUrl((request.getParameter("txtRESOLUCION_URL") == null || request.getParameter("txtRESOLUCION_URL").isEmpty()) ? null : request.getParameter("txtRESOLUCION_URL"));
        resolucion.setFechaNotificacion((request.getParameter("txtRESOLUCION_FECHA_NOTIFICACION") == null || request.getParameter("txtRESOLUCION_FECHA_NOTIFICACION").isEmpty()) ? null : request.getParameter("txtRESOLUCION_FECHA_NOTIFICACION"));
        particular.setResolucion(resolucion);

        multa.setMonto((request.getParameter("txtMULTA_MONTO") == null || request.getParameter("txtMULTA_MONTO").isEmpty()) ? null : Integer.parseInt(request.getParameter("txtMULTA_MONTO")));
        multa.setMoneda((request.getParameter("cmbMONEDA") == null || request.getParameter("cmbMONEDA").isEmpty()) ? new MONEDA() : DAOSanciones.obtenerMoneda(request.getParameter("cmbMONEDA")));
        particular.setMulta(multa);

        if ((request.getParameter("txtPLAZO") == null || request.getParameter("txtPLAZO").isEmpty()) || (request.getParameter("cmbUNIDAD_MEDIDA_PLAZO") == null || request.getParameter("cmbUNIDAD_MEDIDA_PLAZO").isEmpty()))
        { 
            inhabilitacion.setPlazo(null);
        } 
        else
        { 
            medidaPlazo = DAOSanciones.obtenerUnidadMedidaPlazo(request.getParameter("cmbUNIDAD_MEDIDA_PLAZO"));
            String plazo = (request.getParameter("txtPLAZO") != null)
                    ? request.getParameter("txtPLAZO").concat(" " + medidaPlazo.getValor())
                    : null;
            inhabilitacion.setPlazo(plazo);
        } 

        if ((request.getParameter("txtFECHA_INICIAL") == null || request.getParameter("txtFECHA_INICIAL").isEmpty()) || (request.getParameter("txtFECHA_FINAL") == null || request.getParameter("txtFECHA_FINAL").isEmpty()))
        { 
            inhabilitacion.setFechaInicial(null);
            inhabilitacion.setFechaFinal(null);
        } 
        else
        { 
            dateInicial = formatoFechas.getParse((request.getParameter("txtFECHA_INICIAL") != null) ? request.getParameter("txtFECHA_INICIAL") : null);
            dateFinal = formatoFechas.getParse((request.getParameter("txtFECHA_FINAL") != null) ? request.getParameter("txtFECHA_FINAL") : null);

            if (dateInicial.compareTo(dateFinal) == 0 || dateInicial.compareTo(dateFinal) == -1)
            { 
                fechaInicial = formatoFechas.getFormat(dateInicial);
                fechaFinal = formatoFechas.getFormat(dateFinal);
                inhabilitacion.setFechaInicial(fechaInicial);
                inhabilitacion.setFechaFinal(fechaFinal);
            } 
            else
            { 
                throw new Exception("Error: La fecha inicial de inhabilitaron debe ser mayor que la fecha final!");
            } 
        } 

        particular.setInhabilitacion(inhabilitacion);

        particular.setObservaciones((request.getParameter("txtOBSERVACIONES") == null || request.getParameter("txtOBSERVACIONES").isEmpty()) ? null : request.getParameter("txtOBSERVACIONES"));

        if (request.getParameter("listaDeIdentificadoresDeDocumentosARegistrar") == null)
        {
            particular.setDocumentos((request.getParameter("txtDOCUMENTO_NUM") == null || request.getParameter("txtDOCUMENTO_NUM").isEmpty()) ? new ArrayList<>() : this.obtenerLaCantidadDeValoresCadaDocumento(request));
        }
        else
        {
            particular.setDocumentos((request.getParameter("listaDeIdentificadoresDeDocumentosARegistrar").isEmpty()) ? new ArrayList<>() : this.obtenerLaCantidadDeValoresCadaDocumento(request));
        }
        return particular;
    }

    /**
     * Registra los cambios realizados a un particular usando como filtro el
     * nivel del usuario y el id del registro
     *
     * @param id Id del registro que se va a editar
     * @param particular Objeto que contiene los valores del particular
     * @param usuario Usuario en sesi&oacute;n
     * @param nivelAcceso Nivel que es otorgado a cada usuario
     *
     * @return boolean
     *
     * @throws java.lang.Exception
     */
    @Override
    public String editarParticularSancionado(String id, PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        Document mensaje;                                                       

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        { 

            if (particular.getMetadatos() != null)
            { 
                particular.getMetadatos().setUsuario_modificacion(usuario.getUsuario());
                particular.getMetadatos().setFecha_modificacion(new ManejoDeFechas().getDateNowAsString());
            } 
            else
            { 
                METADATOS metadatos = new METADATOS();
                metadatos.setUsuario_modificacion(usuario.getUsuario());
                metadatos.setFecha_modificacion(new ManejoDeFechas().getDateNowAsString());
                particular.setMetadatos(metadatos);
            } 

            if (particular.getMetadata() != null)
            { 
                particular.getMetadata().setActualizacion(new ManejoDeFechas().getDateNowAsString());
                particular.getMetadata().setInstitucion(usuario.getDependencia().getValor());
                particular.getMetadata().setContacto(usuario.getCorreo_electronico());
                particular.getMetadata().setPersonaContacto(usuario.getNombreCompleto());
            } 
            else
            { 
                METADATA metadata = new METADATA();
                metadata.setActualizacion(new ManejoDeFechas().getDateNowAsString());
                metadata.setInstitucion(usuario.getDependencia().getValor());
                metadata.setContacto(usuario.getCorreo_electronico());
                metadata.setPersonaContacto(usuario.getNombreCompleto());
                particular.setMetadata(metadata);
            } 

            particular.setPublicar("0");

            if (DAOSanciones.editarParticularSancionado(id, particular, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha modificado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible modificar el registro!");
            }
        } 
        else
        { 
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\\u00F3n!");
        } 

        return mensaje.toJson();
    }

    /**
     * Cambia el estatus del valor publicar a 1 para el particular sancionado
     * usando como filtro el id del registro
     *
     * @param id identificador de particular sancionado
     * @param particular Datos del particular sancionado
     * @param usuario Usuario en sesi&oacute;n
     * @param nivelAcceso Acceso del usuario al sistema
     *
     * @return boolean
     *
     * @throws Exception
     */
    @Override
    public String publicarParticularSancionado(String id, PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        Document mensaje;                                                       

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        { 

            if (particular.getMetadatos() != null)
            { 
                particular.getMetadatos().setUsuario_publicacion(usuario.getUsuario());
                particular.getMetadatos().setFecha_publicacion(new ManejoDeFechas().getDateNowAsString());
            } 
            else
            { 
                METADATOS metadatos = new METADATOS();
                metadatos.setUsuario_publicacion(usuario.getUsuario());
                metadatos.setFecha_publicacion(new ManejoDeFechas().getDateNowAsString());
                particular.setMetadatos(metadatos);
            } 
            particular.setPublicar("1");

            if (DAOSanciones.publicarParticularSancionado(id, particular, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha publicado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible publicar el registro!");
            }
        } 
        else
        { 
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        } 

        return mensaje.toJson();
    }

    /**
     * Cambia el estatus del valor publicar a 0 para el particular sancionado
     * usando como filtro el id del registro
     *
     * @param id identificador de particular sancionado
     * @param particular Datos del particular sancionado
     * @param usuario Usuario en sesi&oacute;n
     * @param nivelAcceso Acceso del usuario al sistema
     *
     * @return boolean
     *
     * @throws Exception
     */
    @Override
    public String desPublicarParticularSancionado(String id, PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        Document mensaje;                                                       

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        { 

            if (particular.getMetadatos() != null)
            { 
                particular.getMetadatos().setUsuario_despublicacion(usuario.getUsuario());
                particular.getMetadatos().setFecha_despublicacion(new ManejoDeFechas().getDateNowAsString());
            } 
            else
            { 
                METADATOS metadatos = new METADATOS();
                metadatos.setUsuario_despublicacion(usuario.getUsuario());
                metadatos.setFecha_despublicacion(new ManejoDeFechas().getDateNowAsString());
                particular.setMetadatos(metadatos);
            } 
            particular.setPublicar("0");

            if (DAOSanciones.desPublicarParticularSancionado(id, particular, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha despublicado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible despublicar el registro!");
            }
        } 
        else
        { 
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        } 

        return mensaje.toJson();
    }

    /**
     * Borra de la base de datos el registro del particular sancionado usando
     * como filtro el id del registro
     *
     * @param id identificador de particular sancionado
     * @param particular Datos del particular sancionado
     * @param usuario Usuario en sesi&oacute;n
     * @param nivelAcceso Acceso del usuario al sistema
     *
     * @return boolean
     *
     * @throws Exception
     */
    @Override
    public String eliminarParticularSancionado(String id, PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        Document mensaje;                                                       

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        { 
            if (DAOSanciones.eliminarParticularSancionado(id, particular, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha eliminado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible eliminar el registro!");
            }
        } 
        else
        { 
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        } 

        return mensaje.toJson();
    }

    /**
     * Devuelve el tipo de persona que es particular sancionado
     *
     * @param tipoPersona clave del tipo de persona
     *
     * @return TIPO_PERSONA
     *
     * @throws Exception
     */
    @Override
    public TIPO_PERSONA obtenerTipoPersona(String tipoPersona) throws Exception
    {
        return DAOSanciones.obtenerTipoPersona(tipoPersona);
    }

    /**
     * Devuelve el listado de los tipos de personas registrados en la base de
     * datos
     *
     * @return ArrayList TIPO_PERSONA
     *
     * @throws Exception
     */
    @Override
    public ArrayList<TIPO_PERSONA> obtenerListaTipoPersona() throws Exception
    {
        return DAOSanciones.obtenerListaTipoPersona();
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws java.lang.Exception
     */
    @Override
    public ArrayList<MUNICIPIO_PDE> obtenerListaDeMunicipiosporEstado(String parameter) throws Exception
    {
        return DAOSanciones.obtenerListaDeMunicipiosporEstado(parameter);
    }

    /**
     *
     * @return
     */
    @Override
    public ArrayList<PAIS> obtenerListaDePaises() throws Exception
    {
        return DAOSanciones.obtenerListaDePaises();
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ASENTAMIENTO> obtenerAsentamiento(String parameter) throws Exception
    {
        return DAOSanciones.obtenerAsentamiento(parameter);
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ASENTAMIENTO> obtenerAsentamientoPorCP(String parameter) throws Exception
    {
        return DAOSanciones.obtenerAsentamientoPorCP(parameter);
    }

    /**
     *
     * @param parameter
     *
     * @return
     */
    @Override
    public ArrayList<ENTIDAD_FEDERATIVA_PDE> obtenerListaDeEntidadesFederativa(String parameter) throws Exception
    {
        return DAOSanciones.obtenerListaDeEntidadesFederativas(parameter);
    }

    /**
     *
     * @param parameter
     *
     * @return
     */
    @Override
    public ArrayList<VIALIDAD> obtenerListaDeVialidades(String parameter) throws Exception
    {
        return DAOSanciones.obtenerTiposDeVialidades(parameter);
    }

    /**
     *
     * @return
     */
    @Override
    public ArrayList<TIPO_DOCUMENTO> obtenerTiposDeDocumentos() throws Exception
    {
        return DAOSanciones.obtenerTiposDeDocumentos();
    }

    /**
     *
     * @return @throws Exception
     */
    @Override
    public ArrayList<TIPO_FALTA> obtenerListaTiposDeFaltaPsancionados() throws Exception
    {
        return DAOSanciones.obtenerListaTiposDeFaltaPsancionados();
    }

    /**
     *
     * @return @throws Exception
     */
    @Override
    public ArrayList<TIPO_SANCION_PAR> obtenerListaTiposDeSancionPsancionados() throws Exception
    {
        return DAOSanciones.obtenerListaTiposDeSancionPsancionados();
    }

}

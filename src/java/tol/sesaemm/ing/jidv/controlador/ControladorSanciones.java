/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.controlador;

import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocioS3;
import tol.sesaemm.ing.jidv.javabeans.Miga;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.PAGINACION;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.FILTRO_BUSQUEDA_PSANCIONADOS;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.ParticularSancionado;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.PsancionadosConPaginacion;
import tol.sesaemm.ing.jidv.logicaDeNegocio.LogicaDeNegocioS3V01;
import tol.sesaemm.ing.jidv.logicaDeNegocio.LogicaDeNegocioV01;
import tol.sesaemm.javabeans.ASENTAMIENTO;
import tol.sesaemm.javabeans.ENTE_PUBLICO;
import tol.sesaemm.javabeans.ENTIDAD_FEDERATIVA_PDE;
import tol.sesaemm.javabeans.GENERO;
import tol.sesaemm.javabeans.MONEDA;
import tol.sesaemm.javabeans.MUNICIPIO_PDE;
import tol.sesaemm.javabeans.ORGANO_INTERNO_CONTROL;
import tol.sesaemm.javabeans.PAIS;
import tol.sesaemm.javabeans.PUESTO;
import tol.sesaemm.javabeans.TIPO_DOCUMENTO;
import tol.sesaemm.javabeans.TIPO_PERSONA;
import tol.sesaemm.javabeans.UNIDAD_MEDIDA_PLAZO;
import tol.sesaemm.javabeans.s3psancionados.PSANCIONADOS;
import tol.sesaemm.javabeans.s3psancionados.TIPO_FALTA;
import tol.sesaemm.javabeans.s3psancionados.TIPO_SANCION_PAR;
import tol.sesaemm.javabeans.s3psancionados.VIALIDAD;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
 
public class ControladorSanciones extends tol.sesaemm.ing.jidv.controlador.ControladorBase
{

    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException
    {
        String strAccion = "";
        LogicaDeNegocio LN = new LogicaDeNegocioV01();
        LogicaDeNegocioS3 LN3 = new LogicaDeNegocioS3V01();
        HttpSession sesion = request.getSession();
        USUARIO usuario;
        NIVEL_ACCESO nivelAcceso;

        usuario = (USUARIO) sesion.getAttribute("usuario");
        strAccion = request.getParameter("accion");

        try
        {
            if (strAccion == null)
            {
                if (sesion != null)
                {
                    sesion.invalidate();
                    sesion = null;
                }

                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
            else if (strAccion.equals("paginaPrincipal"))
            {
                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
            else if (strAccion.equals("loguearUsuario"))
            {

                usuario = new USUARIO();
                usuario.setUsuario(request.getParameter("txtUsuario"));
                usuario.setPassword(request.getParameter("txtPassword"));

                usuario = LN.loguearUsuario(usuario);

                if (usuario != null)
                {

                    sesion = request.getSession(true);
                    sesion.setAttribute("usuario", usuario);
                    request.setAttribute("sistemas", LN.obtenerDatosActualizadosDeSistemas(usuario.getSistemas()));
                    this.fordward(request, resp, "/frmMenuSistemas.jsp");

                }
                else
                {
                    if (sesion != null)
                    {
                        sesion.invalidate();
                        sesion = null;
                    }

                    request.setAttribute("mensaje", "Usuario o contraseña incorrectos");
                    this.fordward(request, resp, "/frmPrincipal.jsp");
                }

            }
            else if (strAccion.equals("cerrarSesion"))
            {
                if (sesion != null)
                {
                    sesion.invalidate();
                    sesion = null;
                }

                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
            else if (usuario != null)
            {
                nivelAcceso = LN.tipoDeUsuario(usuario, 3);
                if (strAccion.equals("ingresarSistemaParticulares"))
                {
                    ArrayList<Miga> migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Particulares sancionados", "", "sanciones"));

                    request.setAttribute("migas", migas);
                    request.setAttribute("paginacion", new PAGINACION());
                    request.setAttribute("esAdmin", LN.esAdmin(usuario, 2));
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    this.fordward(request, resp, "s3Sancionados/frmPaginaPrincipalParticularesSancionados.jsp");
                }

                else if (strAccion.equals("consultarParticularesSancionados"))
                {
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    ArrayList<ParticularSancionado> particularesSancionados;
                    ArrayList<Miga> migas = new ArrayList<>();
                    PAGINACION paginacion;
                    FILTRO_BUSQUEDA_PSANCIONADOS filtroBusqueda;
                    PsancionadosConPaginacion listaDeObjetos;

                    paginacion = new PAGINACION();
                    particularesSancionados = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Particulares sancionados", "", "sanciones"));

                    filtroBusqueda = LN3.modelarFiltroParticularSancionado(request);
                    dependenciasDelUsuario = LN3.obtenerDependenciasDelUsuario(usuario);
                    listaDeObjetos = LN3.obtenerParticularesSancionados(usuario, nivelAcceso, dependenciasDelUsuario, filtroBusqueda);
                    if (listaDeObjetos != null)
                    {
                        paginacion = listaDeObjetos.getPaginacion();
                        particularesSancionados = listaDeObjetos.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("particularesSancionados", particularesSancionados);
                    this.fordward(request, resp, "s3Sancionados/frmPaginaPrincipalParticularesSancionados.jsp");
                }

                else if (strAccion.equals("detalleParticularSancionado"))
                {
                    PSANCIONADOS privacidad;
                    PSANCIONADOS particular;
                    String idParticular;
                    ArrayList<Miga> migas;
                    FILTRO_BUSQUEDA_PSANCIONADOS filtroBusqueda;
                    TIPO_PERSONA tipoPersona;

                    filtroBusqueda = LN3.modelarFiltroParticularSancionado(request);
                    idParticular = request.getParameter("txtId");
                    particular = LN3.obtenerDetalleParticularSancionado(idParticular);
                    privacidad = LN3.obtenerEstatusDePrivacidadCamposPSANCIONADOS();
                    tipoPersona = LN3.obtenerTipoPersona(particular.getParticularSancionado().getTipoPersona());

                    migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Particulares sancionados", "consultarParticularesSancionados", "sanciones"));
                    migas.add(new Miga("Datos del servidor p&uacute;blico", "", "contrataciones"));

                    request.setAttribute("migas", migas);
                    request.setAttribute("privacidadDatos", privacidad);
                    request.setAttribute("particular", particular);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("tipoPersona", tipoPersona);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    this.fordward(request, resp, "s3Sancionados/frmDetalleParticularSancionado.jsp");
                }

                else if (strAccion.equals("mostrarPaginaRegistroNuevoParticularSancionado"))
                {
                    ArrayList<Miga> migas = new ArrayList<>();
                    ArrayList<ENTE_PUBLICO> entesPublicos;
                    ArrayList<PUESTO> puestos;
                    ArrayList<GENERO> generos;
                    ArrayList<TIPO_FALTA> tipoFaltas;
                    ArrayList<TIPO_SANCION_PAR> tiposSancion;
                    ArrayList<MONEDA> tipoMoneda;
                    ArrayList<UNIDAD_MEDIDA_PLAZO> unidadesMedidasPlazos;
                    ArrayList<TIPO_PERSONA> tiposDePersonas;
                    ArrayList<ENTIDAD_FEDERATIVA_PDE> entidadesFederativas;
                    ArrayList<MUNICIPIO_PDE> municipios;
                    ArrayList<ASENTAMIENTO> localidades;
                    ArrayList<VIALIDAD> vialidades;
                    ArrayList<PAIS> paises;
                    ArrayList<ORGANO_INTERNO_CONTROL> organosInternos;
                    ArrayList<TIPO_DOCUMENTO> tiposDeDocumento;

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Particulares sancionados", "consultarParticularesSancionados", "sanciones"));
                    migas.add(new Miga("Registro de particular", "", "sanciones"));

                    puestos = LN3.obtenerListaDePuestos();
                    generos = LN3.obtenerListaDeGeneros();
                    entesPublicos = LN3.obtenerEntesPublicos(usuario);
                    tipoFaltas = LN3.obtenerListaTiposDeFaltaPsancionados();
                    tiposSancion = LN3.obtenerListaTiposDeSancionPsancionados();
                    tipoMoneda = LN3.obtenerListaDeTiposDeMoneda();
                    unidadesMedidasPlazos = LN3.obtenerListaDeUnidadMedidaPlazo();
                    tiposDePersonas = LN3.obtenerListaTipoPersona();
                    String parameter = null;
                    entidadesFederativas = LN3.obtenerListaDeEntidadesFederativa(parameter);
                    municipios = LN3.obtenerListaDeMunicipiosporEstado(parameter);
                    localidades = LN3.obtenerAsentamiento(parameter);
                    vialidades = LN3.obtenerListaDeVialidades(parameter);
                    paises = LN3.obtenerListaDePaises();
                    organosInternos = LN3.obtenerListaDeOrganoInternoDeControl();
                    tiposDeDocumento = LN3.obtenerTiposDeDocumentos();

                    request.setAttribute("entesPublicos", entesPublicos);
                    request.setAttribute("puestos", puestos);
                    request.setAttribute("migas", migas);
                    request.setAttribute("generos", generos);
                    request.setAttribute("entesPublicos", entesPublicos);
                    request.setAttribute("tipoFaltas", tipoFaltas);
                    request.setAttribute("tiposSancion", tiposSancion);
                    request.setAttribute("tipoMoneda", tipoMoneda);
                    request.setAttribute("unidadesMedidasPlazos", unidadesMedidasPlazos);
                    request.setAttribute("tiposDePersonas", tiposDePersonas);
                    request.setAttribute("entidadesFederativas", entidadesFederativas);
                    request.setAttribute("municipios", municipios);
                    request.setAttribute("localidades", localidades);
                    request.setAttribute("vialidades", vialidades);
                    request.setAttribute("paises", paises);
                    request.setAttribute("organosInternos", organosInternos);
                    request.setAttribute("tiposDeDocumento", tiposDeDocumento);
                    this.fordward(request, resp, "s3Sancionados/frmRegistraParticularSancionado.jsp");
                }

                else if (strAccion.equals("mostrarVistaPreviaParticularSancionado"))
                {
                    PSANCIONADOS particularsSancionado;
                    ArrayList<Miga> migas;
                    PSANCIONADOS privacidad;
                    TIPO_PERSONA tipoPersona;
                    String[] arregloDePlazo;

                    migas = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Particulares sancionados", "consultarParticularesSancionados", "sanciones"));
                    migas.add(new Miga("Registro de particular", "", "sanciones"));

                    particularsSancionado = LN3.modelarRequestParticularSancionado(request);
                    privacidad = LN3.obtenerEstatusDePrivacidadCamposPSANCIONADOS();
                    tipoPersona = LN3.obtenerTipoPersona(particularsSancionado.getParticularSancionado().getTipoPersona());
                    arregloDePlazo = (particularsSancionado.getInhabilitacion().getPlazo() == null || particularsSancionado.getInhabilitacion().getPlazo().isEmpty()) ? new String[2] : particularsSancionado.getInhabilitacion().getPlazo().split(" ");

                    request.setAttribute("privacidadDatos", privacidad);
                    request.setAttribute("tipoPersona", tipoPersona);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("migas", migas);
                    request.setAttribute("particular", particularsSancionado);
                    request.setAttribute("arregloDePlazo", arregloDePlazo);
                    this.fordward(request, resp, "s3Sancionados/frmVistaPreviaParticularSancionado.jsp");
                }

                else if (strAccion.equals("modificarDatosParticularSancionado"))
                {
                    PSANCIONADOS particularsSancionado;
                    ArrayList<Miga> migas = new ArrayList<>();
                    ArrayList<ENTE_PUBLICO> entesPublicos;
                    ArrayList<PUESTO> puestos;
                    ArrayList<GENERO> generos;
                    ArrayList<TIPO_FALTA> tipoFaltas;
                    ArrayList<TIPO_SANCION_PAR> tiposSancion;
                    ArrayList<MONEDA> tipoMoneda;
                    ArrayList<UNIDAD_MEDIDA_PLAZO> unidadesMedidasPlazos;
                    ArrayList<TIPO_PERSONA> tiposDePersonas;
                    ArrayList<ENTIDAD_FEDERATIVA_PDE> entidadesFederativas;
                    ArrayList<MUNICIPIO_PDE> municipios;
                    ArrayList<ASENTAMIENTO> localidades;
                    ArrayList<VIALIDAD> vialidades;
                    ArrayList<PAIS> paises;
                    ArrayList<ORGANO_INTERNO_CONTROL> organosInternos;
                    ArrayList<TIPO_DOCUMENTO> tiposDeDocumento;
                    String[] arregloDePlazo;

                    migas = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Particulares sancionados", "consultarParticularesSancionados", "sanciones"));
                    migas.add(new Miga("Registro de particular", "", "sanciones"));

                    particularsSancionado = LN3.modelarRequestParticularSancionado(request);
                    puestos = LN3.obtenerListaDePuestos();
                    generos = LN3.obtenerListaDeGeneros();
                    entesPublicos = LN3.obtenerEntesPublicos(usuario);
                    tipoFaltas = LN3.obtenerListaTiposDeFaltaPsancionados();
                    tiposSancion = LN3.obtenerListaTiposDeSancionPsancionados();
                    tipoMoneda = LN3.obtenerListaDeTiposDeMoneda();
                    unidadesMedidasPlazos = LN3.obtenerListaDeUnidadMedidaPlazo();
                    tiposDePersonas = LN3.obtenerListaTipoPersona();
                    String parameter = null;
                    entidadesFederativas = LN3.obtenerListaDeEntidadesFederativa(parameter);
                    municipios = LN3.obtenerListaDeMunicipiosporEstado(parameter);
                    localidades = LN3.obtenerAsentamiento(parameter);
                    vialidades = LN3.obtenerListaDeVialidades(parameter);
                    paises = LN3.obtenerListaDePaises();
                    organosInternos = LN3.obtenerListaDeOrganoInternoDeControl();
                    tiposDeDocumento = LN3.obtenerTiposDeDocumentos();
                    arregloDePlazo = (particularsSancionado.getInhabilitacion().getPlazo() == null) ? new String[2] : particularsSancionado.getInhabilitacion().getPlazo().split(" ");

                    request.setAttribute("particular", particularsSancionado);
                    request.setAttribute("entesPublicos", entesPublicos);
                    request.setAttribute("puestos", puestos);
                    request.setAttribute("migas", migas);
                    request.setAttribute("generos", generos);
                    request.setAttribute("entesPublicos", entesPublicos);
                    request.setAttribute("tipoFaltas", tipoFaltas);
                    request.setAttribute("tiposSancion", tiposSancion);
                    request.setAttribute("tipoMoneda", tipoMoneda);
                    request.setAttribute("unidadesMedidasPlazos", unidadesMedidasPlazos);
                    request.setAttribute("tiposDePersonas", tiposDePersonas);
                    request.setAttribute("entidadesFederativas", entidadesFederativas);
                    request.setAttribute("municipios", municipios);
                    request.setAttribute("localidades", localidades);
                    request.setAttribute("vialidades", vialidades);
                    request.setAttribute("paises", paises);
                    request.setAttribute("organosInternos", organosInternos);
                    request.setAttribute("tiposDeDocumento", tiposDeDocumento);
                    request.setAttribute("arregloDePlazo", arregloDePlazo);
                    this.fordward(request, resp, "s3Sancionados/frmRegistraParticularSancionado.jsp");
                }

                else if (strAccion.equals("registrarParticularSancionado"))
                {
                    PSANCIONADOS particular;
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    ArrayList<ParticularSancionado> particularesSancionados;
                    ArrayList<Miga> migas = new ArrayList<>();
                    PAGINACION paginacion;
                    FILTRO_BUSQUEDA_PSANCIONADOS filtroBusqueda;
                    PsancionadosConPaginacion listaDeObjetos;
                    String mensaje;

                    paginacion = new PAGINACION();
                    particularesSancionados = new ArrayList<>();

                    filtroBusqueda = LN3.modelarFiltroParticularSancionado(request);

                    particular = LN3.modelarRequestParticularSancionado(request);
                    mensaje = LN3.registrarParticularSancionado(particular, usuario, nivelAcceso);

                    dependenciasDelUsuario = LN3.obtenerDependenciasDelUsuario(usuario);
                    listaDeObjetos = LN3.obtenerParticularesSancionados(usuario, nivelAcceso, dependenciasDelUsuario, filtroBusqueda);
                    if (listaDeObjetos != null)
                    {
                        paginacion = listaDeObjetos.getPaginacion();
                        particularesSancionados = listaDeObjetos.getServidores();
                    }

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Particulares sancionados", "", "sanciones"));

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("correcto", true);
                    request.setAttribute("mensaje", mensaje);
                    request.setAttribute("particularesSancionados", particularesSancionados);
                    this.fordward(request, resp, "s3Sancionados/frmPaginaPrincipalParticularesSancionados.jsp");
                }

                else if (strAccion.equals("mostrarPaginaEditarParticularSancionado"))
                {
                    PSANCIONADOS particularsSancionado;
                    ArrayList<Miga> migas;
                    String idParticular;
                    FILTRO_BUSQUEDA_PSANCIONADOS filtroBusqueda;
                    ArrayList<ENTE_PUBLICO> entesPublicos;
                    ArrayList<PUESTO> puestos;
                    ArrayList<GENERO> generos;
                    ArrayList<TIPO_FALTA> tipoFaltas;
                    ArrayList<TIPO_SANCION_PAR> tiposSancion;
                    ArrayList<MONEDA> tipoMoneda;
                    ArrayList<UNIDAD_MEDIDA_PLAZO> unidadesMedidasPlazos;
                    ArrayList<TIPO_PERSONA> tiposDePersonas;
                    ArrayList<ENTIDAD_FEDERATIVA_PDE> entidadesFederativas;
                    ArrayList<MUNICIPIO_PDE> municipios;
                    ArrayList<ASENTAMIENTO> localidades;
                    ArrayList<VIALIDAD> vialidades;
                    ArrayList<PAIS> paises;
                    ArrayList<ORGANO_INTERNO_CONTROL> organosInternos;
                    ArrayList<TIPO_DOCUMENTO> tiposDeDocumento;
                    String[] arregloDePlazo;

                    migas = new ArrayList<>();

                    filtroBusqueda = LN3.modelarFiltroParticularSancionado(request);
                    idParticular = request.getParameter("txtId");
                    particularsSancionado = LN3.obtenerDetalleParticularSancionado(idParticular);
                    puestos = LN3.obtenerListaDePuestos();
                    generos = LN3.obtenerListaDeGeneros();
                    entesPublicos = LN3.obtenerEntesPublicos(usuario);
                    tipoFaltas = LN3.obtenerListaTiposDeFaltaPsancionados();
                    tiposSancion = LN3.obtenerListaTiposDeSancionPsancionados();
                    tipoMoneda = LN3.obtenerListaDeTiposDeMoneda();
                    unidadesMedidasPlazos = LN3.obtenerListaDeUnidadMedidaPlazo();
                    tiposDePersonas = LN3.obtenerListaTipoPersona();
                    String parameter = null;
                    entidadesFederativas = LN3.obtenerListaDeEntidadesFederativa(parameter);
                    municipios = LN3.obtenerListaDeMunicipiosporEstado(parameter);
                    localidades = LN3.obtenerAsentamiento(parameter);
                    vialidades = LN3.obtenerListaDeVialidades(parameter);
                    paises = LN3.obtenerListaDePaises();
                    organosInternos = LN3.obtenerListaDeOrganoInternoDeControl();
                    tiposDeDocumento = LN3.obtenerTiposDeDocumentos();
                    arregloDePlazo = (particularsSancionado.getInhabilitacion().getPlazo() == null) ? new String[2] : particularsSancionado.getInhabilitacion().getPlazo().split(" ");

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Particulares sancionados", "consultarParticularesSancionados", "sanciones"));
                    migas.add(new Miga("Edici&oacute;n de particular", "", ""));

                    request.setAttribute("particular", particularsSancionado);
                    request.setAttribute("entesPublicos", entesPublicos);
                    request.setAttribute("puestos", puestos);
                    request.setAttribute("migas", migas);
                    request.setAttribute("generos", generos);
                    request.setAttribute("entesPublicos", entesPublicos);
                    request.setAttribute("tipoFaltas", tipoFaltas);
                    request.setAttribute("tiposSancion", tiposSancion);
                    request.setAttribute("tipoMoneda", tipoMoneda);
                    request.setAttribute("unidadesMedidasPlazos", unidadesMedidasPlazos);
                    request.setAttribute("tiposDePersonas", tiposDePersonas);
                    request.setAttribute("entidadesFederativas", entidadesFederativas);
                    request.setAttribute("municipios", municipios);
                    request.setAttribute("localidades", localidades);
                    request.setAttribute("vialidades", vialidades);
                    request.setAttribute("paises", paises);
                    request.setAttribute("organosInternos", organosInternos);
                    request.setAttribute("tiposDeDocumento", tiposDeDocumento);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("arregloDePlazo", arregloDePlazo);
                    this.fordward(request, resp, "s3Sancionados/frmEditarParticularSancionado.jsp");
                }

                else if (strAccion.equals("editarDatosParticularSancionado"))
                {
                    PSANCIONADOS particular;
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    ArrayList<ParticularSancionado> particularesSancionados;
                    ArrayList<Miga> migas = new ArrayList<>();
                    PAGINACION paginacion;
                    FILTRO_BUSQUEDA_PSANCIONADOS filtroBusqueda;
                    PsancionadosConPaginacion listaDeObjetos;
                    String mensaje;

                    paginacion = new PAGINACION();
                    particularesSancionados = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Particulares sancionados", "", "sanciones"));

                    filtroBusqueda = LN3.modelarFiltroParticularSancionado(request);

                    particular = LN3.obtenerDetalleParticularSancionado(filtroBusqueda.getId());
                    particular = LN3.modelarRequestParticularSancionado(request, particular);

                    mensaje = LN3.editarParticularSancionado(filtroBusqueda.getId(), particular, usuario, nivelAcceso);

                    dependenciasDelUsuario = LN3.obtenerDependenciasDelUsuario(usuario);
                    listaDeObjetos = LN3.obtenerParticularesSancionados(usuario, nivelAcceso, dependenciasDelUsuario, filtroBusqueda);
                    if (listaDeObjetos != null)
                    {
                        paginacion = listaDeObjetos.getPaginacion();
                        particularesSancionados = listaDeObjetos.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("correcto", true);
                    request.setAttribute("mensaje", mensaje);
                    request.setAttribute("particularesSancionados", particularesSancionados);
                    this.fordward(request, resp, "s3Sancionados/frmPaginaPrincipalParticularesSancionados.jsp");
                }

                else if (strAccion.equals("publicarParticularSancionado"))
                {
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    PSANCIONADOS Particulars;
                    ArrayList<ParticularSancionado> particularesSancionados;
                    ArrayList<Miga> migas = new ArrayList<>();
                    String id;
                    FILTRO_BUSQUEDA_PSANCIONADOS filtroBusqueda;
                    String mensaje;
                    PAGINACION paginacion;
                    PsancionadosConPaginacion listaDeObjetos;

                    paginacion = new PAGINACION();
                    particularesSancionados = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Particulares sancionados", "", "sanciones"));

                    id = request.getParameter("txtId");
                    filtroBusqueda = LN3.modelarFiltroParticularSancionado(request);
                    Particulars = LN3.obtenerDetalleParticularSancionado(id);

                    mensaje = LN3.publicarParticularSancionado(id, Particulars, usuario, nivelAcceso);

                    dependenciasDelUsuario = LN3.obtenerDependenciasDelUsuario(usuario);
                    listaDeObjetos = LN3.obtenerParticularesSancionados(usuario, nivelAcceso, dependenciasDelUsuario, filtroBusqueda);
                    if (listaDeObjetos != null)
                    {
                        paginacion = listaDeObjetos.getPaginacion();
                        particularesSancionados = listaDeObjetos.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("particularesSancionados", particularesSancionados);
                    request.setAttribute("correcto", true);
                    request.setAttribute("mensaje", mensaje);
                    this.fordward(request, resp, "s3Sancionados/frmPaginaPrincipalParticularesSancionados.jsp");
                }

                else if (strAccion.equals("despublicarParticularSancionado"))
                {
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    PSANCIONADOS Particulars;
                    ArrayList<ParticularSancionado> particularesSancionados;
                    ArrayList<Miga> migas = new ArrayList<>();
                    String id;
                    FILTRO_BUSQUEDA_PSANCIONADOS filtroBusqueda;
                    String mensaje;
                    PAGINACION paginacion;
                    PsancionadosConPaginacion listaDeObjetos;

                    paginacion = new PAGINACION();
                    particularesSancionados = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Particulares sancionados", "", "sanciones"));

                    id = request.getParameter("txtId");
                    filtroBusqueda = LN3.modelarFiltroParticularSancionado(request);
                    Particulars = LN3.obtenerDetalleParticularSancionado(id);

                    mensaje = LN3.desPublicarParticularSancionado(id, Particulars, usuario, nivelAcceso);

                    dependenciasDelUsuario = LN3.obtenerDependenciasDelUsuario(usuario);
                    listaDeObjetos = LN3.obtenerParticularesSancionados(usuario, nivelAcceso, dependenciasDelUsuario, filtroBusqueda);
                    if (listaDeObjetos != null)
                    {
                        paginacion = listaDeObjetos.getPaginacion();
                        particularesSancionados = listaDeObjetos.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("particularesSancionados", particularesSancionados);
                    request.setAttribute("correcto", true);
                    request.setAttribute("mensaje", mensaje);
                    this.fordward(request, resp, "s3Sancionados/frmPaginaPrincipalParticularesSancionados.jsp");
                }

                else if (strAccion.equals("eliminarParticularSancionado"))
                {
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    ArrayList<ParticularSancionado> particularesSancionados;
                    ArrayList<Miga> migas = new ArrayList<>();
                    String id;
                    FILTRO_BUSQUEDA_PSANCIONADOS filtroBusqueda;
                    String mensaje;
                    PAGINACION paginacion;
                    PsancionadosConPaginacion listaDeObjetos;
                    PSANCIONADOS particular;

                    paginacion = new PAGINACION();
                    particularesSancionados = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Particulares sancionados", "", "sanciones"));

                    id = request.getParameter("txtId");
                    filtroBusqueda = LN3.modelarFiltroParticularSancionado(request);

                    particular = LN3.obtenerDetalleParticularSancionado(id);

                    mensaje = LN3.eliminarParticularSancionado(id, particular, usuario, nivelAcceso);

                    dependenciasDelUsuario = LN3.obtenerDependenciasDelUsuario(usuario);
                    listaDeObjetos = LN3.obtenerParticularesSancionados(usuario, nivelAcceso, dependenciasDelUsuario, filtroBusqueda);
                    if (listaDeObjetos != null)
                    {
                        paginacion = listaDeObjetos.getPaginacion();
                        particularesSancionados = listaDeObjetos.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("particularesSancionados", particularesSancionados);
                    request.setAttribute("correcto", true);
                    request.setAttribute("mensaje", mensaje);
                    this.fordward(request, resp, "s3Sancionados/frmPaginaPrincipalParticularesSancionados.jsp");
                }

                else if (strAccion.equals("obtenerDatosDelDomicilioEnMexico"))
                {
                    ArrayList<ASENTAMIENTO> asentamientos;
                    Gson gson;

                    gson = new Gson();

                    String parameter = request.getParameter("txtId");
                    asentamientos = LN3.obtenerAsentamientoPorCP(parameter);
                    String json = gson.toJson(asentamientos);
                    resp.setContentType("application/json");
                    resp.setCharacterEncoding("ISO-8859-1");
                    resp.getWriter().write(json);

                }

                else if (strAccion.equals("obtenerListaDeMunicipios"))
                {
                    ArrayList<MUNICIPIO_PDE> municipios;
                    Gson gson;

                    gson = new Gson();

                    String parameter = request.getParameter("txtId");
                    municipios = LN3.obtenerListaDeMunicipiosporEstado(parameter);
                    String json = gson.toJson(municipios);

                    resp.setContentType("application/json");
                    resp.setCharacterEncoding("ISO-8859-1");
                    resp.getWriter().write(json);

                }

                else if (strAccion.equals("obtenerListaDeEntidadFederativa"))
                {
                    ArrayList<ENTIDAD_FEDERATIVA_PDE> entidades;
                    Gson gson;

                    gson = new Gson();

                    String parameter = request.getParameter("txtId");
                    entidades = LN3.obtenerListaDeEntidadesFederativa(parameter);
                    String json = gson.toJson(entidades);

                    resp.setContentType("application/json");
                    resp.setCharacterEncoding("ISO-8859-1");
                    resp.getWriter().write(json);

                }

                else if (strAccion.equals("obtenerListaDeLocalidad"))
                {
                    ArrayList<ASENTAMIENTO> localidades;
                    Gson gson;

                    gson = new Gson();

                    String parameter = request.getParameter("txtId");
                    localidades = LN3.obtenerAsentamiento(parameter);
                    String json = gson.toJson(localidades);

                    resp.setContentType("application/json");
                    resp.setCharacterEncoding("ISO-8859-1");
                    resp.getWriter().write(json);

                }

            }
            else
            {
                if (sesion != null)
                {
                    sesion.invalidate();
                    sesion = null;
                }

                request.setAttribute("mensaje", "Su sesi&oacute;n ha expirado ");
                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
        }
        catch (ServletException | IOException ex)
        {
            request.setAttribute("mensaje", "Error : " + ex.toString());
            this.fordward(request, resp, "/error.jsp");
        }
        catch (Exception ex)
        {
            request.setAttribute("mensaje", "Error " + ex.toString());
            this.fordward(request, resp, "/error.jsp");
        }
    }

    public void fordward(HttpServletRequest req, HttpServletResponse resp, String ruta) throws ServletException, IOException
    {
        req.setAttribute("datosDeLaUltimaActualizacion", ConfVariables.getDATOS_ACTUALIZACION());
        RequestDispatcher despachador = req.getRequestDispatcher(ruta);
        despachador.forward(req, resp);
    }
}

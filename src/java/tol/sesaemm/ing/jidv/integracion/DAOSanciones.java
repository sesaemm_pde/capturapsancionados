/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.integracion;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Collation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bson.Document;
import org.bson.types.ObjectId;
import tol.sesaemm.annotations.Exclude;
import tol.sesaemm.funciones.BitacoraPde;
import tol.sesaemm.funciones.ExpresionesRegulares;
import tol.sesaemm.funciones.ManejoDeFechas;
import tol.sesaemm.funciones.TBITACORAS_PDE;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.PAGINACION;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.FILTRO_BUSQUEDA_PSANCIONADOS;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.ParticularSancionado;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.PsancionadosConPaginacion;
import tol.sesaemm.javabeans.*;
import tol.sesaemm.javabeans.s3psancionados.ENTIDAD_FEDERATIVA;
import tol.sesaemm.javabeans.s3psancionados.LOCALIDAD;
import tol.sesaemm.javabeans.s3psancionados.MUNICIPIO;
import tol.sesaemm.javabeans.s3psancionados.PSANCIONADOS;
import tol.sesaemm.javabeans.s3psancionados.TIPO_FALTA;
import tol.sesaemm.javabeans.s3psancionados.TIPO_SANCION_PAR;
import tol.sesaemm.javabeans.s3psancionados.VIALIDAD;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
 
public class DAOSanciones
{

    private static final Collation collation = Collation.builder().locale("es").caseLevel(true).build();

    /**
     * Devuelve la lista de puestos registrado en la base de datos
     *
     * @return ArrayList PUESTO
     *
     * @throws Exception
     */
    public static ArrayList<PUESTO> obtenerListaDePuestos() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        FindIterable<Document> aIPuesto;                                        
        ArrayList<PUESTO> listadoPuestos;                                       
        MongoCursor<Document> cursor = null;                                    
        PUESTO puesto;
        JsonWriterSettings settings;                                            
        Gson gson;
        ConfVariables confVariables = new ConfVariables();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("puesto");

                aIPuesto = collection.find().sort(new Document("funcional", 1));
                cursor = aIPuesto.iterator();

                listadoPuestos = new ArrayList<>();
                gson = new Gson();
                settings = JsonWriterSettings.builder().outputMode(JsonMode.RELAXED).build();

                while (cursor.hasNext())
                {
                    Document next = cursor.next();
                    puesto = gson.fromJson(next.toJson(settings), PUESTO.class);
                    puesto.setNombre(puesto.getFuncional().trim());
                    puesto.setId(next.get("_id", ObjectId.class));
                    listadoPuestos.add(puesto);
                }

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener puestos funcionales:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return listadoPuestos;
    }

    /**
     * Devuelve la lista de g&eacute;neros almacenados en la base de datos
     *
     * @return ArrayList GENERO
     * @throws java.lang.Exception
     */
    public static ArrayList<GENERO> obtenerListaDeGeneros() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<GENERO> collection;                                     
        ArrayList<GENERO> listadoGeneros;                                       
        ConfVariables confVariables = new ConfVariables();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("genero", GENERO.class);

                listadoGeneros = collection.find().sort(new Document("valor", 1)).into(new ArrayList<>());

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener generos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoGeneros;
    }

    /**
     * Obtener listado de entes p&uacute;blicos asignados al usuario, si es
     * administrador obtendrá toda la lista
     *
     * @param usuario Usuario en sesi&oacute;n
     *
     * @return Lista de entes p&uacute;blicos asignados al usuario
     *
     * @throws Exception
     */
    public static ArrayList<ENTE_PUBLICO> obtenerEntesPublicos(USUARIO usuario) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        FindIterable<Document> findIterable;                                    
        ArrayList<ENTE_PUBLICO> listadoDependencias;                            
        MongoCursor<Document> cursor = null;                                    
        JsonWriterSettings settings;                                            
        Gson gson;
        ENTE_PUBLICO ente_publico;
        ConfVariables confVariables = new ConfVariables();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ente_publico");
                List<Document> where = new ArrayList<>();

                Document or = new Document();

                for (SISTEMAS sistema : usuario.getSistemas())
                {
                    if (sistema.getSistema().getId() == 3)
                    {
                        if (sistema.getNivel_acceso().getId() != 1)
                        {
                            for (ENTE_PUBLICO itemDependencia : usuario.getDependencias())
                            {
                                where.add(new Document("clave", itemDependencia.getClave()));
                            }
                            or.append("$or", where);
                        }
                    }
                }

                findIterable = collection.find(or).sort(new Document("valor", 1));
                cursor = findIterable.iterator();

                listadoDependencias = new ArrayList<>();
                gson = new Gson();
                settings = JsonWriterSettings.builder().outputMode(JsonMode.RELAXED).build();

                while (cursor.hasNext())
                {
                    Document next = cursor.next();
                    ente_publico = gson.fromJson(next.toJson(settings), ENTE_PUBLICO.class);
                    ente_publico.setNombre(ente_publico.getValor());
                    ente_publico.setId(next.get("_id", ObjectId.class));
                    listadoDependencias.add(ente_publico);
                }

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener entes p&uacute;blicos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return listadoDependencias;
    }

    /**
     * Devuelve la lista de los tipos de monedas registrados en la base de datos
     *
     * @return ArrayList MONEDA
     * @throws java.lang.Exception
     */
    public static ArrayList<MONEDA> obtenerListaDeTiposDeMoneda() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<MONEDA> collection;                                     
        ArrayList<MONEDA> listadoDeMonedas;                                     
        ConfVariables confVariables = new ConfVariables();

        listadoDeMonedas = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("moneda", MONEDA.class);

                listadoDeMonedas = collection.find().sort(new Document("valor", 1)).into(new ArrayList<>());

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener listado de tipos de monedas:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        return listadoDeMonedas;
    }

    /**
     * Devuelve el ente publico que le pertenece al servidor p&uacute;blicos
     *
     * @param clave
     *
     * @return INSTITUCION_DEPENDENCIA
     *
     * @throws java.lang.Exception
     */
    public static INSTITUCION_DEPENDENCIA obtenerEntePublico(String clave) throws Exception
    {
        INSTITUCION_DEPENDENCIA institucioDependencia;                          
        ENTE_PUBLICO entePublico;                                               
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ENTE_PUBLICO> collection;                               
        institucioDependencia = new INSTITUCION_DEPENDENCIA();
        ConfVariables confVariables = new ConfVariables();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ente_publico", ENTE_PUBLICO.class);

                entePublico = collection.find(new Document("clave", clave)).sort(new Document("valor", 1)).first();

                if (entePublico != null)
                {
                    institucioDependencia.setClave(entePublico.getClave());
                    institucioDependencia.setNombre(entePublico.getValor());
                    institucioDependencia.setSiglas(entePublico.getSiglas());
                }

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener listado entes p&uacute;blicos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return institucioDependencia;
    }

    /**
     * Devuelve un genero en especifico usando como filtro de b&uacute;squeda el
     * c&oacute;digo del genero
     *
     * @param parameter c&oacute;digo del genero que se buscara
     *
     * @return GENERO
     *
     * @throws java.lang.Exception
     */
    public static GENERO obtenerGenero(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<GENERO> collection;                                     
        GENERO genero;                                                          
        ConfVariables confVariables = new ConfVariables();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("genero", GENERO.class);

                genero = collection.find(new Document("clave", new Document("$eq", parameter))).first();

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el genero de la base de datos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return genero;
    }

    /**
     * Devuelve un puesto en especifico usando como filtro de b&uacute;squeda el
     * valor funcional del puesto
     *
     * @param parameter valor id del puesto que se buscara
     *
     * @return PUESTO
     *
     * @throws Exception
     */
    public static PUESTO obtenerPuesto(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<PUESTO> collection;                                     
        FindIterable<Document> aIGenero;                                        
        PUESTO puesto;                                                          
        MongoCursor<Document> cursor = null;                                    
        Document query;
        Gson gson;                                                              
        puesto = new PUESTO();
        gson = new Gson();
        ConfVariables confVariables = new ConfVariables();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("puesto", PUESTO.class);

                query = new Document("_id", new ObjectId(parameter));

                puesto = collection.find(query).collation(collation).first();

                if (puesto == null)
                {
                    puesto = DAOSanciones.obtenerPuestoPorNombre(parameter);
                }

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el puesto de la base de datos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return puesto;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws Exception
     */
    public static PUESTO obtenerPuestoPorNombre(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<PUESTO> collection;                                     
        PUESTO puesto;                                                          
        Document query;
        ConfVariables confVariables = new ConfVariables();

        puesto = new PUESTO();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("puesto", PUESTO.class);

                query = new Document("funcional", new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(parameter)).append("$options", "i"));

                puesto = collection.find(query).collation(collation).first();

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el puesto de la base de datos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return puesto;
    }

    /**
     * Devuelve una lista de objetos que contiene los valores de cada
     * sanci&oacute;n aplicada al servidor publico
     *
     * @param codigoTiposSanciones
     *
     * @return ArrayList TIPO_SANCION_SER
     *
     * @throws Exception
     */
    public static ArrayList<TIPO_SANCION_PAR> obtenerListaTiposDeSancionPsancionados(List<String> codigoTiposSanciones) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_SANCION_PAR> collection;                           
        ArrayList<TIPO_SANCION_PAR> listadodeSanciones;                         
        ArrayList<Document> where;
        ConfVariables confVariables = new ConfVariables();

        where = new ArrayList<>();
        listadodeSanciones = new ArrayList<>();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_sancion_par", TIPO_SANCION_PAR.class);
                if (codigoTiposSanciones.size() > 0)
                {
                    for (String calve : codigoTiposSanciones)
                    {
                        where.add(new Document().append("clave", calve));
                    }

                    listadodeSanciones = collection.find(new Document("$or", where)).sort(new Document("valor", 1)).into(new ArrayList<>());

                }

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener los tipos de sanciones del servidor p&uacute;blico sancionado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        return listadodeSanciones;
    }

    /**
     * Devuelve los par&aacute;metros correspondientes de un tipo de moneda
     * usando como filtro la clave de la moneda
     *
     * @param parameter clave del tipo de moneda
     *
     * @return MONEDA
     *
     * @throws Exception
     */
    public static MONEDA obtenerMoneda(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<MONEDA> collection;                                     
        MONEDA moneda;                                                          
        ConfVariables confVariables = new ConfVariables();

        moneda = new MONEDA();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("moneda", MONEDA.class);

                moneda = collection.find(new Document("clave", new Document("$eq", parameter))).first();

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener tipo de moneda de la base de datos: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return moneda;
    }

    /**
     * Devuelve el listado de las unidades de medida para el plazo de
     * inhabilitaci&oacute;n
     *
     *
     * @return ArrayList UNIDAD_MEDIDA_PLAZO
     *
     * @throws Exception
     */
    public static ArrayList<UNIDAD_MEDIDA_PLAZO> obtenerListaDeUnidadMedidaPlazo() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<UNIDAD_MEDIDA_PLAZO> collection;                        
        ArrayList<UNIDAD_MEDIDA_PLAZO> listaDePlazos;                           
        ConfVariables confVariables = new ConfVariables();

        listaDePlazos = new ArrayList<>();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("unidad_medida_plazo", UNIDAD_MEDIDA_PLAZO.class);

                listaDePlazos = collection.find().collation(collation).sort(new Document("valor", 1)).into(new ArrayList<>());

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener las unidades de medida para el plazo: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listaDePlazos;
    }

    /**
     * Devuelve la unidad de de medida para el plazo de inhabilitaci&oacute;n
     *
     * @param parameter valor de b&uacute;squeda
     *
     * @return ArrayList UNIDAD_MEDIDA_PLAZO
     *
     * @throws Exception
     */
    public static UNIDAD_MEDIDA_PLAZO obtenerUnidadMedidaPlazo(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<UNIDAD_MEDIDA_PLAZO> collection;                        
        UNIDAD_MEDIDA_PLAZO plazo;                                              
        ConfVariables confVariables = new ConfVariables();
        plazo = new UNIDAD_MEDIDA_PLAZO();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("unidad_medida_plazo", UNIDAD_MEDIDA_PLAZO.class);
                plazo = collection.find(new Document("clave", new Document("$eq", parameter))).collation(collation).sort(new Document("valor", 1)).first();

                if (plazo == null)
                {
                    plazo = collection.find(new Document("valor",
                            new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(parameter)).append("$options", "i")
                    )).collation(collation).sort(new Document("valor", 1)).first();
                }

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener la unidad de medida para el plazo: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return plazo;
    }

    /**
     * Devuelve el listado de los &oacute;rgano internos de control de la base
     * de datos
     *
     * @return ArrayList ORGANO_INTERNO_CONTROL
     *
     * @throws java.lang.Exception
     */
    public static ArrayList<ORGANO_INTERNO_CONTROL> obtenerListaDeOrganoInternoDeControl() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ORGANO_INTERNO_CONTROL> collection;                     
        ArrayList<ORGANO_INTERNO_CONTROL> listaDeOrganos;                       
        ConfVariables confVariables = new ConfVariables();

        listaDeOrganos = new ArrayList<>();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("organo_interno_control", ORGANO_INTERNO_CONTROL.class);

                listaDeOrganos = collection.find().collation(collation).sort(new Document("valor", 1)).into(new ArrayList<>());

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de los organos internos de control: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listaDeOrganos;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws Exception
     */
    public static ORGANO_INTERNO_CONTROL obtenerOrganoInternoDeControl(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ORGANO_INTERNO_CONTROL> collection;                     
        ORGANO_INTERNO_CONTROL organo;                                          
        ConfVariables confVariables = new ConfVariables();

        organo = new ORGANO_INTERNO_CONTROL();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("organo_interno_control", ORGANO_INTERNO_CONTROL.class);

                organo = collection.find(new Document("clave", new Document("$eq", parameter))).collation(collation).sort(new Document("valor", 1)).first();

                if (organo == null)
                {
                    organo = collection.find(new Document("valor", new Document("$eq", parameter))).collation(collation).sort(new Document("valor", 1)).first();
                }

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el organo interno de control: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return organo;
    }

    /**
     * A&ntilde;ade un nuevo registro a la base de datos, con los datos del
     * particular sancionado
     *
     * @param particular Datos del particular sancionado
     * @param usuario Usuario en sesión
     * @param nivelAcceso Nivel de acceso del usario
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static boolean registrarParticularSancionado(PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {

        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        boolean blnbSeRegistra;                                                 
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document psancionados;                                                  
        ConfVariables confVariables = new ConfVariables();

        blnbSeRegistra = false;

        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();

        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("psancionados");

                if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
                {
                    psancionados = Document.parse(gson.toJson(particular));
                    collection.insertOne(psancionados);
                    
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.psancionados, "SAEMM", particular.getId(), psancionados.toJson(), BitacoraPde.registro));
                    
                    blnbSeRegistra = true;
                }

            }
            else
            {
                throw new Exception("Conexion no establecida");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al registrar los datos del particular sanconado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return blnbSeRegistra;
    }

    /**
     * Devuelve el listado de los particulares sancionados junto con los valores
     * de paginacion
     *
     * @param nivelAccesopaginaci&oacute;n
     * @param nivelAcceso
     *
     * @param usuario Usuario en sesi&oacute;n
     * @param dependenciasDelUsuario Dependencias asignadas al usuario
     * @param filtroBusqueda filtro de b&uacute;squeda
     *
     * @return ArrayList Object
     *
     * @throws Exception
     */
    public static PsancionadosConPaginacion obtenerParticularesSancionados(USUARIO usuario, NIVEL_ACCESO nivelAcceso, ArrayList<ENTE_PUBLICO> dependenciasDelUsuario, FILTRO_BUSQUEDA_PSANCIONADOS filtroBusqueda) throws Exception
    {

        PAGINACION filtroPaginacion;                                            
        ArrayList<ParticularSancionado> listadoParticularesSancionados;         
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        AggregateIterable<Document> aIParticulares;                             
        MongoCursor<Document> iteratorDeResultados = null;                      
        Document where;                                                         
        Document order_by;                                                      
        ArrayList<Document> query;                                              
        int totalRegFiltro = 0;                                                 
        int totalPaginas = 0;                                                   
        int saltos = 0;                                                         
        PsancionadosConPaginacion listaDeObjetos;                               
        List<Document> where_dependencia;                                       
        Document or;                                                            
        ConfVariables confVariables = new ConfVariables();

        query = new ArrayList<>();
        listadoParticularesSancionados = new ArrayList<>();
        where = new Document();
        order_by = new Document();
        or = new Document();
        filtroPaginacion = new PAGINACION();
        listaDeObjetos = new PsancionadosConPaginacion();
        where_dependencia = new ArrayList<>();
        or = new Document();
        where = new Document();

        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {

                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("psancionados");

                Collation collation = Collation.builder()
                        .locale("es")
                        .caseLevel(true)
                        .build();

                switch (filtroBusqueda.getOrden())
                {
                    case "dependencia":  
                        order_by.append("institucionDependenciaNombre", 1);
                        order_by.append("fechaCaptura", -1);
                        break; 
                    case "nombreRazonSocial":  
                    default:
                        order_by.append("nombreRazonSocial", 1);
                        order_by.append("fechaCaptura", -1);
                        break;  
                }

                if (nivelAcceso.getId() == 2 || nivelAcceso.getId() == 3)
                {
                    for (ENTE_PUBLICO dependencia : dependenciasDelUsuario)
                    {
                        where_dependencia.add(new Document("institucionDependenciaCodigo", dependencia.getClave()));
                    }
                    or.append("$or", where_dependencia);
                }

                if (nivelAcceso.getId() == 2)
                {
                    where.append("usuarioRegistro", new Document("$eq", usuario.getUsuario()));
                }
                else if (nivelAcceso.getId() == 3)
                {
                    where.append("publicar", new Document("$ne", "0"));
                }
                else if (nivelAcceso.getId() == 5)
                {
                    where.append("publicar", new Document("$ne", "0"));
                }

                boolean tieneCondicion = false;
                if (filtroBusqueda.getRazonSocial() != null && !filtroBusqueda.getRazonSocial().isEmpty())
                {
                    where.append("nombreRazonSocial",
                            new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(filtroBusqueda.getRazonSocial())).append("$options", "i"));
                    tieneCondicion = true;
                }
                if (filtroBusqueda.getInstitucion() != null && !filtroBusqueda.getInstitucion().isEmpty())
                {
                    where.append("institucionDependenciaNombre",
                            new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(filtroBusqueda.getInstitucion())).append("$options", "i"));
                    tieneCondicion = true;
                }

                query.add(
                        new Document("$addFields",
                                new Document("nombreRazonSocial",
                                        new Document("$trim",
                                                new Document("input", "$particularSancionado.nombreRazonSocial")
                                        )
                                )
                                        .append("institucionDependenciaNombre",
                                                new Document("$trim",
                                                        new Document("input",
                                                                new Document("$cond",
                                                                        Arrays.asList(
                                                                                new Document(
                                                                                        "$or",
                                                                                        Arrays.asList(
                                                                                                new Document("$eq",
                                                                                                        Arrays.asList("$institucionDependencia.nombre", "")
                                                                                                ),
                                                                                                new Document("$eq",
                                                                                                        Arrays.asList("$institucionDependencia.nombre", null)
                                                                                                )
                                                                                        )
                                                                                ),
                                                                                "$institucionDependencia.siglas",
                                                                                "$institucionDependencia.nombre"
                                                                        )
                                                                )
                                                        )
                                                )
                                        )
                                        .append("institucionDependenciaCodigo", "$institucionDependencia.clave")
                                        .append("usuarioRegistro", "$metadatos.usuario_registro")
                        )
                );
                query.add(new Document("$match",
                        new Document("$and", Arrays.asList(where, or))
                ));
                
                query.add(new Document("$count", "totalCount"));
                aIParticulares = collection.aggregate(query).collation(collation).allowDiskUse(true);
                iteratorDeResultados = aIParticulares.iterator();
                while (iteratorDeResultados.hasNext())
                {
                    totalRegFiltro = iteratorDeResultados.next().get("totalCount", Integer.class);
                    totalPaginas = (int) Math.ceil((float) totalRegFiltro / filtroBusqueda.getRegistrosMostrar());

                    if (filtroBusqueda.getNumeroPagina() > 1)
                    {
                        saltos = (filtroBusqueda.getNumeroPagina() - 1) * filtroBusqueda.getRegistrosMostrar();
                    }

                    filtroPaginacion.setRegistrosMostrar(filtroBusqueda.getRegistrosMostrar());
                    filtroPaginacion.setNumeroPagina(filtroBusqueda.getNumeroPagina());
                    filtroPaginacion.setTotalPaginas((totalPaginas < 1) ? 1 : totalPaginas);
                    filtroPaginacion.setNumeroSaltos(saltos);
                    filtroPaginacion.setTotalRegistros(totalRegFiltro);
                    filtroPaginacion.setIsEmpty(false);
                }
                query.remove(2);

                
                query.add(new Document("$sort", order_by));

                if (!filtroPaginacion.isEmpty())
                {
                    query.add(new Document("$skip", filtroPaginacion.getNumeroSaltos()));
                    query.add(new Document("$limit", filtroPaginacion.getRegistrosMostrar()));
                }
                

                listadoParticularesSancionados = collection.aggregate(query, ParticularSancionado.class).collation(collation).allowDiskUse(true).into(new ArrayList<>());

                listaDeObjetos.setPaginacion(filtroPaginacion);
                listaDeObjetos.setServidores(listadoParticularesSancionados);

            }
            else
            {
                throw new Exception("Conexion no establecida");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de particulares sancionados : " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, iteratorDeResultados);
        }

        return listaDeObjetos;
    }

    /**
     * Devuelve los valores de un particular sancionado usando como filtro el id
     * de la base de datos
     *
     * @param idParticular id del particular sancionado
     *
     * @return PSANCIONADOS
     *
     * @throws Exception
     */
    public static PSANCIONADOS obtenerDetalleParticularSancionado(String idParticular) throws Exception
    {
        PSANCIONADOS sancionados;                                               
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<PSANCIONADOS> collection;                               
        ConfVariables confVariables = new ConfVariables();

        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();
            sancionados = new PSANCIONADOS();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("psancionados", PSANCIONADOS.class);
                sancionados = collection.find(new Document("_id", new Document("$eq", new ObjectId(idParticular)))).first();

            }
            else
            {
                throw new Exception("Conexion no establecida");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener datos del particular sancionado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return sancionados;
    }

    /**
     * Devuelve el valor de los campos p&uacute;blicos y privados
     *
     * @param coleccion
     *
     * @return PSANCIONADOS
     *
     * @throws java.lang.Exception
     */
    public static PSANCIONADOS obtenerEstatusDePrivacidadCamposPSANCIONADOS(String coleccion) throws Exception
    {
        PSANCIONADOS sancionados;                                               
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        ConfVariables confVariables = new ConfVariables();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();
            sancionados = new PSANCIONADOS();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("campos_publicos");
                Document match = new Document();

                match.append("coleccion", coleccion);   

                sancionados = collection.find(match, PSANCIONADOS.class).first();

            }
            else
            {
                throw new Exception("Conexion no establecida");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener privacidad de campos: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return sancionados;
    }

    /**
     * Devuelve el tipo de persona que es particular sancionado
     *
     * @param tipoPersona clave del tipo de persona
     *
     * @return TIPO_PERSONA
     *
     * @throws Exception
     */
    public static TIPO_PERSONA obtenerTipoPersona(String tipoPersona) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_PERSONA> collection;                               
        TIPO_PERSONA persona;                                                   
        ConfVariables confVariables = new ConfVariables();

        persona = new TIPO_PERSONA();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_persona", TIPO_PERSONA.class);

                persona = collection.find(new Document("clave", new Document("$eq", tipoPersona))).collation(collation).sort(new Document("valor", 1)).first();

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el tipo de persona: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return persona;
    }

    /**
     * Devuelve el listado de los tipos de personas registrados en la base de
     * datos
     *
     * @return ArrayList TIPO_PERSONA
     *
     * @throws Exception
     */
    public static ArrayList<TIPO_PERSONA> obtenerListaTipoPersona() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_PERSONA> collection;                               
        ArrayList<TIPO_PERSONA> listaTipoPersona;                               
        ConfVariables confVariables = new ConfVariables();

        listaTipoPersona = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_persona", TIPO_PERSONA.class);

                listaTipoPersona = collection.find().collation(collation).sort(new Document("valor", 1)).into(new ArrayList<>());

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de tipo de persona: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listaTipoPersona;
    }

    /**
     * Registra los cambios realizados a un particular usando como filtro el
     * nivel del usuario y el id del registro
     *
     * @param id Id del registro que se va a editar
     * @param particular Objeto que contiene los valores del particular
     * @param usuario Usuario en sesi&oacute;n
     * @param nivelAcceso Nivel que es otorgado a cada usuario
     *
     * @return boolean
     *
     * @throws java.lang.Exception
     */
    public static boolean editarParticularSancionado(String id, PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        Document where;                                                         
        boolean blnSeEdita;                                                     
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document psancionados;                                                  
        ConfVariables confVariables = new ConfVariables();

        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();

        where = new Document();
        blnSeEdita = false;

        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {

                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("psancionados");

                if (nivelAcceso.getId() == 1)
                {
                    where.append("_id", new ObjectId(id));
                    blnSeEdita = true;
                }
                else if (nivelAcceso.getId() == 2)
                {
                    where.append("_id", new ObjectId(id));
                    where.append("metadatos.usuario_registro", usuario.getUsuario());
                    blnSeEdita = true;
                }

                if (blnSeEdita)
                {
                    psancionados = Document.parse(gson.toJson(particular));
                    collection.updateOne(where, new Document("$set", psancionados));
                    
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.psancionados, "SAEMM", particular.getId(), psancionados.toJson(), BitacoraPde.modificacion));
                    
                }

            }
            else
            {
                throw new Exception("Conexion no establecida");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al modificar particular sanconado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return blnSeEdita;
    }

    /**
     * Cambia el estatus del valor publicar a 1 para el particular sancionado
     * usando como filtro el id del registro
     *
     * @param id identificador de particular sancionado
     * @param particular Datos del particular sancionado
     * @param usuario Usuario en sesi&oacute;n
     * @param nivelAcceso Acceso del usuario al sistema
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static boolean publicarParticularSancionado(String id, PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {

        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        Document where;                                                         
        boolean blnSePublica;                                                   
        ManejoDeFechas forDeFechas;
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document psancionados;                                                  
        ConfVariables confVariables = new ConfVariables();

        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();

        forDeFechas = new ManejoDeFechas();
        where = new Document();
        blnSePublica = false;

        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {

                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("psancionados");

                if (particular.getMetadatos() != null)
                {
                    particular.getMetadatos().setUsuario_publicacion(usuario.getUsuario());
                    particular.getMetadatos().setFecha_publicacion(forDeFechas.getDateNowAsString());
                }
                else
                {
                    METADATOS metadatos = new METADATOS();
                    metadatos.setUsuario_modificacion(usuario.getUsuario());
                    metadatos.setFecha_modificacion(forDeFechas.getDateNowAsString());
                    particular.setMetadatos(metadatos);
                }

                if (nivelAcceso.getId() == 1)
                {
                    where.append("_id", new ObjectId(id));
                    blnSePublica = true;
                }
                else if (nivelAcceso.getId() == 2)
                {
                    where.append("_id", new ObjectId(id));
                    where.append("metadatos.usuario_registro", usuario.getUsuario());
                    blnSePublica = true;
                }

                if (blnSePublica)
                {
                    particular.setPublicar("1");
                    psancionados = Document.parse(gson.toJson(particular));
                    collection.updateOne(where, new Document("$set", psancionados));
                    
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.psancionados, "SAEMM", particular.getId(), psancionados.toJson(), BitacoraPde.publicacion));
                    
                }

            }
            else
            {
                throw new Exception("Conexion no establecida");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al publicar un particular sancionado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return blnSePublica;
    }

    /**
     * Cambia el estatus del valor publicar a 0 para el particular sancionado
     * usando como filtro el id del registro
     *
     * @param id identificador de particular sancionado
     * @param particular Datos del particular sancionado
     * @param usuario Usuario en sesi&oacute;n
     * @param nivelAcceso Acceso del usuario al sistema
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static boolean desPublicarParticularSancionado(String id, PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {

        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        Document where;                                                         
        boolean seDesPublica;                                                   
        ManejoDeFechas forDeFechas;
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document psancionados;                                                  
        ConfVariables confVariables = new ConfVariables();

        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();

        forDeFechas = new ManejoDeFechas();
        where = new Document();
        seDesPublica = false;

        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("psancionados");

                if (particular.getMetadatos() != null)
                {
                    particular.getMetadatos().setUsuario_publicacion(usuario.getUsuario());
                    particular.getMetadatos().setFecha_publicacion(forDeFechas.getDateNowAsString());
                }
                else
                {
                    METADATOS metadatos = new METADATOS();
                    metadatos.setUsuario_modificacion(usuario.getUsuario());
                    metadatos.setFecha_modificacion(forDeFechas.getDateNowAsString());
                    particular.setMetadatos(metadatos);
                }

                if (nivelAcceso.getId() == 1)
                {
                    where.append("_id", new ObjectId(id));
                    seDesPublica = true;
                }
                else if (nivelAcceso.getId() == 2)
                {
                    where.append("_id", new ObjectId(id));
                    where.append("metadatos.usuario_registro", usuario.getUsuario());
                    seDesPublica = true;
                }

                if (seDesPublica)
                {
                    particular.setPublicar("0");
                    psancionados = Document.parse(gson.toJson(particular));
                    collection.updateOne(where, new Document("$set", psancionados));
                    
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.psancionados, "SAEMM", particular.getId(), psancionados.toJson(), BitacoraPde.despublicacion));
                    
                }

            }
            else
            {
                throw new Exception("Conexion no establecida");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al despublicar un particular sancionado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return seDesPublica;
    }

    /**
     * Borra de la base de datos el registro del particular sancionado usando
     * como filtro el id del registro
     *
     * @param id identificador de particular sancionado
     * @param particular Datos del particular sancionado
     * @param usuario Usuario en sesi&oacute;n
     * @param nivelAcceso Acceso del usuario al sistema
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static boolean eliminarParticularSancionado(String id, PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {

        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        Document query;                                                         
        boolean seBorra;                                                        
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document psancionados;                                                          
        ConfVariables confVariables = new ConfVariables();

        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();

        query = new Document();
        seBorra = false;

        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("psancionados");

                if (nivelAcceso.getId() == 1)
                {
                    query.append("_id", new ObjectId(id));
                    seBorra = true;
                }
                else if (nivelAcceso.getId() == 2)
                {
                    query.append("_id", new ObjectId(id));
                    query.append("metadatos.usuario_registro", usuario.getUsuario());
                    seBorra = true;
                }

                if (seBorra)
                {
                    collection.deleteOne(query);
                    psancionados = Document.parse(gson.toJson(particular));
                    
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.psancionados, "SAEMM", particular.getId(), psancionados.toJson(), BitacoraPde.borrado));
                    
                }

            }
            else
            {
                throw new Exception("Conexion no establecida");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al eliminar un particular sancionado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return seBorra;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws Exception
     */
    public static ArrayList<ENTIDAD_FEDERATIVA_PDE> obtenerListaDeEntidadesFederativas(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ENTIDAD_FEDERATIVA_PDE> collection;                     
        ArrayList<ENTIDAD_FEDERATIVA_PDE> entidadesFederativas;                 
        ConfVariables confVariables = new ConfVariables();

        entidadesFederativas = new ArrayList<>();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("entidades", ENTIDAD_FEDERATIVA_PDE.class);

                entidadesFederativas = collection.find().collation(collation).sort(new Document("nom_agee", 1)).into(new ArrayList<>());

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de tipo de entidades federativas: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return entidadesFederativas;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws Exception
     */
    public static ArrayList<MUNICIPIO_PDE> obtenerListaDeMunicipiosporEstado(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                          
        MongoDatabase database;                                                 
        MongoCollection<MUNICIPIO_PDE> collection;                              
        ArrayList<MUNICIPIO_PDE> municipios;                                    
        ConfVariables confVariables = new ConfVariables();

        municipios = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (parameter != null && !parameter.isEmpty())
            {
                if (conectarBaseDatos != null)
                {
                    database = conectarBaseDatos.getDatabase(confVariables.getBD());
                    collection = database.getCollection("municipios", MUNICIPIO_PDE.class);

                    municipios = collection.find(
                            new Document("cve_agee",
                                    new Document("$eq", parameter)
                            )
                    ).collation(collation).sort(new Document("nom_agem", 1)).into(new ArrayList<>());

                }
                else
                {
                    throw new Exception("Conexion no establecida.");
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de municipios: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return municipios;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws Exception
     */
    public static ArrayList<VIALIDAD> obtenerTiposDeVialidades(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<VIALIDAD> collection;                                   
        ArrayList<VIALIDAD> vialidades;                                         
        ConfVariables confVariables = new ConfVariables();

        vialidades = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_vialidad", VIALIDAD.class);

                vialidades = collection.find().collation(collation).sort(new Document("valor", 1)).into(new ArrayList<>());

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de vialidades: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return vialidades;
    }

    /**
     *
     * @return @throws Exception
     */
    public static ArrayList<PAIS> obtenerListaDePaises() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<PAIS> collection;                                       
        ArrayList<PAIS> paises;                                                 
        ConfVariables confVariables = new ConfVariables();

        paises = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("paises", PAIS.class);

                paises = collection.find().collation(collation).sort(new Document("valor", 1)).into(new ArrayList<>());

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de vialidades: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return paises;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws java.lang.Exception
     */
    public static ArrayList<ASENTAMIENTO> obtenerAsentamiento(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ASENTAMIENTO> collection;                               
        ArrayList<ASENTAMIENTO> asentamientos;                                  
        ConfVariables confVariables = new ConfVariables();

        asentamientos = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("asentamientos", ASENTAMIENTO.class);

                asentamientos = collection.find(new Document("cve_agem", new Document("$eq", parameter))).collation(collation).sort(new Document("nom_asenta", 1)).into(new ArrayList<>());

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de asentamientos: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return asentamientos;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws java.lang.Exception
     */
    public static ArrayList<ASENTAMIENTO> obtenerAsentamientoPorCP(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ASENTAMIENTO> collection;                               
        ArrayList<ASENTAMIENTO> asentamientos;                                  
        ConfVariables confVariables = new ConfVariables();

        asentamientos = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("asentamientos", ASENTAMIENTO.class);

                asentamientos = collection.find(new Document("nom_clave", new Document("$eq", parameter))).collation(collation).sort(new Document("nom_asenta", 1)).into(new ArrayList<>());
                
            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de asentamientos por codigo postal: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return asentamientos;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws java.lang.Exception
     */
    public static PAIS obtenerPais(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<PAIS> collection;                                       
        PAIS pais;                                                              
        ConfVariables confVariables = new ConfVariables();

        pais = new PAIS();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("paises", PAIS.class);

                pais = collection.find(new Document("clave", new Document("$eq", parameter))).collation(collation).sort(new Document("valor", 1)).first();

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el pais: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return pais;
    }

    public static ENTIDAD_FEDERATIVA obtenerEntidadFederativa(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ENTIDAD_FEDERATIVA_PDE> collection;                     
        ENTIDAD_FEDERATIVA entidad;                                             
        ENTIDAD_FEDERATIVA_PDE entidadFederativaPde;                            
        ConfVariables confVariables = new ConfVariables();

        entidad = new ENTIDAD_FEDERATIVA();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("entidades", ENTIDAD_FEDERATIVA_PDE.class);

                entidadFederativaPde = collection.find(new Document("cve_agee", new Document("$eq", parameter))).collation(collation).sort(new Document("nom_agee", 1)).first();

                if (entidadFederativaPde != null)
                {
                    entidad.setClave(entidadFederativaPde.getCve_agee());
                    entidad.setValor(entidadFederativaPde.getNom_agee());
                }

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener la entidad federativa: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return entidad;
    }

    /**
     *
     * @param clvEntidad
     * @param clvMunicipio
     *
     * @return
     *
     * @throws Exception
     */
    public static MUNICIPIO obtenerMunicipio(String clvEntidad, String clvMunicipio) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                  
        MongoDatabase database;                                                 
        MongoCollection<MUNICIPIO_PDE> collection;                              
        MUNICIPIO municipio;                                                    
        MUNICIPIO_PDE municipioPde;
        ArrayList<Document> query;
        ConfVariables confVariables = new ConfVariables();

        municipio = new MUNICIPIO();
        query = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (clvEntidad != null && !clvEntidad.isEmpty()
                    && clvMunicipio != null && !clvMunicipio.isEmpty())
            {
                if (conectarBaseDatos != null)
                {
                    database = conectarBaseDatos.getDatabase(confVariables.getBD());
                    collection = database.getCollection("municipios", MUNICIPIO_PDE.class);

                    if (clvEntidad != null)
                    {
                        query.add(new Document("cve_agee", new Document("$eq", clvEntidad)));
                    }

                    query.add(new Document("cve_agem", new Document("$eq", clvMunicipio)));

                    municipioPde = collection.find(new Document("$and", query)).collation(collation).sort(new Document("nom_agem", 1)).first();

                    if (municipioPde != null)
                    {
                        municipio.setClave(municipioPde.getCve_agem());
                        municipio.setValor(municipioPde.getNom_agem());
                    }

                }
                else
                {
                    throw new Exception("Conexion no establecida.");
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener los datos del municipio: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return municipio;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws Exception
     */
    public static LOCALIDAD obtenerLocalidad(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ASENTAMIENTO> collection;                               
        LOCALIDAD localidad;                                                    
        ASENTAMIENTO asentamiento;
        ConfVariables confVariables = new ConfVariables();

        localidad = new LOCALIDAD();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("asentamientos", ASENTAMIENTO.class);

                asentamiento = collection.find(new Document("cve_asenta", new Document("$eq", parameter))).collation(collation).sort(new Document("nom_asenta", 1)).first();

                if (asentamiento != null)
                {
                    localidad.setClave(asentamiento.getCve_asenta());
                    localidad.setValor(asentamiento.getNom_asenta());
                }

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener la localida: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return localidad;
    }

    public static VIALIDAD obtenerVialidad(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<VIALIDAD> collection;                                   
        VIALIDAD vialidad;                                                      
        ConfVariables confVariables = new ConfVariables();

        vialidad = new VIALIDAD();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_vialidad", VIALIDAD.class);

                vialidad = collection.find(new Document("clave", new Document("$eq", parameter))).collation(collation).sort(new Document("valor", 1)).first();

                if (vialidad == null)
                {
                    vialidad = collection.find(new Document("valor", new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(parameter)).append("$options", "i"))).collation(collation).sort(new Document("valor", 1)).first();
                }

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el tipo de vialidad: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return vialidad;
    }

    /**
     *
     * @return @throws Exception
     */
    public static ArrayList<TIPO_DOCUMENTO> obtenerTiposDeDocumentos() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_DOCUMENTO> collection;                             
        ArrayList<TIPO_DOCUMENTO> documentos;                                   
        ConfVariables confVariables = new ConfVariables();

        documentos = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_documento", TIPO_DOCUMENTO.class);

                documentos = collection.find().collation(collation).sort(new Document("valor", 1)).into(new ArrayList<>());

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de tipos de documentos: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return documentos;
    }

    /**
     *
     * @param parameter
     *
     * @return
     * @throws java.lang.Exception
     */
    public static TIPO_DOCUMENTO obtenerDocumento(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_DOCUMENTO> collection;                             
        TIPO_DOCUMENTO documento;                                               
        ConfVariables confVariables = new ConfVariables();

        documento = new TIPO_DOCUMENTO();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_documento", TIPO_DOCUMENTO.class);

                documento = collection.find(new Document("clave", new Document("$eq", parameter))).collation(collation).sort(new Document("valor", 1)).first();

                if (documento == null)
                {
                    documento = collection.find(new Document("valor", new Document("$eq", parameter))).collation(collation).sort(new Document("valor", 1)).first();
                }

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el tipo de documento: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return documento;
    }

    public static ArrayList<TIPO_FALTA> obtenerListaTiposDeFaltaPsancionados() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_FALTA> collection;                                 
        ArrayList<TIPO_FALTA> listadoDeFaltas;                                                
        ConfVariables confVariables = new ConfVariables();                                 

        listadoDeFaltas = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_falta", TIPO_FALTA.class);

                listadoDeFaltas = collection.find().sort(new Document("valor", 1)).into(new ArrayList<>());

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener listado de tipos de faltas para particulares sancionados:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoDeFaltas;
    }

    public static ArrayList<TIPO_SANCION_PAR> obtenerListaTiposDeSancionPsancionados() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_SANCION_PAR> collection;                           
        ArrayList<TIPO_SANCION_PAR> listadoDeSanciones;                         
        ConfVariables confVariables = new ConfVariables();

        listadoDeSanciones = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_sancion_par", TIPO_SANCION_PAR.class);

                listadoDeSanciones = collection.find().sort(new Document("valor", 1)).into(new ArrayList<>());

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener listado de tipos de sanci&oacute;n para particulares sancionados:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoDeSanciones;
    }

    /**
     *
     * @param parameter
     *
     * @return
     * @throws java.lang.Exception
     */
    public static TIPO_FALTA obtenerTipoDeFaltaPsancionados(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_FALTA> collection;                                 
        TIPO_FALTA falta;                                                       
        ConfVariables confVariables = new ConfVariables();

        falta = new TIPO_FALTA();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_falta", TIPO_FALTA.class);

                falta = collection.find().sort(new Document("valor", 1)).first();

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el tipo de falta para particulares sancionados:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return falta;
    }

    /**
     * Metodo de ayuda para cerrar las conexiones activas
     *
     *
     * @param conectarBaseDatos
     * @param cursor
     */
    public static void cerrarRecursos(MongoClient conectarBaseDatos, MongoCursor<Document> cursor)
    {
        
        if(cursor != null)
        {
            cursor.close();
            cursor = null;
        }
        
        if(conectarBaseDatos != null)
        {
            conectarBaseDatos.close();
            conectarBaseDatos = null;
        }
        
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans.s3Sancionados;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 */
 
public class FILTRO_BUSQUEDA_PSANCIONADOS
  {
    private String id;
    private String razonSocial;
    private String institucion;
    private int registrosMostrar;
    private int numeroPagina;
    private String orden;

    public String getId()
      {
        return id;
      }

    public void setId(String id)
      {
        this.id = id;
      }

    public String getRazonSocial()
      {
        return razonSocial;
      }

    public void setRazonSocial(String razonSocial)
      {
        this.razonSocial = razonSocial;
      }

    public String getInstitucion()
      {
        return institucion;
      }

    public void setInstitucion(String institucion)
      {
        this.institucion = institucion;
      }

    public int getRegistrosMostrar()
      {
        return registrosMostrar;
      }

    public void setRegistrosMostrar(int registrosMostrar)
      {
        this.registrosMostrar = registrosMostrar;
      }

    public int getNumeroPagina()
      {
        return numeroPagina;
      }

    public void setNumeroPagina(int numeroPagina)
      {
        this.numeroPagina = numeroPagina;
      }

    public String getOrden()
      {
        return orden;
      }

    public void setOrden(String orden)
      {
        this.orden = orden;
      }

  }

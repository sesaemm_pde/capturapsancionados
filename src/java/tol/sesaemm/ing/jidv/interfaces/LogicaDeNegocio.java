/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.interfaces;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.javabeans.SISTEMAS;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */
 
public interface LogicaDeNegocio
{

    public USUARIO loguearUsuario(USUARIO usuario) throws Exception;

    public boolean registrarAcceso(String usuario) throws Exception;

    public ArrayList<SISTEMAS> obtenerDatosActualizadosDeSistemas(ArrayList<SISTEMAS> sistemasUsuario) throws Exception;

    public USUARIO modelarUsuario(HttpServletRequest request);

    public boolean esAdmin(USUARIO usuario, int idSistema);

    public NIVEL_ACCESO tipoDeUsuario(USUARIO usuario, int idSistema) throws Exception;

    public String crearExpresionRegularDePalabra(String palabra) throws Exception;
}

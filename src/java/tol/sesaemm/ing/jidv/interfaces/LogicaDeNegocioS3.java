/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.interfaces;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.FILTRO_BUSQUEDA_PSANCIONADOS;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.PsancionadosConPaginacion;
import tol.sesaemm.javabeans.ASENTAMIENTO;
import tol.sesaemm.javabeans.ENTE_PUBLICO;
import tol.sesaemm.javabeans.ENTIDAD_FEDERATIVA_PDE;
import tol.sesaemm.javabeans.GENERO;
import tol.sesaemm.javabeans.MONEDA;
import tol.sesaemm.javabeans.MUNICIPIO_PDE;
import tol.sesaemm.javabeans.ORGANO_INTERNO_CONTROL;
import tol.sesaemm.javabeans.PAIS;
import tol.sesaemm.javabeans.PUESTO;
import tol.sesaemm.javabeans.TIPO_DOCUMENTO;
import tol.sesaemm.javabeans.TIPO_PERSONA;
import tol.sesaemm.javabeans.UNIDAD_MEDIDA_PLAZO;
import tol.sesaemm.javabeans.s3psancionados.PSANCIONADOS;
import tol.sesaemm.javabeans.s3psancionados.TIPO_FALTA;
import tol.sesaemm.javabeans.s3psancionados.TIPO_SANCION_PAR;
import tol.sesaemm.javabeans.s3psancionados.VIALIDAD;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */
 
public interface LogicaDeNegocioS3
{

    public ArrayList<ENTE_PUBLICO> obtenerDependenciasDelUsuario(USUARIO usuario) throws Exception;

    public ArrayList<PUESTO> obtenerListaDePuestos() throws Exception;

    public ArrayList<GENERO> obtenerListaDeGeneros() throws Exception;

    public ArrayList<ENTE_PUBLICO> obtenerEntesPublicos(USUARIO usuario) throws Exception;

    public ArrayList<MONEDA> obtenerListaDeTiposDeMoneda() throws Exception;

    public ArrayList<UNIDAD_MEDIDA_PLAZO> obtenerListaDeUnidadMedidaPlazo() throws Exception;

    public ArrayList<ORGANO_INTERNO_CONTROL> obtenerListaDeOrganoInternoDeControl() throws Exception;

    public ORGANO_INTERNO_CONTROL obtenerOrganoInternoDeControl(String parameter) throws Exception;

    public FILTRO_BUSQUEDA_PSANCIONADOS modelarFiltroParticularSancionado(HttpServletRequest request);

    public PSANCIONADOS modelarRequestParticularSancionado(HttpServletRequest request) throws Exception;

    public String registrarParticularSancionado(PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception;

    public PsancionadosConPaginacion obtenerParticularesSancionados(USUARIO usuario, NIVEL_ACCESO nivelAcceso, ArrayList<ENTE_PUBLICO> dependenciasDelUsuario, FILTRO_BUSQUEDA_PSANCIONADOS filtroBusqueda) throws Exception;

    public PSANCIONADOS obtenerDetalleParticularSancionado(String idParticular) throws Exception;

    public PSANCIONADOS obtenerEstatusDePrivacidadCamposPSANCIONADOS() throws Exception;

    public PSANCIONADOS modelarRequestParticularSancionado(HttpServletRequest request, PSANCIONADOS particular) throws Exception;

    public String editarParticularSancionado(String id, PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception;

    public String publicarParticularSancionado(String id, PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception;

    public String desPublicarParticularSancionado(String id, PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception;

    public String eliminarParticularSancionado(String id, PSANCIONADOS particular, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception;

    public TIPO_PERSONA obtenerTipoPersona(String tipoPersona) throws Exception;

    public ArrayList<TIPO_PERSONA> obtenerListaTipoPersona() throws Exception;

    public ArrayList<MUNICIPIO_PDE> obtenerListaDeMunicipiosporEstado(String parameter) throws Exception;

    public ArrayList<PAIS> obtenerListaDePaises() throws Exception;

    public ArrayList<ASENTAMIENTO> obtenerAsentamiento(String parameter) throws Exception;

    public ArrayList<ASENTAMIENTO> obtenerAsentamientoPorCP(String parameter) throws Exception;

    public ArrayList<ENTIDAD_FEDERATIVA_PDE> obtenerListaDeEntidadesFederativa(String parameter) throws Exception;

    public ArrayList<VIALIDAD> obtenerListaDeVialidades(String parameter) throws Exception;

    public ArrayList<TIPO_DOCUMENTO> obtenerTiposDeDocumentos() throws Exception;

    public ArrayList<TIPO_FALTA> obtenerListaTiposDeFaltaPsancionados() throws Exception;

    public ArrayList<TIPO_SANCION_PAR> obtenerListaTiposDeSancionPsancionados() throws Exception;

}
